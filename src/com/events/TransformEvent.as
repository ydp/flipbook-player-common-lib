﻿package com.events 
{
	import flash.events.Event;
	import flash.geom.Matrix;
	
	/**
	 * ...
	 * @author Pawel Kolodziej
	 */
	public class TransformEvent extends Event 
	{
		
		public static var MATRIX_CHANGE:String = "src.com.events.matrixChange";
		public var matrix:Matrix;
		
		public function TransformEvent(matrix:Matrix,  type:String, bubbles:Boolean = false, cancelable:Boolean = false) 
		{ 
			super(type, bubbles, cancelable);
			this.matrix = matrix;
		} 
		
		public override function clone():Event 
		{ 
			return new TransformEvent(matrix, type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("TransformEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}