﻿package  com.ydp.events
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author 
	 */
	public class CalculatorEvent extends Event
	{
		public static const SWITCH_TO_SCIENTIFIC:String = "switch_to_scientific";
		public static const SWITCH_TO_STANDARD:String = "switch_to_standard";
		
		public function CalculatorEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false ) {
			super(type, bubbles, cancelable);
		}				
	}
	
}