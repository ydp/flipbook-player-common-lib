﻿package  com.ydp.events
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author 
	 */
	public class AddonEvent extends Event
	{
		public static const ADDON_CLOSE:String = "AddonEventClose";
		public static const ADDON_START_DRAG:String = "AddonEventStartDrag";
		public static const ADDON_STOP_DRAG:String = "AddonEventStopDrag";
		public function AddonEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false ) {
			super(type, bubbles, cancelable);
		}
		
	}
	
}