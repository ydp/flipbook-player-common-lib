package com.ydp.utils {
	import flash.geom.Point;
	
	
	/**
	* @author Michal Achtel
	*/
	public class LineGeneralization {
	
		// 2D Vector functions
		private static function dot(u:Object, v:Object):Number {
			return ((u.x*v.x)+(u.y*v.y));
		}
		private static function norm2(v:Object):Number {
			return dot(v, v);
		}
		private static function norm(v:Object):Number {
			return Math.sqrt(norm2(v));
		}
		private static function d2(u:Object, v:Object):Number {
			return norm2(minusNew(u, v));
		}
		private static function d(u:Object, v:Object):Number {
			return norm(minusNew(u, v));
		}
		private static function addNew(v:Object, a:Object):Object {
			return {x:v.x+a.x, y:v.y+a.x};
		}
		private static function multiNew(v:Object, a:Number):Object {
			return {x:v.x*a, y:v.y*a};
		}
		private static function minusNew(v:Object, a:Object):Object {
			return {x:v.x-a.x, y:v.y-a.y};
		}

		public static function simplify(pts:Array, tol:Number, stages:Number):Array {
			var pv:Number = 0;
			var n:Number = pts.length;
			var tol2:Number = tol*tol;
			var pts1:Array = new Array();
			var used:Array = new Array();
			// STAGE 1.  ptsertex Reduction within tolerance of prior vertex cluster
			pts1.push(pts[int(0)]);
			for (var i:int = 1; i<n; i++) {
				if (d2(pts[i], pts[pv])<tol2) {
					continue;
				}
				pts1.push(pts[i]);
				pv = i;
			}
			if (pv<n-1) {
				pts1.push(pts[n-1]);
			}
			if (stages>1) {
				// STAGE 2.  Douglas-Peucker polyline simplification 
				used[int(0)] = used[pts1.length-1]=true;
				simplifyDP(pts1, tol, 0, pts1.length-1, used);
				var pts2:Array = new Array();
				for (i=0; i<used.length; i++) {
					if (used[i]) {
						pts2.push(pts1[i]);
					}
				}
				return pts2;
			} else {
				return pts1;
			}
		}
		private static function simplifyDP(pts:Array, tol:Number, start:Number, end:Number, used:Array):void {
			if (end<=start+1) {
				return;
			}
			//     
			var maxi:Number = start;
			var maxd2:Number = 0;
			var tol2:Number = tol*tol;
			var line:Array = [pts[start], pts[end]];
			var u:Object = minusNew(line[int(1)], line[int(0)]);
			var cu:Number = dot(u, u);
			//
			for (var i:int = start; i<end; i++) {
				var w:Object = minusNew(pts[i], line[int(0)]);
				var cw:Number = dot(w, u);
				var dv2:Number;
				if (cw<=0) {
					dv2 = d2(pts[i], line[int(0)]);
				} else if (cu<=cw) {
					dv2 = d2(pts[i], line[int(1)]);
				} else {
					var b:Number = cw/cu;
					var Pb:Object = addNew(line[int(0)], multiNew(u, b));
					dv2 = d2(pts[i], Pb);
				}
				if (dv2<=maxd2) {
					continue;
				}
				maxi = i;
				maxd2 = dv2;
			}
			if (maxd2>tol2) {
				used[maxi] = true;
				simplifyDP(pts, tol, maxi, end, used);
				simplifyDP(pts, tol, start, maxi, used);
			}
			return;
		}
	}	
}