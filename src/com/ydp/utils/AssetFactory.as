﻿package com.ydp.utils {
	
	import flash.display.DisplayObject;
	import flash.utils.Dictionary;
	import pl.ydp.nexl.Nexl;
	import pl.ydp.nexl.INexlExtensionDescription;
	
	/**
	* creates asset for given id, 
	* @author Michał Samujło
	*/
	public class AssetFactory
	{
		public static function getAsset( id ):DisplayObject {
			var exts:Array = Nexl.library.list( { baseClass:"flash.display.MovieClip", id: id } );
			try {
				if (exts.length) {
					return exts[0].create();
				}
			} catch (e:Error) {
				trace(e);
			}
			return null;
		}
		
	}

	
}