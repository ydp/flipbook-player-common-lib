﻿package com.ydp.virtualKeyboard
{
	import flash.events.IEventDispatcher;
	
	/**
	 * ...
	 * @author Jarek Dziedzic
	 */
	
	public interface IKeyboard 
		extends IEventDispatcher
	{
		function init(keyboardLayout:String):void; // "US", "DE", "PL"
		function release():void;
	}
	
}