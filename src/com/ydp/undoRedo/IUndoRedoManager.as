package com.ydp.undoRedo
{
	import flash.events.IEventDispatcher;
	/**
	 * Interface for UndoRedoManager . 
	 * @see com.ydp.UndoRedoManager 
	 */
	 
	public interface IUndoRedoManager extends IEventDispatcher
	{
		/**
		 * Number of commands kept in history
		 */
		function set maxLength(value:uint):void;
		function get maxLength():uint;
		
		/**
		 *  'undoEnabled' 'redoEnabled' getters.
		 * 
		 */
		function get redoEnabled ():Boolean;
		
		
		function get undoEnabled ():Boolean;
		
		/**
		 * Executes undo. 
		 */
		function undoLastCommand( updateView:Boolean = true ):void;
		
		/**
		 * Executes redo. 
		 */
		function redoLastCommand( updateView:Boolean = true ):void;
		
		/**
		 * Allows to add undoable/redoable command to the list.
		 * If you use 'addCommand' method while 'getRedoList().length' is greater than 0, 
		 * all redo commands will be removed from UndoRedoMaganager and 'm_activeIndex' will
		 * point at the command that is currently being added.
		 * @param cmd command to add to the history.
		 */
		function addCommand( cmd:IUndoRedoCommand ):void;
		
		/**
		 * Returns Array of commands ( items type - IUndoRedoCommand )
		 * @see IUndoRedoCommand
		 */
		function getUndoList ():Array;
		
		/**
		 * Returns Array of commands ( items type - IUndoRedoCommand )
		 * @see IUndoRedoCommand
		 */
		function getRedoList ():Array;
		
		/**
		 * Clear all commands stored in history
		 * @see IUndoRedoCommand
		 */
		function clear ():void;
	}
}
	
	