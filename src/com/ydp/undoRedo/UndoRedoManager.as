package com.ydp.undoRedo
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;	
	/**
	 * Class for undo redo management. 
	 * Operations that are added to UndoRedoManager list should be implemented as seperate commands
	 * Check IUndoRedoCommand interface for more info.   
	 * @see pl.ydp.editor.commands.IUndoRedoCommand
	 */
	
	public class UndoRedoManager extends EventDispatcher implements IUndoRedoManager
	{
		
		/**
		 * Holds undo/redo commands list.
		 */
		protected var m_commandsArray:Array = new Array ();
		
		/**
		 * Index to the 'latest' command ( = next to undo ) 
		 */
		protected var m_activeIndex:int = -1;
		
		/**
		 * undo/redo history length 
		 */
		protected var m_levels:int = 10;
		
		
		/**
		 * Varibale is set to 'false' if there is no command to UNDO
		 * Can be use as source for data binding    
		 */

		public var _undoEnabled:Boolean = false;
		
		/**
		 * Varibale is set to 'false' if there is no command to REDO
		 * Can be use as source for data binding    
		 */

		public var _redoEnabled:Boolean = false;
		
		/**
		 * Property is used as type for "ITEM_CLICKED" UndoRedoListComponent Event
		 * @see UndoRedoListComponent  
		 */
		public const ITEM_CLICKED:String  = "UR_listItemClicked";
		
		/**
		 * Class contructor
		 */
		public function UndoRedoManager(){
			
		}
		
		/**
		 * Executes undo. 
		 */
		public function undoLastCommand( updateView:Boolean = true ):void {
			if ( m_activeIndex > -1 ) {				
				IUndoRedoCommand( m_commandsArray[m_activeIndex] ).undo( updateView );
				m_activeIndex --;
			}
			updateEnabled();
			historyChanged();
		}
		
		/**
		 * Executes redo. 
		 */
		public function redoLastCommand( updateView:Boolean = true ):void {
			if ( m_activeIndex < m_commandsArray.length -1 ) {				
				m_activeIndex ++;				
				IUndoRedoCommand( m_commandsArray[m_activeIndex] ).redo( updateView );
			}
			updateEnabled();
			historyChanged();
		}
		
		/**
		 * Allows to add undoable/redoable command to the list.
		 * If you use 'addCommand' method while 'getRedoList().length' is greater than 0, 
		 * all redo commands will be removed from UndoRedoMaganager and 'm_activeIndex' will
		 * point at the command that is currently being added.
		 * @param cmd command to add to the history.
		 */
		public function addCommand( cmd:IUndoRedoCommand ):void {			
			if ( m_activeIndex > -1 ){
				m_commandsArray = m_commandsArray.slice( 0, m_activeIndex +1);
			}else if ( m_activeIndex == -1 ){
				m_commandsArray = new Array();
			}
			
			m_commandsArray.push( cmd );
			if ( m_commandsArray.length > maxLength ){
				delete m_commandsArray.shift();
			}
			
			m_activeIndex = m_commandsArray.length - 1;
			updateEnabled ();
			historyChanged();
		}
		
		/**
		 * Returns Array of commands ( items type - IUndoRedoCommand )
		 * @see IUndoRedoCommand
		 */
		public function getUndoList ():Array{
			var resultArr:Array = new Array ();
			if ( m_activeIndex > -1 ){
				resultArr = m_commandsArray.slice( 0, m_activeIndex+1);
			}
			resultArr.reverse();
			return resultArr;
		}
		
		/**
		 * Returns Array of commands ( items type - IUndoRedoCommand )
		 * @see IUndoRedoCommand
		 */
		public function getRedoList ():Array{
			var resultArr:Array = new Array ();
			if ( m_commandsArray.length && m_activeIndex < m_commandsArray.length -1 ){
				resultArr = m_commandsArray.slice( m_activeIndex+1, m_commandsArray.length );
			}
			return resultArr;
		}
		
		/**
		 * Updates '_undoEnabled' '_redoEnabled' properties.
		 * Call this method always after modifying 'm_commandsArray' and 'm_activeIndex' variables
		 */
		protected function updateEnabled ():void{
			_undoEnabled = ( m_activeIndex == -1 ) ? false : true;
			_redoEnabled = ( m_activeIndex == m_commandsArray.length-1 ) ? false : true;
		}	
		
		/**
		 * Clear all commands stored in history
		 * @see IUndoRedoCommand
		 */
		public function clear ():void {
			m_commandsArray = new Array();
			m_activeIndex = -1;
			historyChanged();
		}
		
		public function set maxLength(value:uint):void {
			if (value>0) {
				m_levels = value;
			}
		}
		
		public function get maxLength():uint {
			return m_levels;
		}
		
		
		public function get redoEnabled ():Boolean {
			return _redoEnabled;
		}
		
		public function get undoEnabled ():Boolean {
			return _undoEnabled;
		}
		
		protected function historyChanged():void {
			dispatchEvent(new Event(Event.CHANGE));
		}
	}
}