package com.ydp.undoRedo
{
	public interface ICommand
	{
		/**
		 * method executs command   
		 */
		 
		function execute():void;
	}
}