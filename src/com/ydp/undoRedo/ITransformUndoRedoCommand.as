package com.ydp.undoRedo
{
	/**
	 * Interface for undo(redo)able commands. 
	 * @see pl.ydp.editor.core.undoredo.UndoRedoManager 
	 */
	 
	public interface ITransformUndoRedoCommand extends ICommand, IUndoRedoCommand
	{	
		/**
		 * taget's id which is transforming
		 */
		
		function get id():Number;
		
		function set id(n:Number):void;
		
	}
}
	