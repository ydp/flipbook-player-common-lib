package com.ydp.undoRedo
{
	/**
	 * Interface for undo(redo)able commands. 
	 * @see pl.ydp.editor.core.undoredo.UndoRedoManager 
	 */
	 
	public interface IUndoRedoCommand extends ICommand
	{	
		/**
		 * undo executed command   
		 */
		function undo(updateView:Boolean = true):void;
		
		
		/**
		 * redo command   
		 */
		
		function redo(updateView:Boolean = true):void;
		
		/**
		 * info about modification    
		 */
		
		function get info():String;
		
		/**
		 * target added to command
		 */
		
		function get target():*;
	}
}
	