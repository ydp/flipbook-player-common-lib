package com.ydp.media.model {
    public class SoundVO {
        private var _soundURL:String;
        private var _length:Number;

        public function SoundVO(soundURL:String, length:Number) {
            _soundURL = soundURL;
            _length = length;
        }

        public function get length():Number {
            return _length;
        }

        public function set length(value:Number):void {
            _length = value;
        }

        public function get soundURL():String {
            return _soundURL;
        }

        public function set soundURL(value:String):void {
            _soundURL = value;
        }
    }
}
