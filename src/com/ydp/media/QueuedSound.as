package com.ydp.media {

    import com.ydp.media.model.SoundVO;
    
    import flash.events.EventDispatcher;
    import flash.events.IOErrorEvent;
    import flash.media.Sound;
    import flash.net.URLRequest;

    public class QueuedSound extends EventDispatcher {
        private var _sound:Sound;
        private var _timePosition:Number;
        private var _length:Number;

        public function QueuedSound(soundVO:SoundVO, timePosition:Number) {
            _length = soundVO.length;
            _timePosition = timePosition;

            _sound = new Sound();
            _sound.addEventListener(IOErrorEvent.IO_ERROR, sound_errorHandler);
            _sound.load(new URLRequest(soundVO.soundURL));
        }

        private function sound_errorHandler(event:IOErrorEvent):void {
            throw new Error("Can't load sound file " + _sound.url);
        }

        public function get length():Number {
            return _length;
        }

        public function get sound():Sound {
            return _sound;
        }

        public function get timePosition():Number {
            return _timePosition;
        }
    }
}
