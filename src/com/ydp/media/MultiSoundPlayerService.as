package com.ydp.media {
    import com.ydp.media.events.MultiSoundPlayerEvent;
    import com.ydp.media.model.SoundVO;
    
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.TimerEvent;
    import flash.media.Sound;
    import flash.media.SoundChannel;
    import flash.utils.Timer;

    public class MultiSoundPlayerService extends EventDispatcher {

        private var _sounds:Vector.<QueuedSound> = new Vector.<QueuedSound>();
        private var _currentSound:QueuedSound;
        private var _soundChannel:SoundChannel;
        private var _playTimer:Timer = new Timer(250);
        private var _totalTime:Number = 0;

        public function setSounds(soundVOs:Vector.<SoundVO>):void
        {
            stop();

            _sounds.length = 0;
            _totalTime = 0;
            for each (var soundVO:SoundVO in soundVOs)
            {
                _sounds.push(new QueuedSound(soundVO, _totalTime));
                _totalTime += soundVO.length;
            }
        }

        public function play(startTime:Number = 0):void
        {
            stop();

            _currentSound = getSoundByTime(startTime);

            _playTimer.addEventListener(TimerEvent.TIMER, playTimer_tickHandler);
            _playTimer.start();

            playPart(_currentSound.sound, startTime - _currentSound.timePosition);
        }

        public function stop():void
        {
            if (_soundChannel)
            {
                _soundChannel.stop();
                _playTimer.stop();
                removeEventListeners();
            }
        }

        private function removeEventListeners():void {
            _playTimer.removeEventListener(TimerEvent.TIMER, playTimer_tickHandler);
            _soundChannel.removeEventListener(Event.SOUND_COMPLETE, soundChannel_soundCompleteHandler);
        }

        private function playTimer_tickHandler(event:TimerEvent):void {
            dispatchEvent(new MultiSoundPlayerEvent(MultiSoundPlayerEvent.PLAY_PROGRESS,
                    _currentSound.timePosition + _soundChannel.position,
                    _totalTime));
        }

        private function playPart(part:Sound, startTime:Number = 0):void
        {
            _soundChannel = part.play(startTime);
            _soundChannel.addEventListener(Event.SOUND_COMPLETE, soundChannel_soundCompleteHandler);
        }

        private function soundChannel_soundCompleteHandler(event:Event):void {
            _currentSound = getNextSound();

            if (_currentSound)
            {
                playPart(_currentSound.sound);
            }
            else
            {
                removeEventListeners();
                dispatchEvent(new MultiSoundPlayerEvent(MultiSoundPlayerEvent.PLAY_COMPLETE, _totalTime, _totalTime));
            }
        }

        private function getNextSound():QueuedSound {
            return getSoundByTime(_currentSound.timePosition + _currentSound.length);
        }

        private function getSoundByTime(startTime:Number = 0):QueuedSound
        {
            for each (var sound:QueuedSound in _sounds)
            {
                if (startTime < sound.timePosition + sound.length)
                {
                    return sound;
                }
            }

            return null;
        }
    }
}
