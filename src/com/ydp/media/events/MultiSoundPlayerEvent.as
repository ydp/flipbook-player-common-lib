package com.ydp.media.events {
    import flash.events.Event;

    public class MultiSoundPlayerEvent extends Event {

        public static const PLAY_COMPLETE:String = "MultiSoundPlayerEvent.PLAY_COMPLETE";
        public static const PLAY_PROGRESS:String = "MultiSoundPlayerEvent.PLAY_PROGRESS";

        private var _playedTime:Number;
        private var _totalTime:Number;

        public function MultiSoundPlayerEvent(type:String, playedTime:Number, totalTime:Number, bubbles:Boolean = false, cancelable:Boolean = false) {
            _playedTime = playedTime;
            _totalTime = totalTime;
            super(type, bubbles, cancelable);
        }

        public function get totalTime():Number {
            return _totalTime;
        }

        public function get playedTime():Number {
            return _playedTime;
        }
    }
}
