﻿package com.ydp.resources
{
	
	/**
	 * Interface for classes that provide access to property files
	 * @author Michał Samujło
	 */
	public interface IResourceManager {
		function getProperty(name:String):*;
		function addBundle(data:*):void;
		//function loadBundle(url:String):void;
	}
	
}