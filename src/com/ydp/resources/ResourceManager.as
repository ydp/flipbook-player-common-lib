﻿package com.ydp.resources
{
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	import flash.xml.XMLNode;
	
	/**
	 * Provides access to properties defined in external files.
	 * @version 0.1 xml properties support
	 * @author Michał Samujło
	 */
	public class ResourceManager implements IResourceManager 
	{
		private var _properties:Dictionary;
		
		function ResourceManager():void {
			_properties = new Dictionary();
		}
		
		/**
		 * @param	key
		 * @return 	property value for given key or "<"+key+">" if value is null
		 */
		public function getProperty(key:String):* {
			var prop:* = _properties[ key ];
			if (prop==null)
				prop = "<" + key + ">";
			return prop
		}
		
		public function addBundle(bundle:*):void {
			if (bundle is XML) {
				var props:XML = XML( bundle );
				for (var i:int = 0; i < props.p.length() ; i++ ) {
					var prop:XML = props.p[i];
					var text:String;
					text = "";					
					if (prop.hasComplexContent()) {						
						var children:XMLList = prop.children();
						for (var j:int = 0; j < children.length(); j++) {
							text += (children[j] as XML).toXMLString() + " ";
						}						
					}else {
						text = prop.text()
					}					
					_properties[ prop.@name.toString() ] = text ;
				}
			}
		}
		
	}
	
}