﻿package  com.ydp.view.mediaPlayer
{
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author loleszczak
	 */
	public interface IFBMediaPlayer {
		function load(source:Object, type:String):void;
		function play():void;
		function stop():void;
		function pause():void;
		function setPosition(value:Object):void;
		function getPosition():Object;
		function unload():void;
		function setSize(width:Number, height:Number):void;
		function getSize():Rectangle;		
		function getContentBaseSize():Rectangle;
		function getPreferredSize():Rectangle;
		function getContentBounds():Rectangle;
		function getMinSize():Rectangle;
		function set avm1ProxyPath(value:String):void;
	}
	
}