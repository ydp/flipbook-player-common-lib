﻿package  com.ydp.view.mediaPlayer
{
	
	/**
	 * ...
	 * @author loleszczak
	 */
	public class FBMediaPlayerTypes 
	{
		public static const IMAGE:String = "image";
		public static const GLOSSARY:String = "glossary";
		public static const SLIDESHOW:String = "slideshow";
		public static const AUDIO:String = "audio";
		public static const SWF:String = "swf";
		public static const SWF_AWM1:String = "swf_avm1";
		public static const FLV_VIDEO:String = "flv_video";
		public static const YOU_TUBE:String = "youTube";
		
		public static const ZOOM:String = "zoom";
		public static const UTOPIA:String = "content_link";
		public static const LEO:String = "leo_content_link";
		public static const MULTIFILE:String = "multifile";
		
		public function FBMediaPlayerTypes() 
		{
			
		}
		
	}
	
}