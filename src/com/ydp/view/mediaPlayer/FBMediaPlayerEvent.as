﻿package  com.ydp.view.mediaPlayer
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author 
	 */
	public class FBMediaPlayerEvent extends Event 
	{
		public static const CONTENT_LOADED:String = "contentLoaded";
		public static const CONTENT_LOADING_FAILED:String = "contentLoadingFailed";
		
		public static const POSITION_CHANGED:String = "positionChanged";
		public static const STATE_CHANGED:String = "stateChanged";
		
		public static const PLAY_MEDIA:String = "play";
		public static const STOP_MEDIA:String = "stop";
		public static const PAUSE_MEDIA:String = "pause";		
		public static const MUTE_MEDIA:String = "mute";
		public static const UNMUTE_MEDIA:String = "unmute";
		public static const FULLSCREEN:String = "fullscreen";
		public static const EXIT_FULLSCREEN:String = "exit_fullscreen";
		public static const VOLUME_CHANGED:String = "volumeChanged";
		
		public function FBMediaPlayerEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new FBMediaPlayerEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("FBMediaPlayerEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}