﻿package com.ydp.view
{
	import flash.events.IEventDispatcher;
	
	/**
	 * ...
	 * @author loleszczak
	 */
	[Event(name="AddonEventClose", type="AddonEvent")]
	[Event(name="AddonEventStartDrag", type="AddonEvent")]
	[Event(name="AddonEventStopDrag", type="AddonEvent")]
	public interface IYAddon extends IEventDispatcher{
		function init(param:Object = null):void;
		function setSize(width:Number, height:Number):void;
		function setPosition(x:Number, y:Number):void;		
		function deinit(param:Object = null):void;
		function get maxWidth():Number;
		function set maxWidth(value:Number):void;
		function get maxHeight():Number;
		function set maxHeight(value:Number):void;
	}
	
}