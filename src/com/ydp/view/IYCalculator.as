﻿package com.ydp.view
{
	import flash.display.DisplayObjectContainer;
	
	/**
	 * ...
	 * @author loleszczak
	 */
	public interface IYCalculator extends IYAddon{
		function get view():DisplayObjectContainer;
	}
	
}