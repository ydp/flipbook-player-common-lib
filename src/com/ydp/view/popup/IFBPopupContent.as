﻿package  com.ydp.view.popup
{
	import flash.events.IEventDispatcher;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author loleszczak
	 */
	public interface IFBPopupContent extends IEventDispatcher
	{
		function setSize(w:Number, h:Number):void;
		function getSize():Rectangle;
		function getContentBaseSize():Rectangle;
		function getPreferredSize():Rectangle;
		function getContentBounds():Rectangle;
		function getMinSize():Rectangle;
		function requestClose(canCloseHandler:Function):void;
		function unload():void;
		function get isResizable():Boolean;
		function getID():String;
		function pause():void;
	}
	
}