package com.ydp.view 
{
	import flash.display.InteractiveObject;
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.utils.clearTimeout;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	
	/**
	 * ...
	 * @author loleszczak
	 */
	public class DoubleClickDecorator extends EventDispatcher{
		
		protected var _clip:InteractiveObject;
		protected var _mcClip:MovieClip;
		protected var _frames:uint = 3;
		protected var _framesPerState:uint = 3;
		protected var _statesCount:uint = 1;
		
		protected var _doubleClickEndabled:Boolean = false;
		protected var _oneClickTimer:uint;
		protected var _selected:Boolean = false;
		protected var _state:uint = 0;
				
		public static const doubleClickMaxDelay:uint = 250;
		
		public function DoubleClickDecorator(skinMc:InteractiveObject, 			
			framesPerState:uint = 3, statesCount:uint = 1, doubleClickEnabled:Boolean = false) {
			_clip = skinMc;			
			_mcClip = null;			
			_doubleClickEndabled = doubleClickEnabled;
			if (_clip is MovieClip) {
				_mcClip = _clip as MovieClip;
				_mcClip.buttonMode = true;
				_mcClip.mouseChildren = false;
				_mcClip.useHandCursor = true;
			}			
			
			_framesPerState = framesPerState;			
			_statesCount = statesCount;
			_frames = _framesPerState * _statesCount;
			
			_clip.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			_clip.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);	
			_clip.addEventListener(MouseEvent.ROLL_OVER, onMouseOver);				
			_clip.addEventListener(MouseEvent.ROLL_OUT, onMouseOut);				
			
			gotoFrame(1);			
		}
		
		protected function onMouseOver(event:MouseEvent):void {												
			if (_mcClip) {
				gotoStateFrame(2);
			}
		}
		
		protected function onMouseOut(event:MouseEvent):void {						
			if (_mcClip) {
				gotoStateFrame(1);
			}
		}
		
		
		protected function onMouseDown(event:MouseEvent):void {			
			dispatchEvent(new MouseEvent(MouseEvent.MOUSE_DOWN));		
			if (_mcClip) {
				gotoStateFrame(3);
			}
		}
		
		protected function onMouseUp(event:MouseEvent):void {			
			if (_oneClickTimer) {				
				 clearTimeout(_oneClickTimer);				
				_oneClickTimer = 0;
				onDoubleClick();
			}else {
				if(_doubleClickEndabled)
					_oneClickTimer = setTimeout(onOneClick, DoubleClickDecorator.doubleClickMaxDelay);
				else
					onOneClick();
			}
		}
		
		protected function onOneClick():void {			
			if (_oneClickTimer)
				 clearTimeout(_oneClickTimer);
			_oneClickTimer = 0;			
			selected = !_selected;			
			dispatchEvent(new MouseEvent(MouseEvent.CLICK));			
		}
		
		protected function onDoubleClick():void {			
			dispatchEvent(new MouseEvent(MouseEvent.DOUBLE_CLICK));
		}
		
		public function get selected():Boolean {
			return _selected;
		}
		public function set selected(value:Boolean):void {
			_selected = value;
			state = value ? 1 : 0;
		}
		
		public function get state():uint {
			return _state;
		}
		
		public function set state(value:uint):void {
			_state = Math.min(_statesCount-1, Math.max(0, value));			
			if (_mcClip) {
				gotoFrame(_state * _framesPerState+1);
			}
			_selected = _state > 0;
		}
		
		public function cleanUp():void {
			_clip.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown, false);
			_clip.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp, false);	
			_clip.removeEventListener(MouseEvent.ROLL_OVER, onMouseOver, false);	
			_clip.removeEventListener(MouseEvent.ROLL_OUT, onMouseOut, false);
			_clip = null;
			_mcClip = null;
		}
		
		protected function gotoFrame(nr:uint):void {
			if (_mcClip) {
				var frame:int = Math.min(nr, _mcClip.totalFrames);
				_mcClip.gotoAndStop(frame);								
			}
		}
		
		protected function gotoStateFrame(nr:uint):void {
			
			var frame:int;
			if (_mcClip && nr<=_framesPerState) {				
				frame = Math.min(nr+_state * _framesPerState, _mcClip.totalFrames);
				_mcClip.gotoAndStop(frame);				
			}			
		}
		
		public function get clip():InteractiveObject {
			return _clip;
		}
		
		
		
	}
	
}