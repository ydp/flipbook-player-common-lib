﻿/**
* ...
* @author Michal Samujlo
* @version 0.1
*/
package com.ydp.view {
	
	import flash.display.MovieClip;
	import flash.errors.IllegalOperationError;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.events.Event;
	
	/**
	 * Adds checkbox behavior to movie clip
	 * Movie clip should 6 frames not selected normal, over, down 
	 * and selected normal, over, down
	 */
	public class CheckBoxDecorator extends EventDispatcher
	{
		public static const STATE_CHANGED:String ="stateChanged";
		public static const DOWN:String = "down";
		public static const OUT:String = "out";

		// decorated movie clip
		public var clip:MovieClip;

		private var _state:int = 0;
		private var _wasDown:Boolean;
		private var _wasUp:Boolean = false;
		private var _enabled:Boolean = true;		
		
		// TODO Decorator powinien dodać skinMc do swojej display listy i dodawać się do parenta skinMc
		public function CheckBoxDecorator( skinMc:MovieClip )
		{
			if (!(skinMc is MovieClip)) {
				throw new ArgumentError("parameter is not a MovieClip")
			}
			clip = skinMc;
			this.state = 0;
			clip.buttonMode = true;
			clip.useHandCursor = true;
			clip.mouseChildren = false;
			clip.addEventListener(MouseEvent.ROLL_OVER, mOver );
			clip.addEventListener(MouseEvent.ROLL_OUT, mOut );
			clip.addEventListener(MouseEvent.MOUSE_DOWN, mDown );
			clip.addEventListener(MouseEvent.MOUSE_UP, mUp );
			clip.addEventListener(MouseEvent.CLICK, mClick);
			
		}
		
		public function mOver(event:MouseEvent):void
		{
			if (!_enabled) return;
			var fnr:int = Math.min((this.state * 3) + 2, clip.totalFrames);
			clip.gotoAndStop(fnr);
		}
		
		
		public function mOut(event:MouseEvent):void
		{
			if (!_enabled) return;
			var fnr:int = Math.min((this.state * 3) + 1, clip.totalFrames);
			clip.gotoAndStop(fnr);
			dispatchEvent( new Event(OUT) );
		}
		
		public function mDown(event:MouseEvent):void
		{
			if (!_enabled) return;
			_wasDown = true;
			_wasUp = false;
			var fnr:int = Math.min((this.state * 3) + 3, clip.totalFrames);	
			clip.gotoAndStop(fnr)
			dispatchEvent( new Event(DOWN) );
		}
		
		public function mUp(event:MouseEvent):void {
			if (!_wasDown || !_enabled) return;
			_wasDown = false;
			state = (state == 0)? 1 : 0;
			dispatchEvent( new Event(STATE_CHANGED) );						
			_wasUp = true;
		}
		
		public function mClick(event:MouseEvent):void {
			if (!_enabled) return;
			if (!_wasUp) {
				_wasDown = false;
				state = (state == 0)? 1 : 0;
				dispatchEvent( new Event(STATE_CHANGED) );				
			}
			_wasUp = false;	
			
		}

		// getters / setters
		public function set enabled(st:Boolean):void
		{
			var fr:int = st? 1 : 7;
			if (fr > clip.totalFrames) {
				fr = 1;
			}
			clip.gotoAndStop( fr );
			_enabled = st;
			clip.useHandCursor = st;
		}
		
		public function set state (x:int):void 
		{
            this._state = x;    
			var fnr:int = Math.min((3 * this._state) + 1, clip.totalFrames);
			clip.gotoAndStop(fnr);
        }
		
        public function get state():int
		{ 
            return this._state; 
        }
		
		public function set selected(b:Boolean):void {
			state = (b)? 1 : 0;
		}
		public function get selected():Boolean {
			return (state==1);
		}
	
	}
}
