﻿package  com.ydp.view
{
	import flash.display.Sprite;
	import com.ydp.view.TooltipDecorator;
	import flash.display.Sprite;
	import com.ydp.style.IStyleServer;
	import com.ydp.style.IStyleClient;
	import com.ydp.style.StyleSelectorInfo;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.InteractiveObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import pl.ydp.nexl.Nexl;
	import pl.ydp.nexl.INexlExtensionDescription;
	
	/**
	 * ...
	 * @author loleszczak
	 */
	public class Tooltip extends Sprite
	{
		private var tf:TextField;
		private var _mc:InteractiveObject;
		
		public function Tooltip(mc:InteractiveObject, tooltip:String, autoSize:String = "") {						
			_mc = mc;
			var exts:Array = Nexl.library.list( { type: "com.ydp.style.IStyleServer" } );
			var ext:INexlExtensionDescription = exts[0];
			var impl:IStyleServer = ext.create() as IStyleServer;
			
			var styleSelector:StyleSelectorInfo = new StyleSelectorInfo();
			var client:IStyleClient = impl.findNearestStyleClient( _mc );
			if (client) {
				styleSelector.styleType = client.styleType;
				styleSelector.styleClass = client.styleClasses.concat();
				styleSelector.styleIds = client.styleIds.concat();
				styleSelector.stylePseudoClass = client.stylePseudoClasses.concat();
			}
			styleSelector.stylePseudoClass.push( "tooltip" )
			var styles:Object = impl.getStyleDescriptionForSelector( styleSelector );
			
			var embedFont:Boolean = String(styles.embedFont) == "true";						
			tf = new TextField();
			tf.mouseEnabled = false;
			tf.autoSize = autoSize ? autoSize : TextFieldAutoSize.CENTER;
			tf.border = true;
			tf.embedFonts = embedFont;
			tf.borderColor = (styles.borderColor)? styles.borderColor : 0;
			tf.background = true;
			tf.backgroundColor = (styles.backgroundColor)? styles.backgroundColor : 0xFFFFFF;
			
			
			var tfm:TextFormat = new TextFormat();
			tfm.font = (styles.fontFamily)? styles.fontFamily : "Arial";
			tfm.size = (styles.fontSize)? styles.fontSize : 10;
			tfm.color = (styles.color)? styles.color : 0;
			tfm.leftMargin = 2;
			tfm.rightMargin = 2;
			tf.defaultTextFormat = tfm;
			
			tf.htmlText = tooltip;
			tf.height = tf.textHeight + 4;
			tf.mouseEnabled = false;
			tf.x = 0;
			tf.y = 0;
			mouseEnabled = false;
			addChild(tf);
		}
	}
	
}