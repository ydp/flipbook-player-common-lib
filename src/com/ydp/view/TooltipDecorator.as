﻿package com.ydp.view 
{
	import com.ydp.view.TooltipDecorator;
	import flash.display.Sprite;
	import com.ydp.style.IStyleServer;
	import com.ydp.style.IStyleClient;
	import com.ydp.style.StyleSelectorInfo;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.InteractiveObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.setTimeout;
	import pl.ydp.nexl.Nexl;
	import pl.ydp.nexl.INexlExtensionDescription;
	
	/**
	* Adds tooltip behavior to MovieClip
	* @author Łukasz Oleszczak
	* @author Michał Samujło
	*/
	public class TooltipDecorator 
	{
		public static var displayTime:uint = 0; //ms
		public static var tooltipClass:Class = Tooltip;
		
		private var _mc:InteractiveObject;
		private var _text:Array;
		private var _tooltip:Object;
		
		private var _displayTime:Number = NaN;
		private var _styleClass:String;

		public function TooltipDecorator(mc:InteractiveObject, ... tooltips ):void {						
			if (!mc) {
				return;
			}
			mc.addEventListener( MouseEvent.MOUSE_OVER, showTooltip, false, 0, false );
			mc.addEventListener( MouseEvent.MOUSE_OUT, hideTooltip, false, 0, false );
			mc.addEventListener( MouseEvent.CLICK, changeTooltip, false, 0, false );
			this._mc = mc;			
			this._text = tooltips;
			if (_text[0] == null) {
				throw new Error("...no tooltips defined");
			}
		}	
		
		public function set styleClass(value:String):void {
			_styleClass = value;
		}

		private function changeTooltip(e:MouseEvent):void {			
			hideTooltip();
			showTooltip();
		}
		
		private function showTooltip(e:MouseEvent = null):void {						
			hideTooltip();
			if (_mc.stage) {				
				_tooltip = new tooltipClass(_mc, tooltip);
				var styleClass:String;
				var xOffset:Number = 0;
				var yOffset:Number = 0;
				var xOffsetPoint:String = "center";
				var yOffsetPoint:String = "bottom";
				if (_styleClass) {					
					var exts:Array = Nexl.library.list( { type: "com.ydp.style.IStyleServer" } );
					var ext:INexlExtensionDescription = exts[0];
					var impl:IStyleServer = ext.create() as IStyleServer;
					
					var styleSelector:StyleSelectorInfo = new StyleSelectorInfo(_styleClass);
					var styles:Object = impl.getStyleDescriptionForSelector(styleSelector);
					if (styles.hasOwnProperty("xoffset")) {
						xOffset = Number(styles["xoffset"]);
					}
					if (styles.hasOwnProperty("yoffset")) {
						yOffset = Number(styles["yoffset"]);
					}
					if (styles.hasOwnProperty("horizontaltooltipalign")) {
						xOffsetPoint = String(styles["horizontaltooltipalign"]);
					}
					if (styles.hasOwnProperty("verticaltooltipalign")) {
						yOffsetPoint = String(styles["verticaltooltipalign"]);
					}
				}
				
				
				var p:Point = new Point(_mc.width/2,0);
				p = _mc.localToGlobal(p);
				
				var px:Number = 0;
				var py:Number = 0;
				switch(xOffsetPoint) {
					case "right":
						px = _tooltip.width;
					break;
					case "left":					
						px = 0;
					break;
					case "center":
					default:
						px = _tooltip.width/2;
					break;	
				}
				switch(yOffsetPoint) {
					case "bottom":
						py =_tooltip.height;
					break;
					case "top":
						py = 0;
					break;
					case "center":
					default : 
						py = _tooltip.height/2;	
					break;
				}
				
				
				_tooltip.x = Math.max( (p.x +xOffset) - px, 2) ;				
				_tooltip.x = Math.min( (_tooltip.x ), _mc.stage.stageWidth - _tooltip.width - 2);				
				_tooltip.y = Math.max( (p.y +yOffset) - py, 2);
				_tooltip.y = Math.min( (_tooltip.y), _mc.stage.stageHeight -_tooltip.height - 2);								
				
							
				_mc.stage.addChild( _tooltip as DisplayObject);				
				if (tooltipDisplayTime > 0) {					
					setTimeout(hideTooltip, tooltipDisplayTime);
				}
			}
		}
		
		private function get tooltip():String {
			var mc:MovieClip = _mc as MovieClip;
			return (mc && (_text[1] is String) && (mc.currentFrame>=4))? _text[1] : _text[0];
		}
		
		private function hideTooltip(e:MouseEvent = null):void {
			if (_tooltip && _tooltip.parent) {				
				_tooltip.parent.removeChild( _tooltip as DisplayObject);
			}
			_tooltip = null;
		}
		
		public function unload():void {
			if(_mc){
				_mc.removeEventListener( MouseEvent.MOUSE_OVER, showTooltip, false );
				_mc.removeEventListener( MouseEvent.MOUSE_OUT, hideTooltip, false );
				_mc.removeEventListener( MouseEvent.CLICK, changeTooltip, false);					
				hideTooltip();
			}
			
			_mc = null;
		}
		
		public function set tooltipDisplayTime(value:Number):void {
			_displayTime = value;
		}
		
		public function get tooltipDisplayTime():Number{
			if (isNaN(_displayTime)) {
				return TooltipDecorator.displayTime;
			}else {
				return _displayTime;
			}
		}
	}
	
}