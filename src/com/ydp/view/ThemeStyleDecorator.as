﻿package com.ydp.view
{
	import com.ydp.style.IStyleServer;
	import com.ydp.style.StyleSelectorInfo;
	import com.ydp.utils.ColorUtil;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	import flash.text.TextField;
	
	import pl.ydp.nexl.INexlExtensionDescription;
	import pl.ydp.nexl.Nexl;
	
	/**
	 * ThemeStyleDecorator searches decorated movie clip for instances named 
	 * PrimaryTheme, SecondaryTheme or ContractTheme and applies to them color 
	 * transformations based on colors defined in global styles: primaryThemeColor,
	 * primaryThemeAlpha, secondaryThemeColor, secondaryThemeAlpha, contrastThemeColor, 
	 * contrastThemeAlpha, textThemeColor.
	 * 
	 * To avoid querying IStyleServer for each decorated movie clip, styles are initialized only once 
	 * and stored in static vars. This means IStyleServer instance must be ready when TSD is created!
	 * 
	 * @throws Error "IStyleServer not found" is thrown if IStyleServer implementation wasn't found
	 * @author loleszczak
	 */
	public class ThemeStyleDecorator {
		private static var _stylesInitialized:Boolean = false;
		private static var _styleServer:IStyleServer
		
		private static var _primaryThemeColor:int = -1;
		private static var _primaryThemeAlpha:Number = 1;
		
		private static var _secondaryThemeColor:int = -1;
		private static var _secondaryThemeAlpha:Number = 1;
		
		private static var _contrastThemeColor:int = -1;
		private static var _contrastThemeAlpha:Number = 1;
		
		private static var _textThemeColor:int = -1;
		private static var _textThemeAlpha:Number = 1;
		
		private var _maxThemeObjects:uint = 5;
		
		private var _target:DisplayObject;
		
		private var _originalColorInited:Boolean = false;
		private var _originalTextColor:uint = 0;
		
		public function ThemeStyleDecorator(target:DisplayObject) {
			_target = target;
			if(!_target){
				return;
			}
			initializeStyles();
			if (_target is MovieClip) {
				for (var i:int = 0; i < MovieClip(_target).totalFrames; i++){
					MovieClip(_target).addFrameScript(i, updateStyles );
				}
			} else if (_target is TextField) {
				if (ThemeStyleDecorator._textThemeColor>0) {
					TextField(_target).textColor = ThemeStyleDecorator._textThemeColor;
				} else {
					TextField(_target).textColor = _originalTextColor;
				}
			} else {
				applyTint( _target, ThemeStyleDecorator._primaryThemeColor, ThemeStyleDecorator._primaryThemeAlpha);
			}
		}
		
		private function initializeStyles():void
		{
			if (!ThemeStyleDecorator._stylesInitialized) {
				var exts:Array = Nexl.library.list( { type: "com.ydp.style.IStyleServer" } );
				if (exts.length == 0) {
					throw new Error("ThemeStyleDecorator::initializeStyles IStyleServer not found");
				}
				var ext:INexlExtensionDescription = exts[0];
				_styleServer = ext.create() as IStyleServer;
				if (!_styleServer) {
					throw new Error("ThemeStyleDecorator::initializeStyles IStyleServer not found");
				}
				stylesChanged();
				_stylesInitialized = true;
			}
			if (_styleServer) {
				_styleServer.addEventListener( Event.CHANGE, stylesChanged );
			}			
			applyStyles();
		}
		
		protected function stylesChanged(event:Event=null):void {
			var styleSelector:StyleSelectorInfo = new StyleSelectorInfo("presenterskin");
			var styles:Object = _styleServer.getStyleDescriptionForSelector( styleSelector );
			
			resetStyles();
			
			if (styles.hasOwnProperty("primaryThemeColor"))
				ThemeStyleDecorator._primaryThemeColor = styles.primaryThemeColor;
			if (styles.hasOwnProperty("primaryThemeAlpha"))
				ThemeStyleDecorator._primaryThemeAlpha = styles.primaryThemeAlpha;
			if (styles.hasOwnProperty("secondaryThemeColor"))
				ThemeStyleDecorator._secondaryThemeColor = styles.secondaryThemeColor;
			if (styles.hasOwnProperty("secondaryThemeAlpha"))
				ThemeStyleDecorator._secondaryThemeAlpha = styles.secondaryThemeAlpha;
			if (styles.hasOwnProperty("contrastThemeColor"))
				ThemeStyleDecorator._contrastThemeColor = styles.contrastThemeColor;
			if (styles.hasOwnProperty("contrastThemeAlpha"))
				ThemeStyleDecorator._contrastThemeAlpha = styles.contrastThemeAlpha;
			if (styles.hasOwnProperty("textThemeColor"))
				ThemeStyleDecorator._textThemeColor = styles.textThemeColor;
			
			if (event) {
				applyStyles();
			}
		}
		
		protected function applyStyles():void {
			if (_target is MovieClip) {
				updateStyles();
			} else if (_target is TextField) {
				if (!_originalColorInited) {
					_originalColorInited = true;
					_originalTextColor = TextField(_target).textColor;
				}
				if (ThemeStyleDecorator._textThemeColor!=-1) {
					TextField(_target).textColor = ThemeStyleDecorator._textThemeColor;
				} else {
					TextField(_target).textColor = _originalTextColor;
				}
			} else {
				applyTint( _target, ThemeStyleDecorator._primaryThemeColor, ThemeStyleDecorator._primaryThemeAlpha);
			}
		}
		
		protected function updateStyles():void {
			updateTheme("PrimaryTheme", _primaryThemeColor, _primaryThemeAlpha);
			updateTheme("SecondaryTheme", _secondaryThemeColor, _secondaryThemeAlpha);	
			updateTheme("ContrastTheme", _contrastThemeColor, _contrastThemeAlpha);
		}
		
		protected function resetStyles():void {
			ThemeStyleDecorator._primaryThemeColor = -1;
			ThemeStyleDecorator._primaryThemeAlpha = 1;
			ThemeStyleDecorator._secondaryThemeColor = -1;
			ThemeStyleDecorator._secondaryThemeAlpha = 1;
			ThemeStyleDecorator._contrastThemeColor = -1;
			ThemeStyleDecorator._contrastThemeAlpha = 1;
			ThemeStyleDecorator._textThemeColor = -1;
		}
		
		protected function updateTheme(themeName:String, tintColor:Number, tintAlpha:Number):void {
			var mc:MovieClip = _target[themeName];
			if (mc) {
				applyTint(mc, tintColor, tintAlpha);
			}
			for (var i:int = 0; i < _maxThemeObjects; i++){
				mc = _target[themeName+String(i)];
				if (mc) {
					applyTint(mc, tintColor, tintAlpha);
				}else {
					break;
				}
			}
		}

		protected function applyTint(target:DisplayObject, color:int, alpha:Number):void {
			var tint:ColorTransform = ColorUtil.setTint(color,alpha);
			target.transform.colorTransform = tint;
		}

	}
	
}