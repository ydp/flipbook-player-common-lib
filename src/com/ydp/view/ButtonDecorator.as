﻿/**
* ...
* @author Michal Samujlo
* @version 0.1
*/
package com.ydp.view {
	
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.events.Event;
	
	/**
	 * Adds button behavior to movie clip
	 * Movie clip should have 3 frames - normal,over,down
	 */
	public class ButtonDecorator extends EventDispatcher
	{		
		public static const CLICK:String = "click";
		public static const DOWN:String = "down";
		public static const OUT:String = "out";		
		public static const OVER:String = "over";
		
		public var clip:MovieClip;
		
		private var _wasDown:Boolean;
		private var _wasUp:Boolean = false;
		private var _enabled:Boolean = true;
		
		// TODO Decorator powinien dodać skinMc do swojej display listy i dodawać się do parenta skinMc
		public function ButtonDecorator( skinMc:MovieClip )
		{
			super();
			if (!(skinMc is MovieClip)) {
				throw new ArgumentError("parameter is not a MovieClip")
			}
			
			clip = skinMc;
			
			clip.gotoAndStop(1);
			clip.buttonMode = true;
			clip.useHandCursor = true;
			clip.mouseChildren = false;
			clip.addEventListener(MouseEvent.ROLL_OVER,mOver);
			clip.addEventListener(MouseEvent.ROLL_OUT,mOut);
			clip.addEventListener(MouseEvent.MOUSE_DOWN,mDown);			
			clip.addEventListener(MouseEvent.MOUSE_UP, mUp);
			clip.addEventListener(MouseEvent.CLICK, mClick);			
		}
		
		public function mOver(event:MouseEvent):void
		{
			if (!_enabled) return;
			clip.gotoAndStop(2);
			dispatchEvent( new Event(OVER) );
		}
		
		public function mOut(event:MouseEvent):void
		{
			if (!_enabled) return;
			clip.gotoAndStop(1)
			dispatchEvent( new Event(OUT) );
		}
		
		public function mDown(event:MouseEvent):void
		{
			if (!_enabled) return;
			_wasDown = true;
			_wasUp = false;
			clip.gotoAndStop(3);
			dispatchEvent( new Event(DOWN) );
		}

		public function mUp(event:MouseEvent):void
		{
			if (!_wasDown || !_enabled) return;
			_wasDown = false;			
			_wasUp = true;
			clip.gotoAndStop(2);
			dispatchEvent( new Event(CLICK) );			
		}
		
		public function mClick(event:MouseEvent):void {
			if (!_enabled) return;
			if (!_wasUp) {
				_wasDown = false;			
				clip.gotoAndStop(2);
				dispatchEvent( new Event(CLICK) );	
			}
			_wasUp = false;
		}

		public function set enabled(st:Boolean):void
		{
			var fr:int = st? 1 : 4;
			if (fr > clip.totalFrames) {
				fr = 1;
			}
			clip.gotoAndStop( fr );
			_enabled = st;
			clip.useHandCursor = st;
		}
		
		public function cleanUp():void {
			clip.removeEventListener(MouseEvent.ROLL_OVER,mOver);
			clip.removeEventListener(MouseEvent.ROLL_OUT,mOut);
			clip.removeEventListener(MouseEvent.MOUSE_DOWN,mDown);			
			clip.removeEventListener(MouseEvent.MOUSE_UP, mUp);	
		}
	}
}
