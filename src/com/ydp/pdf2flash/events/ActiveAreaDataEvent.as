﻿package com.ydp.pdf2flash.events {
	import com.ydp.pdf2flash.hotspots.IActiveArea;
	import flash.events.Event;
	
	public class ActiveAreaDataEvent extends Event {
		
		public static const ACTIVE_AREAS_LOADED:String = "activeAresLoaded";
		
		public var leftAreas:Array;
		public var rightAreas:Array;
		
		public function ActiveAreaDataEvent(lAreas:Array, rAreas:Array) {
			super(ACTIVE_AREAS_LOADED);
			this.leftAreas = lAreas;
			this.rightAreas = rAreas;
		}
		
		public override function clone():Event { 
			return new ActiveAreaDataEvent(this.leftAreas, this.rightAreas);
		} 	
		
		public override function toString():String { 
			return formatToString("ActiveAreaDataEvent", "type", "bubbles", "cancelable", "eventPhase", "area"); 
		}			
	}
	
}