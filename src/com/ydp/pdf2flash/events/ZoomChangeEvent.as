﻿package com.ydp.pdf2flash.events {
	import flash.events.*;
	
	public class ZoomChangeEvent extends Event {
		
		public static const ZOOM_CHANGED:String = "zoomChanged";
		
		public var scale:Number;
		public var minScale:Number;
		public var maxScale:Number;
		
		public function ZoomChangeEvent( scale:Number, minScale:Number = 1, maxScale:Number = 1) {
			super( ZOOM_CHANGED );
			this.scale = scale;
			this.minScale = minScale;
			this.maxScale = maxScale;
		}
		
		public override function clone():Event { 
			return new ZoomChangeEvent(scale, minScale, maxScale);
		} 
		
		public override function toString():String { 
			return formatToString("ZoomChangeEvent", "type", "bubbles", "cancelable", "eventPhase", "scale", "minScale", "maxScale");
		}		
	}
}
