package com.ydp.pdf2flash.events {
	import flash.events.Event;
	
	/**
	 * ...
	 * @author 
	 */
	public class OpenUrlEvent extends Event {
		
		public var url:String;
		public static const OPEN:String = "open";
		
		public function OpenUrlEvent(type:String, link:String, bubbles:Boolean=false, cancelable:Boolean=false) { 
			url = link;
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event { 
			return new OpenUrlEvent(type, url, bubbles, cancelable);
		} 
		
		public override function toString():String { 
			return formatToString("OpenUrlEvent", "type", "url","bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}