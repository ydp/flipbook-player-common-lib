package com.ydp.pdf2flash.events {
	import flash.events.Event;
	
	/**
	 * ...
	 * @author 
	 */
	public class FbLeoPlayerOpenUrlEvent extends Event {
		
		public var url:String;
		public static const OPEN_URL:String = "openUrl";
		
		public function FbLeoPlayerOpenUrlEvent(type:String, link:String, bubbles:Boolean=false, cancelable:Boolean=false) { 
			url = link;
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event { 
			return new FbLeoPlayerOpenUrlEvent(type, url, bubbles, cancelable);
		} 
		
		public override function toString():String { 
			return formatToString("FbLeoPlayerOpenUrlEvent", "type", "url","bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}