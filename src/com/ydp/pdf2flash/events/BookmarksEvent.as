﻿package  com.ydp.pdf2flash.events
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author loleszczak
	 */
	public class BookmarksEvent extends Event
	{
		public static const SHOW_BOOKMARKS:String = "showBookmarks";
		public static const HIDE_BOOKMARKS:String = "hideBookmarks";		
		public static const SHOW_SHORT_BOOKMARKS:String = "showShortBookmarks";
		public static const HIDE_SHORT_BOOKMARKS:String = "hideShortBookmarks";
		public static const BOOKMARK_PAGES:String = "bookmarkPages";		
		public static const BOOKMARK_STATE_CHANGED:String = "bookmarkStateChanged";
		public static const REMOVE_ITEM:String = "bookmarkRemoveItem";
		
		public function BookmarksEvent(type:String) {
			super(type);
		}	
		
		public override function clone():Event { 
			return new BookmarksEvent(type);
		} 
	}
	
}