﻿package com.ydp.pdf2flash.events {
	import flash.events.Event;
	
	/**
	* Events sent to ViewerController by views
	* Additional events are defined in their own classes:
	* PageChangeEvent, SearchEvent
	* 
	* @author Michal Samujlo
	*/
	public class ViewerEvent extends Event {
		
		public static const SHOW_COPYRIGHTS_POPUP:String = "showCopyrightsPopup";
		
		public static const SINGLE_VIEW:String = "singleView";
		
		public static const DOUBLE_VIEW:String = "doubleView";
		
		public static const GO_BACK:String = "goBack";
		
		public static const NEXT_PAGE:String = "nextPage";
		
		public static const PREV_PAGE:String = "prevPage";
		
		public static const FIRST_PAGE:String = "firstPage";
		
		public static const LAST_PAGE:String = "lastPage";

		public static const CLOSE:String = "close";
		
		public static const START_DRAG:String = "startDrag";
		
		public static const NAVIGATE_TO_URL:String = "navigateToUrl";
		
		public static const PRINT_LEFT:String = "printLeft";
		public static const PRINT_RIGHT:String = "printRight";
		public static const PRINT_TWO:String = "printTwo";
		
		public static const ZOOM_IN:String = "zoomIn";
		public static const ZOOM_OUT:String = "zoomOut";
		public static const ZOOM_RESET:String = "zoomReset";
		public static const ZOOM_CHANGED:String = "zoomChanged";
		public static const ZOOM_DRAGGED:String = "zoomDragged";
		public static const ZOOM_STARTED:String = "zoomStarted";
		public static const ZOOM_FINISHED:String = "zoomFinished";
		public static const ZOOM_PAGE:String = "zoomPage";
		
		public static const ZOOM_SHOW_MAP:String = "zoomShowMap";
		public static const ZOOM_HIDE_MAP:String = "zoomHideMap";
		
		public static const ZOOM_TOOL_SHOW:String = "zoomToolShow";
		public static const ZOOM_TOOL_HIDE:String = "zoomToolHide";
		
		public static const SHOW_TOOLBARS:String = "showToolbars";
		public static const HIDE_TOOLBARS:String = "hideToolbars";
		
		public static const SHOW_INDEX:String = "showIndex";
		public static const HIDE_INDEX:String = "hideIndex";
		
		public static const HIDE_RESOURCES:String = "hideResources";
		public static const SHOW_RESOURCES:String = "showResources";
		
		public static const HIDE_BOOKMARKS:String = "hideBookmarks";
		public static const SHOW_BOOKMARKS:String = "showBookmarks";
		
		public static const HIDE_NOTES:String = "hideNotes";
		public static const SHOW_NOTES:String = "showNotes";
		public static const NOTES_MOVED:String = "movedNotes";

		public static const OPEN_NOTE_WINDOW:String = "openNoteWindow";
		
		public static const HIDE_MARKS:String = "hideMarks";
		public static const SHOW_MARKS:String = "showMarks";
		public static const REMOVE_MARK:String = "removeMark";
		
		public static const HIDE_CREDITS:String = "hideCredits";
		public static const SHOW_CREDITS:String = "showCredits";
		
		public static const HIDE_SPOT_TOOL:String = "hideSpotTool";
		public static const SHOW_SPOT_TOOL:String = "showSpotTool";
		
		public static const HIDE_CACHE_TOOL:String = "hideCacheTool";
		public static const SHOW_CACHE_TOOL:String = "showCacheTool";
		
		public static const HIDE_SPOT_OVAL_TOOL:String = "hideSpotOvalTool";
		public static const SHOW_SPOT_OVAL_TOOL:String = "showSpotOvalTool";
		
		public static const HIDE_CACHE_OVAL_TOOL:String = "hideCacheOvalTool";
		public static const SHOW_CACHE_OVAL_TOOL:String = "showCacheOvalTool";
		
		public static const SHOW_GALLERY:String = "showGallery";
		public static const HIDE_GALLERY:String = "hideGallery";
		
		public static const SHOW_TEACHER_PANEL:String = "showTeacherPanel";
		public static const HIDE_TEACHER_PANEL:String = "hideTeacherPanel";
		
		public static const SHOW_LESSONS_PANEL:String = "showLessonsPanel";
		public static const HIDE_LESSONS_PANEL:String = "hideLessonsPanel";

		public static const FULLSCREEN_ON:String = "fullscreenOn";
		public static const FULLSCREEN_OFF:String = "fullscreenOff";
		
		public static const SHOW_TOC:String = "showToc";
		public static const HIDE_TOC:String = "hideToc";
		
		public static const OPEN_TOC_PAGE:String = "openTocPage";
		
		public static const PAGE_HISTORY_BACK:String	= "pageHistoryBack";
		public static const PAGE_HISTORY_FORWARD:String	= "pageHistoryForward";
		
		public static const STOP_DRAWING:String	= "stopDrawing";
		public static const START_DRAWING:String	= "startDrawing";
		
		public static const SHOW_HELP:String	= "showHelp";
		
		//added by CAOgroup
		public static const SHOW_SEND_TO_FRIEND:String = "showSendToFriend";
		public static const HIDE_SEND_TO_FRIEND:String = "hideSendToFriend";
		public static const GO_TO_BASKET:String = "goToBasket";
		
		public static const LOCK_KEYBOARD:String = "lockKeyboard";
		public static const UNLOCK_KEYBOARD:String = "unlockKeyboard";
		
		public static const ENABLE_SOUNDS:String = "enableSounds";
		public static const DISABLE_SOUNDS:String = "disableSounds";
		
		public static const POPUP_OPENED:String = "popupOpened";
		public static const POPUP_CLOSED:String = "popupClosed";
		
		public static const SHOW_HIGHLIGHT_HOTSPOTS:String = "showHighlightHotspots";
		public static const HIDE_HIGHLIGHT_HOTSPOTS:String = "hideHighlightHotspots";
		
		public static const SHOW_HOTSPOTS:String = "showHotspots";
		public static const HIDE_HOTSPOTS:String = "hideHotspots";
		
		public static const SELECT_IMAGE:String = "selectImage";
		
		public static const SHOW_PRINT:String = "showPrint";
		public static const HIDE_PRINT:String = "hidePrint";
		public static const POPUP_FAILED:String = "POPUP_FAILED";
		
		public static const SAVE_AS_PDF:String = "saveAsPdf";
		
		public static const SERVER_STATE_SEND:String = "serverStateSend";
		public static const SERVER_STATE_GET:String = "serverStateGet";
		
		public static const SAVE_STATE:String = "saveState";
		public static const LOAD_STATE:String = "loadState";
		
		public static const SILENT_ON:String = "silentModeOn";
		public static const SILENT_OFF:String = "silentModeOff";

		public static const PANEL_CLOSE:String = "panelClose";
		
		public static const SHOW_STOCK_PANEL:String = "showStockPanel";
		public static const HIDE_STOCK_PANEL:String = "hideStockPanel";

		public static const ADD_NOTE_BT_MD:String = "addNoteByMD";
		
		public static const SHOW_OPTION_PANEL:String = "SHOW_OPTION_PANEL";
		public static const HIDE_OPTION_PANEL:String = "HIDE_OPTION_PANEL";
		public static const SHOW_GLOSSARY:String = "SHOW_GLOSSARY";
		public static const HIDE_GLOSSARY:String = "HIDE_GLOSSARY";

		public static const WORD_SELECTED:String = "WORD_SELECTED";
		
		protected var _data:Object;
		
		public function ViewerEvent(type:String, data:Object=null) {
			_data = data;
			super(type);
		}
		
		public override function clone():Event { 
			return new ViewerEvent(type, data);
		} 
		
		public function get data():Object
		{
			return _data;
		}
		
		public function set data(value:Object):void
		{
			_data = value;
		}
		
		public override function toString():String { 
			return formatToString("ViewerEvent", "data", "type", "bubbles", "cancelable", "eventPhase");
		}		
	}
	
}