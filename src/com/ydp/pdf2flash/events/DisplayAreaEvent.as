﻿package com.ydp.pdf2flash.events 
{
	import flash.events.Event;
	
	/**
	* ...
	* @author Michał Samujło
	*/
	public class DisplayAreaEvent extends Event 
	{
		public static const PAGE_TURNED:String = "pageIsTurned";
		public static const PAGES_FIXED:String = "pagesFixed";
		public static const BOOK_APPLICATION_READY:String = "bookAppReady";
		public static const SHOW_HELP:String = "showHelp";
		
		public function DisplayAreaEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new DisplayAreaEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("DisplayAreaEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}