package com.ydp.pdf2flash.events
{
	import flash.events.Event;

	public class PageChangeEvent extends Event {
		
		public static const PAGE_CHANGED:String = "pageChanged";
		public static const BEFORE_PAGE_CHANGED:String = "beforePageChanged";
		
		/**
		 * number of page page node
		 */
		public var pageNum:Number;
		
		/**
		 * label used as page number
		 */
		public var pageNumber:String;
		
		public function PageChangeEvent( type:String, num:Number = -1, pnum:String = "") {
			super(type);
			this.pageNum = num;
			this.pageNumber = pnum;
		}
		
		public override function clone():Event { 
			return new PageChangeEvent(this.type, this.pageNum, this.pageNumber);
		} 
		
		public override function toString():String { 
			return formatToString("PageChangeEvent", "type", "bubbles", "cancelable", "eventPhase", "pageNum", "pageNumber"); 
		}		
	}
}