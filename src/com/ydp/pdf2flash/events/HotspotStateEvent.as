package com.ydp.pdf2flash.events
{
	import flash.events.Event;
	
	public class HotspotStateEvent extends Event
	{
		public static const UPDATED:String = "com.ydp.pdf2flash.events.HotspotStateEvent.UPDATED";
		
		protected var _hotspotID:String;
		protected var _state:String;
		
		public function HotspotStateEvent(type:String, hotspotID:String, state:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_hotspotID = hotspotID;
			_state = state;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new HotspotStateEvent(type, hotspotID, state);
		}
		
		public function get hotspotID():String
		{
			return _hotspotID;
		}

		public function get state():String
		{
			return _state;
		}
	}
}