﻿package com.ydp.pdf2flash.events {
	import com.ydp.pdf2flash.hotspots.IActiveArea;
	import flash.events.Event;
	
	/**
	* @author Michal Samujlo
	*/
	public class ActiveAreaEvent extends Event {
		
		public static const OPEN:String = "openArea";
		public static const MOUSE_OVER:String = "mouse_over";
		public static const MOUSE_OUT:String = "mouse_out";
		
		public static const ADD_INTERNAL_LINK:String = "addInternalLink";
		public static const ADD_CONTENT_LINK:String = "addContentLink";
		public static const ADD_ASSET:String = "addAsset";
		public static const DEFINE_ZOOM:String = "defineZoom";
		public static const ZOOM_DEFINED:String = "zoomDefined";
		public static const ZOOM_CANCELED:String = "zoomCanceled";
		
		public static const ADD:String = "addArea";
		public static const DELETE:String = "deleteArea";
		
		public static const OPEN_INFO:String = "openInfo";
		
		public static const COPY:String = "copyArea";
		public static const PASTE:String = "pasteArea";
		
		public static const MOVE_UP:String = "moveAreaUp";
		public static const MOVE_DOWN:String = "moveAreaDown";
		
		public var area:IActiveArea;
		
		public function ActiveAreaEvent(type:String, area:IActiveArea=null,  bubbles:Boolean = false, cancelable:Boolean = false ) {
			super(type);
			this.area = area;
		}
		
		[Deprecated(replacement=ActiveAreaEvent.area)]
		public function set activeArea(value:IActiveArea):void {
			area = value;
		}
		
		public override function clone():Event { 
			return new ActiveAreaEvent(this.type, this.area);
		} 
		
		public override function toString():String { 
			return formatToString("ActiveAreaEvent", "type", "bubbles", "cancelable", "eventPhase", "area"); 
		}
		
	}
	
}