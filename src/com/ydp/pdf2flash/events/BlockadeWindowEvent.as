﻿package com.ydp.pdf2flash.events 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Tomasz Szarzyński
	 */
	public class BlockadeWindowEvent extends Event
	{		
		public static const IDENTIFY:String = "BlockadeEvent_Identify";				
		public static const REGISTER:String = "BlockadeEvent_Register";	
		
		public var login:String;
		public var password:String;
		
		public function BlockadeWindowEvent(type:String, login:String = "", password:String = "") {
			super(type);
			this.login = login;
			this.password = password;
		}
		
		public override function clone():Event { 
			return new BlockadeWindowEvent(this.type, this.login, this.password);
		} 
		
		public override function toString():String { 
			return formatToString("BlockadeWindowEvent", "type", "bubbles", "cancelable", "eventPhase", "login", "password"); 
		}			
		
	}
	
}