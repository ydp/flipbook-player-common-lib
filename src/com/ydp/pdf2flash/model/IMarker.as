package com.ydp.pdf2flash.model
{
	public interface IMarker
	{

		function get id():int;
		function get page():int;
		function get idS():int;
		function get idE():int;
		
		function set id(i:int):void;
		function set page(i:int):void;
		function set idS(i:int):void;
		function set idE(i:int):void;
		
	}
}