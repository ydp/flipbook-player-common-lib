package com.ydp.pdf2flash.model
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.text.TextField;

	public class Note extends EventDispatcher
	{
		private var _id:int;
		private var _page:int;
		private var _pos_x:int;
		private var _pos_y:int;
		private var _note:String;
		private var _pageTitle:String;
		private var _timeStamp:Number;
		static public const CHANGE_NOTE:String="changeNote";

		public function Note(id:int, page:int, pos_x:int, pos_y:int, note:String, pageTitle:String="")
		{
			_id=id;
			_page=page;
			_pos_x=pos_x;
			_pos_y=pos_y;
			_note=note;
			_pageTitle=pageTitle;
		}

		public function get pageTitle():String
		{
			return _pageTitle;
		}

		//DEPRECATED
		public function get id():int
		{
			return _id;
		}

		public function get page():int
		{
			return _page;
		}

		public function get idS():int
		{
			return _pos_x;
		}

		public function get idE():int
		{
			return _pos_y;
		}

		public function get note():String
		{
			return _note;
		}

		public function get noteText():String
		{
			var tf:TextField=new TextField();
			tf.htmlText=_note;
			return tf.text;
		}

		//DEPRECATED
		public function set id(i:int):void
		{
			_id=i;
		}

		public function set page(i:int):void
		{
			_page=i;
			dispatchEvent(new Event(CHANGE_NOTE));
		}

		public function set idS(i:int):void
		{
			_pos_x=i;
			dispatchEvent(new Event(CHANGE_NOTE));
		}

		public function set idE(i:int):void
		{
			_pos_y=i;
			dispatchEvent(new Event(CHANGE_NOTE));
		}

		public function set note(n:String):void
		{
			_note=n;
			dispatchEvent(new Event(CHANGE_NOTE));
		}

		public function set pageTitle(value:String):void
		{
			_pageTitle=value;
		}

		public function get timeStamp():Number
		{
			return _timeStamp;
		}

		public function set timeStamp(value:Number):void
		{
			_timeStamp = value;
		}

	}
}
