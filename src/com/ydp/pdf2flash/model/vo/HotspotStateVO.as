package com.ydp.pdf2flash.model.vo
{
	import flash.events.Event;
	import flash.events.EventDispatcher;

	public class HotspotStateVO extends EventDispatcher
	{
		private var _id:String;
		private var _modificationDate:Number;
		private var _hotspotId:String;
		private var _data:String;
		
		public function HotspotStateVO()
		{
			id = "-1";
		}
		
		public function toXML():XML
		{
			var xmlString:String = "";
			xmlString+='<textFieldData id="'+id+'" textFieldId="'+hotspotId+'" modificationDate="'+modificationDate+'">';
			xmlString+=escape(data);
			xmlString+='</textFieldData>';
			return new XML(xmlString);
		}
		
		public function updateModificationDate():void
		{
			modificationDate = new Date().time;
		}
		
		public function get id():String
		{
			return _id;
		}
		
		public function set id(value:String):void
		{
			_id = value;
		}
		
		public function get hotspotId():String
		{
			return _hotspotId;
		}
		
		public function set hotspotId(value:String):void
		{
			_hotspotId = value;
		}
		
		public function get modificationDate():Number
		{
			return _modificationDate;
		}
		
		public function set modificationDate(value:Number):void
		{
			_modificationDate = value;
		}
		
		public function get data():String
		{
			return (!_data)?'':_data;
		}
		
		public function set data(value:String):void
		{
			_data = value;
			dispatchEvent(new Event(Event.CHANGE));
		}
	}
}
