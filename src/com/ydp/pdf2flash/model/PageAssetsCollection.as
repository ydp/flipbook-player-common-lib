package com.ydp.pdf2flash.model
{
	public class PageAssetsCollection extends AssetsCollection
	{
		
		protected var m_src:String;
		protected var m_index:uint;
		
		public function PageAssetsCollection(page:XML, index:uint)
		{
			super();
			m_index = index;
			m_src = page.@src;
		}
		
		public function get index():uint {
			return m_index;
		}
		
		public function get source():String {
			return m_src;
		}
	}
}