﻿package com.ydp.pdf2flash.model
{	

	public interface ISearchWindow 
	{
		function set onScreen(bool:Boolean):void;
		function get onScreen(): Boolean;
		function setActualSize(w : Number, h: Number):void;
		function setActualPosition(x : Number, y: Number) : void;
		function showSearchResult(data:Vector.<ISearchResult>):void
	}
	
}