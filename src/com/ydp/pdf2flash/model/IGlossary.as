package com.ydp.pdf2flash.model
{
	public interface IGlossary
	{
		function getWordByName(s:String):IGlossaryWordVO;
		function getWordByDescription(des:String):IGlossaryWordVO;
		function getAllWords():Array;
		function getWordsAmmount():int;
	}
}