package com.ydp.pdf2flash.model
{
	public class Marker implements IMarker
	{
		private var _id:int;
		private var _page:int;
		private var _idS:int;
		private var _idE:int;
		private var _timeStamp:Number;
		public function Marker(id:int, page:int, idS:int, idE:int)
		{
			_id = id;
			_page = page;
			_idS = idS;
			_idE = idE;
		}
		
		public function get id():int {
			return _id;
		}
		public function get page():int {
			return _page;
		}
		public function get idS():int {
			return _idS;
		}
		public function get idE():int {
			return _idE;
		}
		
		public function set id(i:int):void {
			_id = i;
		}
		public function set page(i:int):void {
			_page = i;
		}
		public function set idS(i:int):void {
			_idS = i;
		}
		public function set idE(i:int):void {
			_idE = i;
		}
		public function get timeStamp():Number
		{
			return _timeStamp;
		}
		
		public function set timeStamp(value:Number):void
		{
			_timeStamp = value;
		}

		
	}
}