package com.ydp.pdf2flash.model
{
	import flash.utils.Dictionary;

	public class AssetsCollection
	{
		public static const PARAM_ASSET_CLASS_NAME:String = "className";
		public static const PARAM_ASSET_TYPE:String = "type";
		
		protected var m_assets:Array;
		protected var m_types:Dictionary;
		protected var m_classNames:Dictionary;
		
		public function AssetsCollection()
		{
			clean();
		}
		
		public function addAsset(asset:IAssetData):void {
			if (asset) {
				m_assets.push(asset);
				if (asset.type.length>0)
					m_types[asset.type] = 1;
				if (asset.className.length>0)
					m_classNames[asset.className] = 1;
			}
		}
		
		public function hasAssetOf(paramName:String, value:String):Boolean {
			var found:Boolean;
			switch (paramName) {
				case PARAM_ASSET_CLASS_NAME:
					found = m_classNames[value] == 1 ? true : false;
					break;
				case PARAM_ASSET_TYPE:
					found = m_types[value] == 1 ? true : false;
					break;
				default:
					//noop
			}
			return found;
		}
		
		public function sortAssetsOn(params:Array):void {
			m_assets.sortOn( params, Array.CASEINSENSITIVE );
		}
		
		public function appendCollection(toAdd:AssetsCollection):AssetsCollection {
			if (toAdd && toAdd.numAssets>0) {
				for each (var asset:IAssetData in toAdd) {
					addAsset(asset);
				}
			}
			return this;
		}
		
		public function getAssetsByClassNames(classNames:Array):AssetsCollection {
			var asc:AssetsCollection = new AssetsCollection();
			for each (var className:String in classNames) {
				if (className.length>0)
					asc = asc.appendCollection(getAssetsByClassName(className));
			}
			return asc;
		}
		
		public function getAssetsByTypes(types:Array):AssetsCollection {
			var asc:AssetsCollection = new AssetsCollection();
			for each (var type:String in types) {
				if (type.length>0)
					asc = asc.appendCollection(getAssetsByType(type));
			}
			return asc;
		}
		
		public function getAssetsByClassName(className:String):AssetsCollection {
			var asc:AssetsCollection = new AssetsCollection();
			if (hasAssetOf(PARAM_ASSET_CLASS_NAME, className)) {
				for each (var asset:IAssetData in m_assets) {
					if (asset.className == className)
						asc.addAsset(asset);
				}
			}
			return asc;
		}
		
		public function getAssetsByType(type:String):AssetsCollection {
			var asc:AssetsCollection = new AssetsCollection();
			if (hasAssetOf(PARAM_ASSET_TYPE, type)) {
				for each (var asset:IAssetData in m_assets) {
					if (asset.type == type)
						asc.addAsset(asset);
				}
			}
			return asc;
		}
		
		public function get assets():Array {
			return m_assets;
		}
		
		public function get numAssets():uint {
			return m_assets.length;
		}
		
		public function clean():void {
			m_assets = new Array();
			m_types = new Dictionary();
			m_classNames = new Dictionary();
		}
	}
}