package com.ydp.pdf2flash.model
{
	import flash.events.IEventDispatcher;

	public interface IHotspotStateModel extends IEventDispatcher
	{
		function setHotspotState(hotspotID:String, state:String):void;
		function setAllHotspotsState(hotspotStates:Array):void;
		function getAllHotspotsStates():Array;
	}
}