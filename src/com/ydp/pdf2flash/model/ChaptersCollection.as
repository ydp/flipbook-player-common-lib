package com.ydp.pdf2flash.model
{
	import flash.utils.Dictionary;

	public class ChaptersCollection
	{
		
		protected var m_chapters:Dictionary;
		protected var m_numChapters:uint;
		
		public function ChaptersCollection()
		{
			clean();
		}
		
		public function addChapter(chapter:ChapterAssetsCollection):void {
			if (chapter) {
				var existingChapter:ChapterAssetsCollection = m_chapters[chapter.index];
				if (existingChapter) {
					existingChapter.appendCollection(chapter);
				} else {
					m_chapters[chapter.index] = chapter;
					m_numChapters++;
				}
			}
		}
		
		public function getChapterByIndex(index:uint):ChapterAssetsCollection {
			return m_chapters[index];
		}
		
		public function appendCollection(toAdd:ChaptersCollection):ChaptersCollection {
			if (toAdd && toAdd.numChapters>0) {
				for each (var chapter:ChapterAssetsCollection in toAdd) {
					addChapter(chapter);
				}
			}
			return this;
		}
		
		
		public function getChaptersWithAssetsOfClassNames(classNames:Array):ChaptersCollection {
			var asc:ChaptersCollection = new ChaptersCollection();
			for each (var className:String in classNames) {
				if (className.length>0)
					asc = asc.appendCollection(getChaptersWithAssetsOfClassName(className));
			}
			return asc;
		}
		
		public function getChaptersWithAssetsOfTypes(types:Array):ChaptersCollection {
			var asc:ChaptersCollection = new ChaptersCollection();
			for each (var type:String in types) {
				if (type.length>0)
					asc = asc.appendCollection(getChaptersWithAssetsOfType(type));
			}
			return asc;
		}
		
		public function getChaptersWithAssetsOfClassName(className:String):ChaptersCollection {
			var asc:ChaptersCollection = new ChaptersCollection();
			if (hasAssetOf(AssetsCollection.PARAM_ASSET_CLASS_NAME, className)) {
				for each (var chapter:ChapterAssetsCollection in m_chapters) {
					if (chapter.hasAssetOf(AssetsCollection.PARAM_ASSET_CLASS_NAME, className))
						asc.addChapter(chapter);
				}
			}
			return asc;
		}
		
		public function getChaptersWithAssetsOfType(type:String):ChaptersCollection {
			var asc:ChaptersCollection = new ChaptersCollection();
			if (hasAssetOf(AssetsCollection.PARAM_ASSET_TYPE, type)) {
				for each (var chapter:ChapterAssetsCollection in m_chapters) {
					if (chapter.hasAssetOf(AssetsCollection.PARAM_ASSET_TYPE, type))
						asc.addChapter(chapter);
				}
			}
			return asc;
		}
		
		protected function hasAssetOf(paramName:String, value:String):Boolean {
			var chapter:ChapterAssetsCollection;
			for each (chapter in m_chapters) {
				if (chapter.hasAssetOf(paramName, value)) {
					return true;
				}
			}
			return false;
		}
		
		
		
		
		
		
		
		public function get assets():AssetsCollection {
			var assets:AssetsCollection = new AssetsCollection();
			for each (var value:ChapterAssetsCollection in m_chapters) {
				assets.appendCollection(value);
			}
			return assets;
		}
		
		public function get numAssets():uint {
			var num:uint = 0;
			for each (var value:ChapterAssetsCollection in m_chapters) {
				num+=value.numAssets;
			}
			return num;
		}
		
		public function get numChapters():uint {
			return m_numChapters;
		}
		
		public function clean():void {
			for each (var value:ChapterAssetsCollection in m_chapters) {
				value.clean();
			}
			m_chapters = new Dictionary();
		}
	}
}