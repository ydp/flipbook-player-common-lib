package com.ydp.pdf2flash.model
{
	public class ChapterAssetsCollection extends AssetsCollection
	{
		
		protected var m_label:String;
		protected var m_index:uint;
		
		public function ChapterAssetsCollection(chapter:XML, index:uint)
		{
			super();
			m_index = index;
			m_label = chapter.@name;
		}
		
		public function get index():uint {
			return m_index;
		}
		
		public function get label():String {
			return m_label;
		}
	}
}