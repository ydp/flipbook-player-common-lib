package com.ydp.pdf2flash.model 
{
	import flash.display.DisplayObject;
	/**
	 * ...
	 * @author 
	 */
	public interface IGlossaryWordVO extends IAssetData
	{
		function get word():String ;
		function get ico():DisplayObject;
		function get description():String ;
		function toString():String;
		function get assets():Array;
		function set assets(a:Array):void;
	}

}