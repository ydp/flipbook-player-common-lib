package com.ydp.pdf2flash.model
{
	import flash.display.DisplayObjectContainer;

	/**
	 * ...
	 * @author Pawel Kolodziej
	 */
	public interface IConfig 
	{
		function get basePath():String;
		
		function get configPath():String;
		
		function get publicationPath():String;
		
		function get assetsPath():String;
		
		function get interfacePath():String;

		function get libraryPath():String;
		/**
		 * path to folder with utopia files used by active area players
		 */
		function get contentTargetPath():String;		
		/**
		 * name of file with style definitions for player e.g. player.css
		 */
		function get playerCss():String;
		/**
		 * name of file with style definitions for active areas e.g. area.css
		 */
		function get publicationCss():String;
		/**
		 * name of file with style definitions for active areas cerated by user e.g. user.css
		 */
		function get userCss():String;
		
		function get publicationConf():String ;

		/**
		 * filename of page which should be shown when book is loaded
		 */
		function get pageSrc():String;

		/**
		 * filename of page which should be shown when book is loaded
		 */
		function get pageNumber():int;
		
		function get title():String;
		
		function get debugMode():Boolean ;
				
		/**
		 * set page mode on app star (double or single)
		 */
		function get pageMode():String ;
		
		function get imagePopup():XML ;
		
		function set imagePopup(value:XML):void;
		
		function get moviePopup():XML ;
		function set moviePopup(value:XML):void;
		
		function get audioPopup():XML ;
		function set audioPopup(value:XML):void ;
		
		// popup templates
		function get imagePopupConf():String;
		function get moviePopupConf():String;
		function get audioPopupConf():String;

		// temporary solution for storage of stage reference
		function get mainClip():DisplayObjectContainer;
		function set mainClip(value:DisplayObjectContainer):void ;
		
		/**
		 * malmberg specific params
		 */
		
		function get userId():String ;
		function get publicationId():String ;
		function get bookCode():String ;
		function get svcHost():String ;
		
		function get getUserLayerURL():String ;
		function get saveUserLayerURL():String ;
		
		function get language():String;
					
		/**
		 * Maximum pages remembered by PageHistoryProxy
		 */
		function get pageHistoryLength():uint ;
		
		/**
		 * fbplayer params
		 */
		function get searchURL():String ;
		function get startPage():String;
		
		// path to localization file
		function get messagesConf():String;
		
		// config provides default before loading message before messages are loaded
		function get loadingMessage():String ;
		
		// should player register ExternalInterface callbacks?
		function get externalCallbacks():Boolean ;
		
		// should popup close after while changing pages?
		function get closePopupWhileChangingPage():Boolean;
		
		// should mouse page flip be enable?
		function get mousePageFlip():Boolean ;
		
		// Right to left book version (skin arabic)
		function get rtlMode():Boolean ;
		
		//Change normal digits to different ones
		function get differentDigits():Array;
		
		//show thumbnails and index btn?
		function get showThumbnails():Boolean;
		
		//show watermark?
		function get watermarkPath():String ;
		
		//watermark delay in ms
		function get watermarkTimeout():int;
		
		//define watermark position for left side pages
		function get watermarkPositionLeftPage():String;
		
		//define watermark position for right side pages
		function get watermarkPositionRightPage():String;

		//show print btn?
		function get printEnabled():Boolean ;

		
		//use internal or utopia player in popups
		function get useUtopiaPopups():Boolean ;
		function get limitDraggingArea():Boolean ;
		
		//1 - old windows (TOC, Resources, Page index) layout
		//2 - new layout
		function get windowsLayout():int;
		
		function get useTransformTool():Boolean;
		
		//show print btn?
		function get titleEnabled():Boolean ;
		
		/**
		 * @return 3 if zoom map should appear in top right corner, 
		 * 2 if zoom map plugin is disabled, 
		 * 1 otherwise
		 */
		function get zoomMode():String;
		
		/**
		 * zoom step
		 */
		function get zoomStep():int;
		
		/**
		 * max zoom level
		 */
		function get zoomMax():int;
		
		/**
		 * after reaching max zoom level, next click will reset zoom
		 */
		function get zoomReturn():Boolean;
		
		/**
		 * slider in zoom map to set zoom level
		 */
		function get zoomMapSliderVisible():Boolean;
		
		/**
		 * allow more then one popup to be opened at the same time
		 */
		function get allowMultiplePopups():Boolean;
		
		function get autoOpenSavedPopups():Boolean;
		
		function get drawingToolsEnabled():Boolean;
		
		function get addBackgroundUnderPopup():Boolean ;
		
		function get maximizeVideoPopup():Boolean;
		
		function get mediaProgresScaleEnabled():Boolean;
		
		function get pageIndexDisplayMode():String;
		
		function get addPagesShadow():Boolean ;
		
		function get zoomToolEnabled():Boolean;
		
		function get showResources():Boolean ;
		
		function get allowToDrawOnPopups():Boolean;
		function get enableAutomation():Boolean ;
		
		function get ivonaServer():String;
		function set ivonaServer(value:String):void
		
		function get noteEnabled():Boolean;
		
		function get markEnabled():Boolean;
        
		function get dataApi():String;
        
        function get edgateServer():String;
        
        function get textSelectionEnabled():Boolean;
		
		function get copyTextEnabled():Boolean;
		
		function get googleSearchEnabled():Boolean;
		
		function get imageCaptureEnabled():Boolean;
		
		function get closeButtonLabel():String;
		
		function get isBookshelf():Boolean;
        
        function get version():String;
		
		function get analyticsApiUrl():String;
		
		function get analyticsSource():String;
		
		function get empiratorUrl():String
			
		function get ttsAutoPlay():Boolean;
	}
	
}