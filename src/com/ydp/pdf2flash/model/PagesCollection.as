package com.ydp.pdf2flash.model
{
	import flash.utils.Dictionary;

	public class PagesCollection
	{
		
		protected var m_pages:Dictionary;
		protected var m_numPages:uint;
		
		public function PagesCollection()
		{
			clean();
		}
		
		public function addPage(page:PageAssetsCollection):void {
			if (page) {
				var existingPage:PageAssetsCollection = m_pages[page.source];
				if (existingPage) {
					existingPage.appendCollection(page);
				} else {
					m_pages[page.source] = page;
					m_numPages++;
				}
			}
		}
		
		public function getAssetsFromPage(index:uint):PageAssetsCollection {
			for each (var page:PageAssetsCollection in m_pages) {
				if (page.index == index)
					return page;
			}
			return null;
		}
		
		public function appendCollection(toAdd:PagesCollection):PagesCollection {
			if (toAdd && toAdd.numPages>0) {
				for each (var page:PageAssetsCollection in toAdd) {
					addPage(page);
				}
			}
			return this;
		}
		
		/*
		public function getPagesWithAssetsOfClassNames(classNames:Array):PagesCollection {
			var asc:PagesCollection = new PagesCollection();
			for each (var className:String in classNames) {
				if (className.length>0)
					asc = asc.appendCollection(getPagesWithAssetsOfClassName(className));
			}
			return asc;
		}
		
		public function getPagesWithAssetsOfTypes(types:Array):PagesCollection {
			var asc:PagesCollection = new PagesCollection();
			for each (var type:String in types) {
				if (type.length>0)
					asc = asc.appendCollection(getPagesWithAssetsOfType(type));
			}
			return asc;
		}
		
		public function getPagesWithAssetsOfClassName(className:String):PagesCollection {
			var asc:PagesCollection = new PagesCollection();
			if (hasAssetOf(AssetsCollection.PARAM_ASSET_CLASS_NAME, className)) {
				for each (var page:PageAssetsCollection in m_pages) {
					if (page.hasAssetOf(AssetsCollection.PARAM_ASSET_CLASS_NAME, className))
						asc.addPage(page);
				}
			}
			return asc;
		}
		
		public function getPagesWithAssetsOfType(type:String):PagesCollection {
			var asc:PagesCollection = new PagesCollection();
			if (hasAssetOf(AssetsCollection.PARAM_ASSET_TYPE, type)) {
				for each (var page:PageAssetsCollection in m_pages) {
					if (page.hasAssetOf(AssetsCollection.PARAM_ASSET_TYPE, type))
						asc.addPage(page);
				}
			}
			return asc;
		}
		*/
		/*protected function hasAssetOf(paramName:String, value:String):Boolean {
			var page:PageAssetsCollection;
			for each (page in m_pages) {
				if (page.hasAssetOf(paramName, value)) {
					return true;
				}
			}
			return false;
		}*/
		/*
		public function getAssetsForPage(index:uint):PageAssetsCollection {
			return m_pages
		}
		*/
		
		
		
		
		public function get assets():AssetsCollection {
			var assets:AssetsCollection = new AssetsCollection();
			for each (var value:PageAssetsCollection in m_pages) {
				assets.appendCollection(value);
			}
			return assets;
		}
		
		public function get numAssets():uint {
			var num:uint = 0;
			for each (var value:PageAssetsCollection in m_pages) {
				num+=value.numAssets;
			}
			return num;
		}
		
		public function get numPages():uint {
			return m_numPages;
		}
		
		public function clean():void {
			for each (var value:PageAssetsCollection in m_pages) {
				value.clean();
			}
			m_pages = new Dictionary();
		}
	}
}