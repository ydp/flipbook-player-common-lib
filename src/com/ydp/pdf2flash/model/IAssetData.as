package com.ydp.pdf2flash.model
{
	public interface IAssetData
	{
		function set label(value:String):void;
		function set source(value:String):void;
		function set currentPage(value:int):void;
		function set className(value:String):void;
		function set visible(value:Boolean):void;
		function set type(value:String):void;
		function set pageSrc(value:String):void;
		
		function get label():String;
		function get source():String;
		function get currentPage():int;
		function get className():String;
		function get visible():Boolean;
		function get type():String;
		function get pageSrc():String;
	}
}