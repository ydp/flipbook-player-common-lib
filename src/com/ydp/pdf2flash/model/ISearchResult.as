package com.ydp.pdf2flash.model
{
	public interface ISearchResult
	{
		function get assetData():IAssetData;
		function set assetData(value:IAssetData):void;
		function get pageNumber():int;
		function set pageNumber(value:int):void;
		function get pageName():String;
		function set pageName(value:String):void;
		function get pageId():String;
		function set pageId(value:String):void
		function get context():String;
		function set context(value:String):void	
	}
}