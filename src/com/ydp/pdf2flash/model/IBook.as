package com.ydp.pdf2flash.model {
	
	import com.ydp.pdf2flash.events.PageChangeEvent;
	
	import flash.events.IEventDispatcher;
	
	/**
	 * Dispatched when currentPage has changed
	 * @eventType com.ydp.pdf2flash.events.PageChangeEvent.PAGE_CHANGED
	 */
	[Event(name = "pageChanged", type = "com.ydp.pdf2flash.events.PageChangeEvent")]
	
	/**
	 * 
	 */
	public interface IBook extends IEventDispatcher {
		function set currentPage(i:int):void
		function get currentPage():int;
		
		/**
		 * number of pages
		 */
		function get numPages():int;
		
		/**
		 * number of assets
		 */		
		function get numAssets():int;
		
		/**
		 * returns XMLList of chapter nodes
		 */
		function get chapters():XMLList;
		
		/**
		 * returns XMLList of page nodes
		 */
		function get pages():XMLList;
		
		/**
		 * returns XMLList of assets nodes
		 */
		function get assets():XMLList;
		
		/**
		 * returns main xml
		 */
		function get publication():XML;

		/**
		 * returns XMLList of page nodes with specified name attribute
		 * @param	name
		 * @return XMLList
		 */
		function getPagesByName( name:String ):XMLList;
		
		/**
		 * returns XMLList of page nodes with specified src attribute
		 * @param	name
		 * @return XMLList
		 */
		function getPagesBySrc( src:String):XMLList;
		
		/**
		 * returns i-th page node 
		 * 
		 * @param	i node index
		 * @return IPage
		 */
		function getPageByNum( i:int ):XML;
		
		/**
		 * returns number of node by pageNumber attribute
		 * 
		 * @param	name 
		 * @return	num
		 */
		function getPageNumByPageNumber(num:int):int;
		
		/**
		 * returns number of node by name attribute
		 * 
		 * @param	name 
		 * @return	num
		 */
		function getPageNumByName(name:String):int;
					
		/**
		 * returns number of node by dataSrc attribute
		 * 
		 * @param	name 
		 * @return	num
		 */
		function getPageNumBySrc(name:String):int;
		
		/**
		 * returns name of page
		 * 
		 * @param	name 
		 * @return	name
		 */
		function getPageNameBySrc(name:String):String;
		
		/**
		 * returns chapter for n-th page or null if not found
		 */
		function getChapterForPageNum(n:int):XML;
		
		/**
		 * returns chapter number for given chapter node or -1 if not found
		 */
		function getChapterNum( chapter:XML ):int;
		
		/**
		 * returns asset node with specified src
		 * 
		 * @param	src 
		 * @return
		 */
		function getAssetBySrc(src:String):XMLList;
		
		/**
		 * Returns array of pageRefVO, where first vo describes requested chapter and rest assets in this chapter
		 * Assets are sorted by type and label.
		 */
		function getResourcesForChapterNum( ... args ):Array;
		
		/**
		 * Returns array of pageRefVO describing assets that appear at requested page
		 * Assets are sorted by type and label.
		 */
		function getResourcesForPageNum( ... args ):Array;

		/**
		 * Returns number od table of content page or -1 if table of content wasn't found.
		 * Table of content page 
		 * 
		 * @return int
		 */		
		function getTocPage():int;
		
		/**
		 * main model
		 */		
		function set bookData(data:XML):void;
		
		/**
		 * returns first chapter node
		 */
		function get bookData():XML;
		
		/**
		 * publication title
		 */
		function get publicationTitle():String;

		/**
		 * returns main node
		 */
		function get publicationData():XML;
		
		/**
		 * 
		 */
		function set basePath(path:String):void;
		function get basePath():String;
		
		//added by CAOgroup
		function get basketPath() : String;
		//added by CAOGROUP
		function get sendToFriendPath() : String;
		//added by CAOGROUP
		function get inscriptionPath() : String;
		//added by CAOGROUP
		function get identificationPath() : String;
		
		//for yTeach only
		function set viewerTitle(s:String) : void;
		function get viewerTitle() : String;
		
		/**
		 * resourcesList
		 */
		function get resources():Array;
		function getAssetsInChaptersByClassNames(className:Array):ChaptersCollection;
		function getAssetsInChaptersByClassName(className:String):ChaptersCollection;
		function getAssetsFromChapter(chapter:uint):ChapterAssetsCollection;
		
		function getAssetsFromPage(pageNo:uint):PageAssetsCollection;
		
		function get assetsInChapters():ChaptersCollection;
		function get additionalAssets():AssetsCollection;
		function get webAssets():AssetsCollection;

	}
}