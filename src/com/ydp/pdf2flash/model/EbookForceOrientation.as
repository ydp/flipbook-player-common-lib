package com.ydp.pdf2flash.model
{
	public class EbookForceOrientation
	{
		public static const AUTO_ORIENT:int = 0;
		public static const HOROZIONTAL_LOCK:int = 1;
		public static const VERTICAL_LOCK:int = 2;
	}
}