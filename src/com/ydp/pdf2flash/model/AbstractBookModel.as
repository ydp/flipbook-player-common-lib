package com.ydp.pdf2flash.model {
	import flash.events.Event;
	import flash.events.EventDispatcher;

	/**
	 * ...
	 * @author Michał Samujło
	 */
	public class AbstractBookModel extends EventDispatcher implements IBook {

		/* INTERFACE com.ydp.pdf2flash.model.IBook */

		public function set currentPage(i:int):void {
		}

		public function get currentPage():int {
			return -1;
		}

		public function get numPages():int {
			return 0;
		}

		public function get numAssets():int {
			return 0;
		}

		public function get chapters():XMLList {
			return new XMLList();
		}

		public function get pages():XMLList {
			return new XMLList();
		}

		public function get assets():XMLList {
			return new XMLList();
		}

		public function get publication():XML {
			return new XML();
		}

		public function getPagesByName(name:String):XMLList {
			return new XMLList();
		}

		public function getPagesBySrc(src:String):XMLList {
			return new XMLList();
		}

		public function getPageByNum(i:int):XML {
			return new XML();
		}

		public function getPageNumByPageNumber(num:int):int {
			return -1;
		}

		public function getPageNumByName(name:String):int {
			return -1;
		}

		public function getPageNumBySrc(name:String):int {
			return -1;
		}

		public function getPageNameBySrc(name:String):String {
			return "";
		}
		
		public function getChapterForPageNum(n:int):XML {
			return null;
		}

		public function getChapterNum(chapter:XML):int {
			return -1;
		}

		public function getAssetBySrc(src:String):XMLList {
			return new XMLList();
		}

		public function getResourcesForChapterNum(... args):Array {
			return [];
		}

		public function getResourcesForPageNum(... args):Array {
			return [];
		}

		public function getTocPage():int {
			return -1;
		}

		public function set bookData(data:XML):void {
		}

		public function get bookData():XML {
			return new XML();
		}

		public function get publicationTitle():String {
			return "";
		}

		public function get publicationData():XML {
			return new XML();
		}

		public function set basePath(path:String):void {
		}

		public function get basePath():String {
			return "";
		}

		public function get basketPath():String {
			return "";
		}

		public function get sendToFriendPath():String {
			return "";
		}

		public function get inscriptionPath():String {
			return "";
		}

		public function get identificationPath():String {
			return "";
		}

		public function set viewerTitle(s:String):void {

		}

		public function get viewerTitle():String {
			return "";
		}

		public function get resources():Array {
			return [];
		}
		
		public function get assetsInChapters():ChaptersCollection {
			return new ChaptersCollection();
		}
				
		public function getAssetsInChaptersByClassNames(classNames:Array):ChaptersCollection {
			return new ChaptersCollection();
		}
		
		public function getAssetsInChaptersByClassName(className:String):ChaptersCollection {
			return new ChaptersCollection();
		}
		
		public function getAssetsFromChapter(chapter:uint):ChapterAssetsCollection {
			return new ChapterAssetsCollection(new XML(), 1);
		}
		
		public function getAssetsFromPage(pageNo:uint):PageAssetsCollection {
			return new PageAssetsCollection(new XML(), 1);
		}
		
		/**
		 * additiona resources node
		 */
		public function get additionalAssets():AssetsCollection {
			return new AssetsCollection();
		}
		/**
		 * web resources nod
		 */
		public function get webAssets():AssetsCollection {
			return new AssetsCollection();
		}
	}

}