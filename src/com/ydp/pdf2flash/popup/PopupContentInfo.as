﻿package com.ydp.pdf2flash.popup 
{
	import com.ydp.pdf2flash.hotspots.IActiveArea;	
	import com.ydp.utils.UriUtil;
	import flash.utils.Dictionary;
	import com.ydp.pdf2flash.model.IGlossaryWordVO;
	
	/**
	* Klasa trzymająca informację na temat kontentu PopupPanela.
	* @author Marcin Kałdonek
	*/
	public class PopupContentInfo {
		protected var m_type:String;
		protected var m_utpPagePath:String;
		protected var m_utpPageNumber:int;
		
		protected var m_utpPlayerPath:String;
		protected var m_utopiaPlayerBasePath:String;
		protected var m_utpPlayerFile:String;
		protected var m_utpPage:XML;
		protected var m_title:String;
		protected var m_width:Number;
		protected var m_height:Number;
		protected var m_autoPlay:Boolean;
		
		protected var m_resizeBorderWidth:Number;
		protected var m_resizeBorderColor:Number;
		
		protected var m_source:XML;
		protected var m_contentType:String;
		
		protected var _mediaPath:String;
		protected var _mediaVersion:String;
		protected var _glossaryWordVO:IGlossaryWordVO;
		//added by ts
		public var activeArea : IActiveArea;
		 
		/**
		 * maksymalna szerokość popupu
		 */
		protected var m_maxWidth:Number;
		
		/**
		 * Typ kontentu
		 * @see com.ydp.pdf2flash.popup.PopupContentType
		 */
		public function set type(val:String):void {
			m_type = val;
		}
		
		public function get type():String {
			return m_type;
		}
		
		/**
		 * Typ kontentu (audio, video, swf, slideshow, image)		 
		 */
		public function set contentType(val:String):void {
			m_contentType = val;
		}
		
		public function get contentType():String {
			return m_contentType;
		}
		
		/**
		 * Ścieżka do strony lub testu utopii.
		 */
		public function set utpPagePath(path:String):void {
			var ind:int = path.indexOf("?");
			if (-1!=ind) {
				var params:Dictionary = UriUtil.getParams( path );
				m_utpPageNumber = int( params['page'] );
				path = path.substring(0, ind);
			}
			m_utpPagePath = path;
		}
		
		public function get utpPagePath():String {
			return m_utpPagePath;
		}
		
		public function get utpPageNumber():int {
			return m_utpPageNumber;
		}
		
		/**
		 * Player utopii (plik utp definiujący playera).
		 */
		public function set utpPlayerFile(name:String):void {
			m_utpPlayerFile = name;
		}
		
		public function get utpPlayerFile():String {
			return m_utpPlayerFile;
		}
		
		/**
		 * Ścieżka do playerów utopii (plików utp definiujących playery).
		 */
		public function set utpPlayersPath(path:String):void {
			m_utpPlayerPath = path;
		}
		
		public function get utpPlayersPath():String {
			return m_utpPlayerPath;
		}
		
		/**
		 * Ścieżka bazowa dla UtopiaMediaPlayer.
		 */
		public function set utopiaPlayerBasePath(path:String):void {
			m_utopiaPlayerBasePath = path;
		}
		
		public function get utopiaPlayerBasePath():String {
			return m_utopiaPlayerBasePath;
		}

		/**
		 * path to media
		 */
		public function set mediaPath(value:String):void {
			_mediaPath = value;
		}
		public function get mediaPath():String {
			return _mediaPath;
		}
		
		/**
		 * media version
		 */
		public function set mediaVersion(value:String):void {
			_mediaVersion = value;
		}
		public function get mediaVersion():String {
			return _mediaVersion;
		}
		
		/**
		 * Player utopii w postaci strony XML.
		 */
		public function set utpPage(page:XML):void {
			m_utpPage = page;
		}
		
		public function set width(val:Number):void {
			m_width = val;
		}
		
		public function get width():Number {
			return m_width;
		}
		
		public function set height(val:Number):void {
			m_height = val;
		}
		
		public function get height():Number {
			return m_height;
		}
		
		public function get utpPage():XML {
			return m_utpPage;
		}
		
		public function set title(value:String):void 
		{
			m_title = value;
		}
		
		public function get title():String { 
			return m_title; 
		}
		
		public function set autoPlay(val:Boolean):void {
			m_autoPlay = val;
		}
		
		public function get autoPlay():Boolean {
			return m_autoPlay;
		}
		
		public function set resizeBorderWidth(val:Number):void {
			m_resizeBorderWidth = val;
		}
		
		public function get resizeBorderWidth():Number {
			return m_resizeBorderWidth;
		}
		
		public function set resizeBorderColor(val:Number):void {
			m_resizeBorderColor = val;
		}
		
		public function get resizeBorderColor():Number {
			return m_resizeBorderColor;
		}
		
		public function set maxWidth(val:Number):void {
			m_maxWidth = val;
		}
		
		public function get maxWidth():Number {
			return m_maxWidth;
		}
		
		
		public function set source(value:XML):void {
			this.m_source = value;
		}
		
		public function get source():XML{
			return m_source ;
		}
		
		public function get glossaryWordVO():IGlossaryWordVO 
		{
			return _glossaryWordVO;
		}
		
		public function set glossaryWordVO(value:IGlossaryWordVO):void 
		{
			_glossaryWordVO = value;
		}

	}
	
}