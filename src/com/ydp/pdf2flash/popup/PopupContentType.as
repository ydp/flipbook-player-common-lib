﻿package com.ydp.pdf2flash.popup {
	
	/**
	* Wyszczególnienie typów kontetu PopupPanela.
	* @author Marcin Kałdonek
	*/
	public class PopupContentType {
		
		/**
		 * Player utopii odtwarzający pliki video.
		 */
		public static const UTOPIA_MOVIE_PLAYER:String = "utopiaMoviePlayer";
		
		/**
		 * Player utopii odtwarzający pliki audio.
		 */
		public static const UTOPIA_AUDIO_PLAYER:String = "utopiaAudioPlayer";
		
		/**
		 * Slideshow utopii.
		 */
		public static const UTOPIA_SLIDESHOW:String = "utopiaSlideshow";
		
		/**
		 * Strona lub test utopii.
		 */
		public static const UTOPIA_INTERACTIVE_CONTENT:String = "utopiaInteractiveContent";
        
        public static const LEO_INTERACTIVE_CONTENT:String = "leoInteractiveContent";
	}
}