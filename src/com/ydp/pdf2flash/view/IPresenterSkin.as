﻿package com.ydp.pdf2flash.view {
	import com.ydp.core.IResizable;
	import com.ydp.pdf2flash.model.IBook;
	import com.ydp.teacherPanel.ITeacherPanelTextEditorToolbar;
	import com.ydp.teacherPanel.ITeacherPanelToolbar;
	import com.ydp.virtualKeyboard.IKeyboard;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.IEventDispatcher;
	import flash.geom.Rectangle;
	
	/**
	 * User interface for presenter application
	 * @author Michał Samujło
	 */
	public interface IPresenterSkin extends IResizable, IEventDispatcher {
		
		// publication model
		function set publication(value:IBook):void;
		function get publication():IBook;
		
		// setPagePanel
		function setPagePanel(digits:Array = null):void;
		
		// is print function enabled
		function set printEnabled(value:Boolean):void
		function get printEnabled():Boolean

		// single or double view
		function set viewMode(value:String):void
		function get viewMode():String

		// zoom of display area
		function set zoomValue(value:Number):void
		function get zoomValue():Number;

		// area where application can place
		function getDisplayAreaBounds(target:DisplayObject=null):Rectangle;

		// bounds of all skin elements, can be used to mask skin
		function getSkinBounds(target:DisplayObject=null):Rectangle;

		// update all skin elements
		function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number ):void;

		function showProgressBar(loaded:Number, total:Number):void;
		function hideProgressBar():void;
		
		function showMessage( title:String, message:String ):void;
		
		function showPageNotFoundMessage():void;
		
		function showSearchResults( results:XML ):void;
		
		function showResources( value:Boolean ):void;
		
		function showNotes( value:Boolean ):void;
		
		function showGlossary( value:Boolean ):void;
		
		function showPrint( value:Boolean ):void;
		
		function showTeacherPanel( value:Boolean ):void;
		
		function showPageIndex( value:Boolean ):void;
		
		function showToc( value:Boolean ):void;
		
		function showSpot( value:Boolean ):void;
		
		function showCache( value:Boolean ):void;
		
		function showOvalSpot( value:Boolean ):void;
		
		function showOvalCache( value:Boolean ):void;
		
		function showCurrentPageNumber(value:String):void;
		
		function stopDrawing():void;
		
		function showThumbnails(b:Boolean):void;
		
		function set textLayoutConfig(value:Object):void;
		
		function get resourcesWindowEnabled():Boolean;
		
		// reserved for future use
		function set layout(value:Object):void;
		function get layout():Object;
		
		function init(graphicsPath:String):void;
		
		function set soundsEnabled(value:Boolean):void;
		function set title(value:String):void;
		function showBookshelfBtn(b:Boolean):void;
		function showResourcesBtn(b:Boolean):void;
		function showNotesBtn(b:Boolean):void;
		function showGoogleLbl(b:Boolean):void;
		function showImageCaptureBtn(b:Boolean):void;
		
		function get teacherPanelToolbar():ITeacherPanelToolbar;
		function get teacherPanelTextToolbar():ITeacherPanelTextEditorToolbar
		function set printMode(mode:int):void;
		
		function setZoomParams(min:Number, max:Number, step:Number):void;
	}
	
}