﻿package com.ydp.pdf2flash.view.hotspots {
	
	import com.ydp.pdf2flash.events.ActiveAreaEvent;
	import com.ydp.pdf2flash.hotspots.ActiveAreaType;
	import com.ydp.pdf2flash.hotspots.Asset;
	import com.ydp.pdf2flash.hotspots.IActiveArea;
	import com.ydp.style.IStyleClient;
	import com.ydp.style.IStyleServer;
	import com.ydp.style.StyleSelectorInfo;
	import com.ydp.utils.ConvertUtil;
	import com.ydp.utils.ObjectUtil;
	import com.ydp.utils.UriUtil;
	import com.ydp.utils.YLoader;
	import com.ydp.view.TooltipDecorator;
	
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.utils.getQualifiedClassName;
	import flash.xml.XMLNode;
	
	import pl.ydp.nexl.INexlExtensionDescription;
	import pl.ydp.nexl.Nexl;

	
	/**
	 * Dispatched when area is clicked
	 * @eventType com.ydp.pdf2flash.events.ActiveAreaEvent.OPEN
	 */
	[Event(name = "open", type = "com.ydp.pdf2flash.events.ActiveAreaEvent")]	
	public class ActiveArea extends Sprite implements IActiveArea {
		protected static const SHAPE_RECT:String = "rectangle";
		protected static const SHAPE_ELLIPSE:String = "ellipse";
		protected static const SHAPE_CIRCLE:String = "circle";
		protected static const SHAPE_TRIANGLE:String = "triangle";
		protected static const SHAPE_POLYGON:String = "polygon";
		
		
		protected static var styleServer:IStyleServer;
		
		protected var _conf:XML;
		protected var _shapeType:String = SHAPE_RECT;
		protected var _shapePoints:XMLList;
		
		// własności styli
		protected var _styles:Object;
		
		// obiekt przechowujący własności styli dla różnych selektorów
		protected var _stateStyles:Object;
		
		protected var _description:String;
		protected var _tooltip:String;
		
		// dynamiczne pseudo klasy n.p.: over, disabled
		protected var _stylePseudoClasses:Array = [];
		
		protected var _type:ActiveAreaType;
		protected var _isPopup:Boolean;
		
		protected var _assets:Array;
		
		protected var _icon:DisplayObject;
		protected var _iconOver:DisplayObject;
		protected var _iconDisabled:DisplayObject;
		
		protected var _shadowSprite:Sprite;
		protected var _shadowSpriteMask:Sprite;		
		
		//mouseCatcher przykrywa pole tekstowe
		protected var _mouseCatcher:Sprite;
		protected var _iconMask:Sprite;
		protected var _iconOverMask:Sprite;
		protected var _iconDisabledMask:Sprite;
		
		public var labelVisible:Boolean = true;
		public var labelOverVisible:Boolean = true;
		public var labelDisabledVisible:Boolean = true;
		
		public var textColor:int = 0x000000;
		public var textOverColor:int = 0x000000;
		public var textDisabledColor:int = 0x000000;
		public var textSize:int = 10;
		public var textOverSize:int = 10;
		public var textDisabledSize:int = 10;
		
		public var frameColor:int = 0xffffff;
		public var frameOverColor:int = 0xffffff;
		public var frameDisabledColor:int = 0xffffff;
		
		public var frameAlpha:Number = 0.2;
		public var frameOverAlpha:Number = 0.2;
		public var frameDisabledAlpha:Number = 0.2;
		
		public var frameWidth:int = 1;
		public var frameOverWidth:int = 1;
		public var frameDisabledWidth:int = 1;
		
		public var backgroundColor:int = 0xffffff;
		public var backgroundOverColor:int = 0xffffff;
		public var backgroundDisabledColor:int = 0xffffff;
		
		public var backgroundAlpha:Number = 0.2;
		public var backgroundOverAlpha:Number = 0.2;
		public var backgroundDisabledAlpha:Number = 0.2;
		
		public var shadowColor:int = 0xffffff;
		public var shadowOverColor:int = 0xffffff;
		public var shadowDisabledColor:int = 0xffffff;
		
		public var shadowAlpha:Number = 0;
		public var shadowOverAlpha:Number = 0;
		public var shadowDisabledAlpha:Number = 0;
		
		public var popupColor:int = 0xffffff;
		public var popupOverColor:int = 0xffffff;
		public var popupDisabledColor:int = 0xffffff;
		public var popupAlpha:Number = 0.2;
		
		public var popupFrameColor:int = 0xffffff;
		public var popupFrameOverColor:int = 0xffffff;
		public var popupFrameDisabledColor:int = 0xffffff;
		public var popupOverAlpha:Number = 0.2;
		
		public var iconSrc:String;
		public var iconValign:String = "bottom";
		public var iconHalign:String = "right";

		public var iconOverSrc:String;
		public var iconOverValign:String = "bottom";
		public var iconOverHalign:String = "right";
		
		public var iconDisabledSrc:String;
		public var iconDisabledValign:String = "bottom";
		public var iconDisabledHalign:String = "right";

		protected var _descriptionTextField:TextField;
		
		protected var _enabled:Boolean = true;
		protected var _zoomEnabled:Boolean = true;
		protected var _pageData:XML;
		
		protected var _areasVisible:Boolean = true;
		
		protected var _assetSrc:String;
		
		protected var _openPopupOnMouseOver:Boolean = false;
		
		public function ActiveArea():void {
			
			_assets = new Array();
			_stateStyles = { };
					
			this.soundTransform = new SoundTransform(0, 0);	
			
			cacheAsBitmap = true;
			opaqueBackground = null;
		}
		
		public function set zoomEnabled(b:Boolean):void {
			_zoomEnabled = b;
		}
		public function updateDisplayList(evt:Event = null):void {
			var isMouseOver:Boolean = false;
			var format:TextFormat;
			if (evt && evt is MouseEvent && evt.type==MouseEvent.ROLL_OVER) {
				isMouseOver = true;
			}
			
			if (_conf && _conf is XML) {
				if (_mouseCatcher) {
					var gfx:Graphics = _mouseCatcher.graphics;
					gfx.clear();
					gfx.beginFill( 0x00ff00, 0 );
					drawShape(gfx, _shapeType, _shapePoints);
					gfx.endFill();
				}
				if (_iconMask) {					
					gfx = _iconMask.graphics;
					gfx.clear();
					gfx.beginFill( 0x00ff00, 1 );
					drawShape(gfx, _shapeType, _shapePoints);
					gfx.endFill();
				}
				
				if (_iconOverMask) {					
					gfx = _iconOverMask.graphics;
					gfx.clear();
					gfx.beginFill( 0x00ff00, 1 );
					drawShape(gfx, _shapeType, _shapePoints);
					gfx.endFill();
				}
				
				if (_iconDisabledMask) {					
					gfx = _iconDisabledMask.graphics;
					gfx.clear();
					gfx.beginFill( 0x00ff00, 1 );
					drawShape(gfx, _shapeType, _shapePoints);
					gfx.endFill();
				}
				
				
				if (_enabled) {
					var _fill:int = (isMouseOver)? shadowOverColor : shadowColor;
					var _alpha:Number = (isMouseOver)? shadowOverAlpha : (_areasVisible) ? shadowAlpha : 0;
					
					var fill:int = (isMouseOver)? backgroundOverColor : backgroundColor;
					var alpha:Number = (isMouseOver)? backgroundOverAlpha : (_areasVisible) ? backgroundAlpha : 0;
					var borderColor:int = (isMouseOver)? frameOverColor : frameColor;
					var borderWidth:Number = (isMouseOver)? frameOverWidth : frameWidth;
					var borderAlpha:Number = (isMouseOver)? frameOverAlpha : (_areasVisible) ? frameAlpha : 0;
										
					if (!_shadowSprite) {
						_shadowSprite = new Sprite();
						_shadowSprite.cacheAsBitmap = true;
						_shadowSprite.blendMode = BlendMode.LAYER;
						addChildAt(_shadowSprite, 0);
						_shadowSpriteMask = new Sprite();						
						_shadowSprite.addChild(_shadowSpriteMask);						
					}
					_shadowSprite.graphics.clear();
					_shadowSprite.graphics.beginFill(_fill, _alpha);
					drawShape(_shadowSprite.graphics, _shapeType, _shapePoints, 4, 4);						
					_shadowSprite.graphics.endFill();
					
					_shadowSpriteMask.cacheAsBitmap = true;
					_shadowSpriteMask.blendMode = BlendMode.ERASE;
					_shadowSpriteMask.graphics.clear();
					_shadowSpriteMask.graphics.beginFill(0x000000, 1);
					drawShape(_shadowSpriteMask.graphics, _shapeType, _shapePoints);						
					_shadowSpriteMask.graphics.endFill();
					
					
					graphics.clear();
					graphics.lineStyle( borderWidth, borderColor, borderAlpha );
					drawShape(graphics, _shapeType, _shapePoints);
					
					graphics.beginFill( fill, alpha );
					drawShape(graphics, _shapeType, _shapePoints);
					graphics.endFill();
					
					
					if (_iconDisabled) {
						_iconDisabled.visible = false;
					}
					var currentIcon:DisplayObject ;
					var currentHAlign:String;
					if (_icon) {
						_icon.visible = !(isMouseOver);
						if(_icon.visible){
							currentIcon = _icon;
							currentHAlign = iconHalign;
						}
						positionIcon(_icon, iconValign, iconHalign);
						if (_icon is MovieClip && MovieClip(_icon).totalFrames > 0) {
							MovieClip(_icon).gotoAndPlay( 1 );
						}
					}
					if (_iconOver) {
						_iconOver.visible = (isMouseOver);
						if(_iconOver.visible){
							currentIcon = _iconOver;
							currentHAlign = iconOverHalign;
						}
						positionIcon(_iconOver, iconOverValign, iconOverHalign);
						if (_iconOver is MovieClip && MovieClip(_iconOver).totalFrames > 0) {
							MovieClip(_iconOver).gotoAndPlay( 1 );
						}
					}
					if (_descriptionTextField) {
						_descriptionTextField.visible = (isMouseOver) ? labelOverVisible: labelVisible;
						var size:int = (isMouseOver)? textOverSize : textSize;
						var color:int = (isMouseOver)? textOverColor : textColor;
																		
						format = new TextFormat("Arial", size, color);
						_descriptionTextField.setTextFormat( format, 0, _descriptionTextField.text.length );
						_descriptionTextField.width = currentIcon ? _conf.@width - currentIcon.width : _conf.@width;						
						positionText(currentIcon, currentHAlign);
					}
				} else {
					
					if (!_shadowSprite) {
						_shadowSprite = new Sprite();
						_shadowSprite.cacheAsBitmap = true;
						_shadowSprite.blendMode = BlendMode.LAYER;
						addChildAt(_shadowSprite, 0);
						_shadowSpriteMask = new Sprite();						
						_shadowSprite.addChild(_shadowSpriteMask);						
					}
					
					_shadowSprite.graphics.clear();
					_shadowSprite.graphics.beginFill(shadowDisabledColor, shadowDisabledAlpha);
					drawShape(_shadowSprite.graphics, _shapeType, _shapePoints, 4, 4);						
					_shadowSprite.graphics.endFill();
					
					_shadowSpriteMask.cacheAsBitmap = true;
					_shadowSpriteMask.blendMode = BlendMode.ERASE;
					_shadowSpriteMask.graphics.clear();
					_shadowSpriteMask.graphics.beginFill(0x000000, 1);
					drawShape(_shadowSpriteMask.graphics, _shapeType, _shapePoints);						
					_shadowSpriteMask.graphics.endFill();
														
					graphics.clear();
					graphics.beginFill( backgroundDisabledColor, backgroundDisabledAlpha );
					graphics.lineStyle( frameDisabledWidth, frameDisabledColor, frameDisabledAlpha );
					drawShape(graphics, _shapeType, _shapePoints);
					graphics.endFill();
					graphics.beginFill( backgroundDisabledColor, backgroundDisabledAlpha );
					drawShape(graphics, _shapeType, _shapePoints);
					graphics.endFill();
					
					if (_icon)
						_icon.visible = false;
					if (_iconOver)
						_iconOver.visible = false;
					if (_iconDisabled) {
						_iconDisabled.visible = true;
						positionIcon(_iconDisabled, iconDisabledValign, iconDisabledHalign);
					}
					if (_iconDisabled is MovieClip && MovieClip(_iconDisabled).totalFrames > 0) {
						MovieClip(_iconDisabled).gotoAndPlay( 1 );
					}
					if (_descriptionTextField) {
						_descriptionTextField.visible = labelDisabledVisible;
						format = new TextFormat("Arial", textDisabledSize, textDisabledColor);
						_descriptionTextField.setTextFormat( format, 0, _descriptionTextField.text.length );
					}
				}
			}
		}
		
		
		protected function drawShape(graphics:Graphics, type:String, points:XMLList, xOffset:Number = 0, yOffset:Number = 0 ):void {			
			if (!points || points.length() < 2)
				return;			
			switch(type) {				
				case SHAPE_ELLIPSE:
				case SHAPE_CIRCLE:
					graphics.drawEllipse(points[0].@x + xOffset, points[0].@y + yOffset,
						points[1].@x - points[0].@x, points[1].@y - points[0].@y);
				break;				
				case SHAPE_TRIANGLE:
				case SHAPE_POLYGON:
					graphics.moveTo(Number(points[0].@x) +xOffset, Number(points[0].@y) +yOffset);
					for (var i:int = 1; i < points.length(); i++) {
						graphics.lineTo(Number(points[i].@x) +xOffset, Number(points[i].@y) +yOffset)
					}
					graphics.lineTo(Number(points[0].@x) +xOffset, Number(points[0].@y) + yOffset);
				break;
				case SHAPE_RECT:
				default:
					graphics.drawRect(points[0].@x+xOffset, points[0].@y+yOffset, points[1].@x - points[0].@x, points[1].@y - points[0].@y);
				break;
			}
		}
		
		
		protected function positionIcon(icon:DisplayObject, valign:String, halign:String):void {
			if (!icon) {
				_descriptionTextField.x = _descriptionTextField.y = 0;
				positionText(icon, halign);
				return;
			}
			switch (valign) {
				case "top":
					icon.y = 0;
					break;
				case "center":
					icon.y = _conf.@height/2 - icon.height/2;
					break;
				case "bottom":
				default:
					icon.y = _conf.@height - icon.height;
					break;
			}
			switch (halign) {
				case "left":
					icon.x = 0;
					if (_descriptionTextField && icon.visible)
						_descriptionTextField.x = icon.x + icon.width + 5;					
					break;
				case "center":
					icon.x = _conf.@width / 2 - icon.width / 2;					
				break;
				case "right":
				default:
					icon.x = _conf.@width - icon.width;
					if (_descriptionTextField && icon.visible)
						_descriptionTextField.x = icon.x -_descriptionTextField.width;						
					break;
			}
			positionText(icon, halign);
		}
		
		protected function positionText(icon:DisplayObject,  halign:String):void {			
			if (!_descriptionTextField) return;
			if (!icon) {
				_descriptionTextField.x = _descriptionTextField.y = 0;
				return;
			}					
			switch (halign) {
				case "left":					
					if (_descriptionTextField && icon.visible)
						_descriptionTextField.x = icon.x + icon.width + 10;					
					break;
				case "center":
					_descriptionTextField.x = 0;
					
					break;
				case "right":
				default:					
					if (_descriptionTextField && icon.visible)
						_descriptionTextField.x = icon.x -_descriptionTextField.width ;
					break;
			}			
		}
		
		protected function onMouseOver(event:Event):void {			
			updateDisplayList(event);
			dispatchEvent( new ActiveAreaEvent( ActiveAreaEvent.MOUSE_OVER, this ) );
			if (_openPopupOnMouseOver)
				open(event);
			//if (_type == ActiveAreaType.MULTIFILE)
				
			
		}
		
		protected function onMouseOut(event:Event):void {
			updateDisplayList(event);
			dispatchEvent( new ActiveAreaEvent( ActiveAreaEvent.MOUSE_OUT, this ) );
		}
		
		protected function onStartOpen(event:Event):void {			
			event.stopPropagation();
		}
		
		public function open(evt:Event = null):void {
			if (_type == ActiveAreaType.WATERMARK){
				evt.stopPropagation();
				return
			}
			dispatchEvent( new ActiveAreaEvent( ActiveAreaEvent.OPEN, this ) );
			if (!_zoomEnabled && evt) {
				evt.stopPropagation();
			}
		}
		
		public function isPopup():Boolean {
			return _isPopup;
		}
		public function hasButton():Boolean {
			return false;
		}
		public function isPlayerUIVisible():Boolean {
			return true;
		}
		public function areaTooltip():String {
			return "";
		}
		
		/**
		 * read visual styles from IStyleServer
		 * supported styles:
		 * backgroundColor, alpha, frameColor, frameOverColor, frameAlpha, frameWidth
		 * supported pseudoclases:
		 * over - on mouse over
		 * text - style for active area description
		 * disabled - used when area is inactive
		 */
		protected function setStyles():void {
			
			initAreaType();
			
			_openPopupOnMouseOver = ConvertUtil.getNotNull(getStyle("showPopupOnMouseOver"), false);
			
			shadowColor = ConvertUtil.getNotNull( getStyle("shadowColor") , shadowColor ) as int;
			shadowAlpha =  ConvertUtil.getNotNull( getStyle("shadowAlpha") , shadowAlpha ) as Number;
			
			backgroundColor = ConvertUtil.getNotNull( getStyle("backgroundColor") , backgroundColor ) as int;
			backgroundAlpha =  ConvertUtil.getNotNull( getStyle("backgroundAlpha") , backgroundAlpha ) as Number;
			frameColor  = ConvertUtil.getNotNull( getStyle("frameColor") , frameColor ) as int;
			frameAlpha = ConvertUtil.getNotNull( getStyle("frameAlpha") , frameAlpha ) as Number;
			frameWidth = ConvertUtil.getNotNull( getStyle("frameWidth") , frameWidth ) as int;
			
			labelVisible = ConvertUtil.getNotNull( getStyle("labelVisible") , labelVisible ) as Boolean;
			
			iconSrc = getStyle("icon");
			iconValign = ConvertUtil.getNotNull( getStyle("iconValign") , iconValign ) as String;
			iconHalign = ConvertUtil.getNotNull( getStyle("iconHalign") , iconHalign ) as String;
			
			var fileName:String="";
			try {
				if(iconSrc)
					fileName = UriUtil.filename(iconSrc);
			}catch (error:Error) {
				
			}
			
			if (iconSrc != null && iconSrc != "none" && iconSrc != "" && fileName.length>0) {
				var req:URLRequest = new URLRequest(iconSrc);
				var ldr:YLoader = new YLoader();								
				
				ldr.contentLoaderInfo.addEventListener( Event.INIT, 
					function(evt:Event):void {
						if (!_iconMask) {							
							_iconMask = new Sprite();
							addChild(_iconMask);
						}
						var info:LoaderInfo = LoaderInfo( evt.target );						
						
						_icon = info.content;								
						_icon.mask = _iconMask;
						addChild(_icon);
						updateDisplayList();
					}
				);
				ldr.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR,
					function(evt:IOErrorEvent):void {
						var info:LoaderInfo = LoaderInfo( evt.target );
						trace( "icon load error: " + info.url );
					}
				);
				ldr.load( req );
			}
			
			textColor = ConvertUtil.getNotNull( getStyle("color") , textColor ) as int;
			textSize  = ConvertUtil.getNotNull( getStyle("size") , textSize ) as int;

			// style dla pseudo klasy (elementu) "text"
			_stylePseudoClasses = ["text"];
			stylesChanged();
			textColor = ConvertUtil.getNotNull( getStyle("color") , textColor ) as int;
			textSize  = ConvertUtil.getNotNull( getStyle("size") , textSize ) as int;
			
			// style dla pseudo klasy (stanu wizualnego) "over"
			// zamiast odczytywać je po każdej zmianie stanu czytam raz i zapamiętuję
			_stylePseudoClasses = ["over"];
			stylesChanged();
			backgroundOverColor = ConvertUtil.getNotNull( getStyle("backgroundColor") , backgroundOverColor ) as int;
			backgroundOverAlpha = ConvertUtil.getNotNull( getStyle("backgroundAlpha") , backgroundOverAlpha ) as Number;

			labelOverVisible = ConvertUtil.getNotNull( getStyle("labelVisible") , labelOverVisible ) as Boolean;

			shadowOverColor = ConvertUtil.getNotNull( getStyle("shadowColor") , shadowOverColor ) as int;
			shadowOverAlpha = ConvertUtil.getNotNull( getStyle("shadowAlpha") , shadowOverAlpha ) as Number;
			
			frameOverColor = ConvertUtil.getNotNull( getStyle("frameColor") , frameOverColor ) as int;
			frameOverAlpha = ConvertUtil.getNotNull( getStyle("frameAlpha") , frameOverAlpha ) as Number;
			frameOverWidth = ConvertUtil.getNotNull( getStyle("frameWidth") , frameOverWidth ) as int;
			iconOverSrc = getStyle("icon");
			iconOverValign = ConvertUtil.getNotNull( getStyle("iconValign") , iconOverValign ) as String;
			iconOverHalign = ConvertUtil.getNotNull( getStyle("iconHalign") , iconOverHalign ) as String;
			
			
			fileName="";
			try {
				if(iconOverSrc)
					fileName = UriUtil.filename(iconOverSrc);
			}catch (error:Error) {
				
			}
			if (iconOverSrc!=null && iconOverSrc!="none" && iconOverSrc!="" && fileName.length>0) {
				try {
					var req2:URLRequest = new URLRequest(iconOverSrc);
					var ldr2:YLoader = new YLoader();
					ldr2.contentLoaderInfo.addEventListener( Event.INIT, 
						function(evt:Event):void {
							if (!_iconOverMask) {							
								_iconOverMask = new Sprite();
								addChild(_iconOverMask);
							}
							
							var info:LoaderInfo = LoaderInfo( evt.target );
							_iconOver = info.content;
							_iconOver.visible = false;
							_iconOver.mask = _iconOverMask;
							addChild(_iconOver);
							updateDisplayList();
						}
					);
					ldr2.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR,
						function(evt:IOErrorEvent):void {
							var info:LoaderInfo = LoaderInfo( evt.target );
							trace( "over icon load error: " + info.url );
						}
					);
					ldr2.load( req2 );
				} catch (e:Error) {
					trace( "load failed: " + e );
				}
			}
			textOverColor = ConvertUtil.getNotNull( getStyle("color") , textColor ) as int;
			textOverSize  = ConvertUtil.getNotNull( getStyle("size") , textSize ) as int;
			
			// style dla pseudo klas "text" i "over"
			_stylePseudoClasses = ["over"];
			stylesChanged();
			textOverColor = ConvertUtil.getNotNull( getStyle("color") , textOverColor ) as int;
			textOverSize  = ConvertUtil.getNotNull( getStyle("size") , textOverSize ) as int;			
			
			// disabled
			_stylePseudoClasses = ["disabled"];
			stylesChanged();
			
			backgroundDisabledColor = ConvertUtil.getNotNull( getStyle("backgroundColor") , backgroundDisabledColor ) as int;
			backgroundDisabledAlpha = ConvertUtil.getNotNull( getStyle("backgroundAlpha") , backgroundDisabledAlpha ) as Number;
			
			shadowDisabledColor = ConvertUtil.getNotNull( getStyle("shadowColor") , shadowDisabledColor ) as int;
			shadowDisabledAlpha = ConvertUtil.getNotNull( getStyle("shadowAlpha") , shadowDisabledAlpha ) as Number;
			
			labelDisabledVisible = ConvertUtil.getNotNull( getStyle("labelVisible") , labelDisabledVisible ) as Boolean;
			
			frameDisabledColor = ConvertUtil.getNotNull( getStyle("frameColor") , frameDisabledColor ) as int;
			frameDisabledAlpha = ConvertUtil.getNotNull( getStyle("frameAlpha") , frameDisabledAlpha ) as Number;
			frameDisabledWidth = ConvertUtil.getNotNull( getStyle("frameWidth") , frameDisabledWidth ) as int;
			
			iconDisabledSrc = getStyle("icon");
			iconDisabledValign = ConvertUtil.getNotNull( getStyle("iconValign") , iconDisabledValign ) as String;
			iconDisabledHalign = ConvertUtil.getNotNull( getStyle("iconHalign") , iconDisabledHalign ) as String;
			
			fileName="";
			try {
				if(iconDisabledSrc)
					fileName = UriUtil.filename(iconDisabledSrc);
			}catch (error:Error) {
				
			}
			if (iconDisabledSrc!=null && iconDisabledSrc!="none" && iconDisabledSrc!="" && fileName.length>0) {
				try {
					var req3:URLRequest = new URLRequest(iconDisabledSrc);
					var ldr3:YLoader = new YLoader();
					ldr3.contentLoaderInfo.addEventListener( Event.INIT, 
						function(evt:Event):void {
							if (!_iconDisabledMask) {							
								_iconDisabledMask = new Sprite();
								addChild(_iconDisabledMask);
							}
							
							var info:LoaderInfo = LoaderInfo( evt.target );
							_iconDisabled = info.content;
							_iconDisabled.visible = false;
							_iconDisabled.mask = _iconDisabledMask;
							addChild(_iconDisabled);
							updateDisplayList();
						}
					);
					ldr3.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR,
						function(evt:IOErrorEvent):void {
							var info:LoaderInfo = LoaderInfo( evt.target );
							trace( "over icon load error: " + info.url );
						}
					);
					ldr3.load( req3 );
				} catch (e:Error) {
					trace( "load failed: " + e );
				}
			}
			
			textDisabledColor = ConvertUtil.getNotNull( getStyle("color") , textColor ) as int;
			textDisabledSize  = ConvertUtil.getNotNull( getStyle("size") , textSize ) as int;
			
			// style dla pseudo klas "text" i "over"
			_stylePseudoClasses = ["text","over"];
			stylesChanged();

			textDisabledColor = ConvertUtil.getNotNull( getStyle("color") , textDisabledColor ) as int;
			textDisabledSize  = ConvertUtil.getNotNull( getStyle("size") , textDisabledSize ) as int;			

			// usunięcie pseudo klas
			_stylePseudoClasses = [];
			stylesChanged();
			
			// dla zachowania zgodności ze starszymi wersjami
			var bg:XML = areaData.style.background[0];
			if (bg!=null) {
				backgroundColor = ConvertUtil.getColor( bg.@color, backgroundColor );
				backgroundOverColor = ConvertUtil.getColor( bg.@overColor, backgroundOverColor );
				backgroundAlpha = ConvertUtil.getNumber( bg.@alpha, backgroundAlpha);
			}
			
			
		}
		
		protected function setAssets():void {
			var a:Asset;
			for each (var asset:XML in areaData.assets.asset ) {
				a = new Asset();
				a.src = asset.@src;
				a.title = asset.@title;
				//added by ts				
				a.type = ActiveAreaType.getByName(asset.@type);
				a.version = (asset.@version == undefined || asset.@version == "")? null: asset.@version;
				if (a.src!=areaData.@link) {
					assets.push( a );
				}
			}
			if (areaData.@link != null && areaData.@link != "") {
				a = new Asset();
				a.src = areaData.@link;
				a.title = areaData.@title;
				assets.push( a );
			}
		}
		
		
		protected function setTooltip():void {
			if (_tooltip) {				
				var td:TooltipDecorator = new TooltipDecorator( this, _tooltip );
				(td as Object).tooltipDisplayTime = 0;
			}
		}
		
		
		protected function setDescription():void {
			if (_description) {
				var format:TextFormat = new TextFormat("Arial", textSize, textColor );
				_descriptionTextField = new TextField();
				_descriptionTextField.autoSize = TextFieldAutoSize.LEFT;
				_descriptionTextField.wordWrap = true;
				_descriptionTextField.selectable = false;
				_descriptionTextField.type = TextFieldType.DYNAMIC;
				_descriptionTextField.defaultTextFormat = format;
				var lineEnds:RegExp = /\r\n/g;  
				_descriptionTextField.htmlText = _description.replace(lineEnds, "\n");
				addChild( _descriptionTextField );
				updateDisplayList();
			}
		}
		
		//mousecatcher ustawiany aby po najechaniu na
		//pole tekstowe z opisem pojawiała się łapka
		protected function setMouseCatcher():void {
			if (_description){
				if (!_mouseCatcher) {
					_mouseCatcher = new Sprite();
					addChild(_mouseCatcher);
				}
				if (_type == ActiveAreaType.WATERMARK){
					_mouseCatcher.buttonMode = false;
				}else{
					_mouseCatcher.buttonMode = true;
				}
			}


		}
		
		protected function setListeners():void {
			if (!hasEventListener(MouseEvent.ROLL_OVER)) {
				addEventListener( MouseEvent.ROLL_OVER, onMouseOver );
				addEventListener( MouseEvent.ROLL_OUT, onMouseOut );
                addEventListener( MouseEvent.CLICK, open);
                addEventListener( MouseEvent.MOUSE_DOWN, onStartOpen);
			}
		}
		
		protected function initAreaType():void {			
			if (areaData) {				
				//kompatybilnosc empty -> tooltip
				_areaType = areaData.@type;	
				_areaClass = areaData.attribute("class");				
				if (_areaType == ActiveAreaType.TOOLTIP.toString()) {
					var bStyle:Object = getStyle("backgroundColor");
					if(!bStyle){
						
						if(!_areaClass || _areaClass.length==0 || _areaClass==_areaType){
							_areaClass = ActiveAreaType.EMPTY.toString()
						}
						_areaType = ActiveAreaType.EMPTY.toString();
						_styles  = null;
					}
				}
				if (areaData.@isTxt == "true"){
					_areaType = ActiveAreaType.GLOSSARY+"_txt";
					_areaClass = ActiveAreaType.GLOSSARY+"_txt";
					
				}

			}			
		}
		
		
		protected var _areaType:String;
		protected var _areaClass:String;
		// IStyleClient
		public function get styleType():String {			
			return _areaType ? _areaType : areaData.@type;
			//return areaData.@type;
		}
		
		public function get styleClasses():Array {
			var res:Array = _areaClass ? [_areaClass] : [ areaData.attribute("class") ];			
			return res;
		}
		
		public function get styleIds():Array {
			return [];
		}
		
		public function get stylePseudoClasses():Array {
			return _stylePseudoClasses;
		}
		
		/**
		 * obiekt po którym IStyleClient dziedziczy własności stylu
		 */
		public function get styleParent():IStyleClient {
			return null;
		}
		
		/**
		 * tablica dzieci typu IStyleClient
		 * @return Array<IStyleClient>
		 */
		public function get styleChildren():Array {
			return [];
		}
		
		public function setStyle( name:String, value:*):void {
		}
		
		public function getStyle( name:String):* {
			if (_styles == null) {
				_styles = { };
				if(!styleServer){
					try {
						var exts:Array = Nexl.library.list( { type: "com.ydp.style.IStyleServer" } );
						var ext:INexlExtensionDescription = exts[0];
						styleServer = ext.create() as IStyleServer;						
					} catch (e:Error) {
						//trace("ActiveArea::getStyle IStyleServer not found");
					}
				}
				if(styleServer)
					_styles = styleServer.getStyleDescription( this );
			}
			return _styles[name];
		}
		
		public function stylesChanged():void {
			_styles = null;
		}
		
		
		public function set enabled(b:Boolean):void {
			_enabled = b;
			updateDisplayList();
		}
		
		public function get areaData():XML {
			return _conf;
		}
		
		/**
		 * 
		 *	<area x="62" y="259" width="264" height="312" 
		 *		type="video" class="red" 
		 *		isPopup="true" hasButton="true"
		 *		isPlayerUIVisible="true" link="" areaTooltip="" button="" rolloverIcon="">
		 *		<assets>
		 *			<asset src="assets/dummy.flv" title="asset title" version="" />
		 * 			<asset src="assets/dummy.swf" title="asset title" version="avm1" />
		 * 			<asset src="assets/dummy.swf" title="asset title" version="" />
		 *		</assets>
		 *		<areaConf type="polygon">
		 *			<point x="0" y="0" />
		 *			<point x="100" y="0" />
		 *			<point x="50" y="50" />
		 *			<point x="0" y="50" />
		 *		</areaConf>
		 *		<desc><![CDATA[active area description]]></desc>
 		 *	</area>
		 */
		public function set areaData(value:XML):void {
			_conf = value;
			
			if (_conf.areaConf[0]) {
				var areaConf:XML = (_conf.areaConf[0]);
				_shapeType = String(areaConf.@type);
				_shapePoints = areaConf..point;
			}else {
				_shapeType = SHAPE_RECT;
				_shapePoints = new XMLList();
				_shapePoints += <point x="0" y="0" />;
				_shapePoints += <point x={_conf.@width} y={_conf.@height} />;				
			}
			
			type = ActiveAreaType.getByName(value.@type);
			
			if (_type == ActiveAreaType.WATERMARK){
				useHandCursor = false;
				buttonMode = false;
			}else{
				useHandCursor = true;
				buttonMode = true;
			}
			
			_isPopup = (value.@isPopup == "true");
			_description = value.desc;
			_tooltip = value.tooltip;
			
			setStyles();
			setAssets();
			setDescription();
			setTooltip();
			setMouseCatcher();
			setListeners();
			updateDisplayList();
		}
		
		public function get type():ActiveAreaType {
			return _type;
		}
		public function set type(t:ActiveAreaType):void {
			_type = t;			
		}
		
		public function get assets():Array {
			return _assets;
		}
		
		public function highlightAreas(b:Boolean):void {
			var _fill:int = (b)? shadowOverColor : shadowColor;
			var _alpha:Number = (b)? shadowOverAlpha : (_areasVisible) ? shadowAlpha : 0;
			
			var fill:int = (b)? backgroundOverColor : backgroundColor;
			var alpha:Number = (b)? backgroundOverAlpha : backgroundAlpha;
			var borderColor:int = (b)? frameOverColor : frameColor;
			var borderWidth:Number = (b)? frameOverWidth : frameWidth;
			var borderAlpha:Number = (b)? frameOverAlpha : (_areasVisible) ? frameAlpha : 0;
					
			//draw shadow
			graphics.clear();
			graphics.beginFill( _fill, _alpha );
			graphics.drawRect( _conf.@width, 4, 4, _conf.@height);
			graphics.drawRect( 4, _conf.@height, _conf.@width, 4);
			graphics.drawRect( _conf.@width, _conf.@height, 4, 4);
			graphics.endFill();
					
			graphics.lineStyle( borderWidth, borderColor, borderAlpha );
			graphics.drawRect( 0, 0, _conf.@width, _conf.@height );
					
			graphics.beginFill( fill, alpha );
			graphics.drawRect( 0, 0, _conf.@width, _conf.@height );
			graphics.endFill();
		}
		
		
		public function showHotSpotsAreas(b:Boolean):void {
			_areasVisible = b;
			var _fill:int = (b)? shadowColor : shadowColor;
			var _alpha:Number = (b)? shadowAlpha : 0;
			
			var fill:int = (b)? backgroundColor : backgroundColor;
			var alpha:Number = (b)? backgroundAlpha : 0;
			var borderColor:int = (b)? frameColor : frameColor;
			var borderWidth:Number = (b)? frameWidth : frameWidth;
			var borderAlpha:Number = (b)? frameAlpha : 0;
					
			//draw shadow
			graphics.clear();
			graphics.beginFill( _fill, _alpha );
			graphics.drawRect( _conf.@width, 4, 4, _conf.@height);
			graphics.drawRect( 4, _conf.@height, _conf.@width, 4);
			graphics.drawRect( _conf.@width, _conf.@height, 4, 4);
			graphics.endFill();
					
			graphics.lineStyle( borderWidth, borderColor, borderAlpha );
			graphics.drawRect( 0, 0, _conf.@width, _conf.@height );
					
			graphics.beginFill( fill, alpha );
			graphics.drawRect( 0, 0, _conf.@width, _conf.@height );
			graphics.endFill();
		}
		
		//added by ts
		public function set pageData(value:XML) : void
		{
			_pageData = value;
		}
		public function get pageData() : XML
		{
			return _pageData;
		}
		
		public function get assetSrc():String {
			return _assetSrc;
		}
		
		public function set assetSrc(s:String):void {
			_assetSrc = s;
		}

		public function get mouseCatcher():Sprite
		{
			return _mouseCatcher;
		}

		public function set mouseCatcher(value:Sprite):void
		{
			_mouseCatcher = value;
		}
		
		
		
		
		
	}
	
}