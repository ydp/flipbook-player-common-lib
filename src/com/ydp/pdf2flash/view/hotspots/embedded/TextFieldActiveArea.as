package com.ydp.pdf2flash.view.hotspots.embedded 
{
	import com.ydp.pdf2flash.events.HotspotStateEvent;
	import com.ydp.pdf2flash.hotspots.IEmbeddedActiveArea;
	import com.ydp.pdf2flash.hotspots.IStatefulHotspot;
	import com.ydp.pdf2flash.view.hotspots.ActiveArea;
	import com.ydp.utils.ConvertUtil;
	
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	
	public class TextFieldActiveArea extends ActiveArea implements IEmbeddedActiveArea, IStatefulHotspot
	{
		protected var _textInput:TextField;
		protected var _maxLines:int;
		protected var _basePath:String;
		protected var _hotspotId:String;
		protected var _stateData:String;
		
		protected var _showBorderOnFocus:Boolean = false;
		
		public function TextFieldActiveArea() 
		{
			super();
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
		
		//IEmbeddedActiveArea
		public function stop():void
		{
		}
		
		public function get basePath():String
		{
			return _basePath;
		}
		
		public function set basePath(value:String):void
		{
			_basePath = value;
		}
		//
		
		//IStatefulHotspot
		public function get hotspotId():String 
		{
			return _hotspotId;
		}
		
		public function set hotspotId(value:String):void 
		{
			_hotspotId = value;
		}
		
		public function set stateData(value:String):void
		{
			_stateData = value;
			updateTextFieldFromState();
		}
		
		public function get stateData():String
		{
			if (_textInput)
			{
				return _textInput.text;
			}
			if (_stateData)
			{
				return _stateData;
			}
			return "";
		}
		//
		
		override public function set areaData(value:XML):void
		{
			super.areaData = value;
			_hotspotId = value.hasOwnProperty("@id") ? value.@id : "";
			
			var textFieldXml:XML = value.textfield[0];
			if (textFieldXml)
			{
				if (_textInput == null)
				{
					_textInput = new TextField();
					_textInput.type = TextFieldType.INPUT;
					_textInput.addEventListener(FocusEvent.FOCUS_OUT, onLostFocus);
				}
				_textInput.x = textFieldXml.hasOwnProperty("@x") ? Number(textFieldXml.@x) : 0;
				_textInput.y = textFieldXml.hasOwnProperty("@y") ? Number(textFieldXml.@y) : 0;
				_textInput.width = textFieldXml.hasOwnProperty("@width") ? Number(textFieldXml.@width) : 100;
				_textInput.height = textFieldXml.hasOwnProperty("@height") ? Number(textFieldXml.@height) : 20;
				_maxLines = textFieldXml.hasOwnProperty("@lines") ? Number(textFieldXml.@lines) : 1;
				if (isNaN(_maxLines) || _maxLines == 1)
				{
					_textInput.multiline = false;
					_textInput.addEventListener(Event.CHANGE, onTextInputSingleline);
				}
				else
				{
					_textInput.multiline = true;
					_textInput.wordWrap = true;
					_textInput.addEventListener(Event.CHANGE, onTextInputMultiline);
				}
				var textInputFormat:TextFormat = new TextFormat();
				if (textFieldXml.hasOwnProperty("@underline"))
				{
					textInputFormat.underline = textFieldXml.@underline == "true";
				}
				if (textFieldXml.hasOwnProperty("@italic"))
				{
					textInputFormat.italic = textFieldXml.@italic == "true";
				}
				if (textFieldXml.hasOwnProperty("@bold"))
				{
					textInputFormat.bold = textFieldXml.@bold == "true";
				}
				if (textFieldXml.hasOwnProperty("@leading"))
				{
					textInputFormat.leading = Number(textFieldXml.@leading);
				}
				if (textFieldXml.hasOwnProperty("@color"))
				{
					textInputFormat.color = Number(textFieldXml.@color);
				}
				if (textFieldXml.hasOwnProperty("@size"))
				{
					textInputFormat.size = Number(textFieldXml.@size);
				}
				_textInput.defaultTextFormat = textInputFormat;
				addChild(_textInput);
				
				frameAlpha = ConvertUtil.getNotNull( getStyle("frameAlpha") , 0 ) as Number;
				
				if (frameAlpha > 0)
				{
					_showBorderOnFocus = true;
				}
				
				updateTextFieldFromState();
				
			}
		}
		
		protected function onTextInputSingleline(e:Event):void
		{
			_textInput.scrollH = 0;
			_textInput.scrollV = 0;
			var tt:String = _textInput.text;
			while (_textInput.textWidth > _textInput.width - 4 && tt.length > 1)
			{
				_textInput.text = tt.substring(0, tt.length - 1);
				tt = _textInput.text;
			}
			_stateData = _textInput.text;
		}
		
		protected function onTextInputMultiline(e:Event):void
		{
			_textInput.scrollH = 0;
			_textInput.scrollV = 0;
			var tt:String = _textInput.text;
			while(_textInput.numLines > _maxLines && tt.length > 1) 
			{
				_textInput.text = tt.substring(0, tt.length - 1);
				tt = _textInput.text;
			}
			_stateData = _textInput.text;
		}
		
		protected function updateTextFieldFromState():void
		{
			if (_textInput && _stateData)
			{
				_textInput.text = _stateData;
			}
		}
		
		protected function onLostFocus(e:FocusEvent):void
		{
			if(_textInput)
			{
				var width:int =  ConvertUtil.getNotNull( getStyle("frameWidth") , frameWidth ) as int;
				frameWidth = width;
				updateDisplayList();
			}
			dispatchEvent(new HotspotStateEvent(HotspotStateEvent.UPDATED, _hotspotId, stateData));
		}
		
		override public function open(evt:Event = null):void 
		{
			var width:int =  ConvertUtil.getNotNull( getStyle("frameWidth") , frameWidth ) as int;
			frameWidth = width+1;
			updateDisplayList();
		}
		
		override protected function setMouseCatcher():void 
		{
			super.setMouseCatcher();
			if (mouseCatcher)
			{
				mouseCatcher.buttonMode = false;
			}
		}
		
		protected function onRemovedFromStage(e:Event):void
		{
			if (_textInput)
			{
				_textInput.removeEventListener(Event.CHANGE, onTextInputMultiline);
				_textInput.removeEventListener(Event.CHANGE, onTextInputSingleline);
				_textInput.removeEventListener(FocusEvent.FOCUS_OUT, onLostFocus);
				removeChild(_textInput);
				_textInput = null;
			}
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
	}
}