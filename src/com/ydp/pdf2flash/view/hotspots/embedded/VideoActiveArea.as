﻿package com.ydp.pdf2flash.view.hotspots.embedded
{
	import com.ydp.pdf2flash.events.ActiveAreaEvent;
	import com.ydp.pdf2flash.hotspots.ActiveAreaType;
	import com.ydp.pdf2flash.hotspots.IEmbeddedActiveArea;
	import com.ydp.pdf2flash.view.hotspots.ActiveArea;
	import com.ydp.style.IStyleClient;
	import com.ydp.utils.UriUtil;
	import com.ydp.utils.YLoader;
	
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.NetStatusEvent;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.net.URLRequest;
	import flash.text.TextField;
	
	public class VideoActiveArea extends ActiveArea implements IEmbeddedActiveArea
	{
		protected var _isPlaying:Boolean
		
		//skin
		protected var video:Video = new Video();
		protected var previewImage:YLoader = new YLoader();
		protected var _basePath:String;
		//protected var skin:MovieClip;
		
		protected var nc:NetConnection;
		protected var ns:NetStream ;
		protected var videoUrl:String;
		
		protected var secondTry:Boolean = false;
		
		public function VideoActiveArea()
		{
			init()
			super();
		}
		
		public function get basePath():String
		{
			return _basePath;
		}
		
		public function set basePath(value:String):void
		{
			false;
			_basePath = value;
		}
		
		public function stop():void
		{
			if (ns)
			{
				ns.removeEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
				ns.pause();
				ns.close();
			}
			if (nc)
			{
				nc.close();
			}
			
			previewImage.visible = true;
			video.visible = false;
			_isPlaying = false;
		}

		public function init():void
		{
			video.smoothing = true;
			video.visible = false;
			this.addChild(video);
			this.addChild(previewImage);
		}
		
		override public function set areaData(value:XML):void
		{
			super.areaData =value;
			
			
			var previewUrl:String;
			
			
			for each (var i:XML in areaData.assets.asset) 
			{
				if (i.@type == "preview")
				{
					previewUrl = String(i.@src)
				}
				else
				{
					videoUrl = String(i.@src)
				}
			}
			
			if (previewUrl != "")
			{
				loadPreviewImage(previewUrl);
			}
			
			
			
			video.width = areaData.@width;
			video.height = areaData.@height;
		}
		
		protected function loadPreviewImage(url:String):void
		{
			
			
			url = UriUtil.resolvePath(basePath, url);
			
			var request:URLRequest = new URLRequest(url);
			
			previewImage.contentLoaderInfo.addEventListener(Event.COMPLETE, onPreviewImageLoadComplete);
			
			try
			{
				previewImage.load(request);
			} 
			catch(error:Error) 
			{
				trace (error);
			}
			
			
		}
		
		protected function onPreviewImageLoadComplete (e:Event):void{
			previewImage.width = areaData.@width;
			previewImage.height = areaData.@height;
		}
		
		override public function open(evt:Event = null):void {
			
			if (!_isPlaying){
				
				var url:String = UriUtil.resolvePath(basePath, videoUrl);
				root.loaderInfo.loaderURL
				
				nc = new NetConnection();
				nc.connect(null);
				ns = new NetStream(nc);
				ns.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
				ns.client = this
				video.attachNetStream(ns);
				ns.play(url);
				
				
				_isPlaying = true;
				//previewImage.content.visible = false;
				previewImage.visible = false;
				video.visible = true;
			}
			else
			{
				stop();
			}
			
			
		}

		
		
		private function netStatusHandler(evt:NetStatusEvent):void {
			switch(evt.info.code)
			{
				case "NetStream.Play.Stop":
				{
					stop();
					break;
				}
				case "NetStream.Play.StreamNotFound":
				{
		
					stop();
					if (!secondTry)
					{
						basePath = "../" + basePath;
						secondTry = true;
						
						open(null);
					}
					break;
				}
					
				default:
				{
					break;
				}
			}
			
		}
		
		public function onMetaData(infoObject:Object):void 
		{ 
			trace ("METADANE", infoObject);
		}
		
		public function onXMPData(infoObject:Object):void 
		{ 
			trace ("METADANE", infoObject);
		}
	}
	
}

