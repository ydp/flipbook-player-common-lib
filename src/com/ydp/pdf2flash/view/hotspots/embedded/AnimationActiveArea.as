﻿package com.ydp.pdf2flash.view.hotspots.embedded
{
	import com.senocular.utils.SWFReader;
	import com.ydp.pdf2flash.events.ActiveAreaEvent;
	import com.ydp.pdf2flash.hotspots.ActiveAreaType;
	import com.ydp.pdf2flash.hotspots.IEmbeddedActiveArea;
	import com.ydp.pdf2flash.view.hotspots.ActiveArea;
	import com.ydp.style.IStyleClient;
	import com.ydp.utils.UriUtil;
	import com.ydp.utils.YLoader;
	
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.NetStatusEvent;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	
	public class AnimationActiveArea extends ActiveArea implements IEmbeddedActiveArea
	{
		protected var _isPlaying:Boolean
		
		protected var loader:YLoader = new YLoader();
		protected var previewImage:YLoader = new YLoader();
		protected var _basePath:String;
		//protected var skin:MovieClip;
		
		protected var _position:Number = 0;	
		protected var _frameRate:Number = 24;
		
		protected var animationUrl:String;
		
		public function AnimationActiveArea()
		{
			init()
			super();
		}
		
		public function get basePath():String
		{
			return _basePath;
		}

		public function set basePath(value:String):void
		{
			_basePath = value;
		}

		public function stop():void
		{
			_position = 0;
						
			previewImage.visible = true;

			
			if (loader) {
				if (loader && loader.content is MovieClip) {
					(loader.content as MovieClip).removeEventListener(Event.ENTER_FRAME, stopMovieOnLastFrame);
					(loader.content as MovieClip).gotoAndStop(1);
				}
				loader.unload();
				loader.visible = false;
			}
			_isPlaying = false;
		}
		
		public function init():void
		{
			loader.visible = false;

			this.addChild(loader);
			this.addChild(previewImage);
		}
		
		override public function set areaData(value:XML):void
		{
			super.areaData =value;
			

			var previewUrl:String;
			
			
			for each (var i:XML in areaData.assets.asset) 
			{
				if (i.@type == "preview")
				{
					previewUrl = String(i.@src)
				}
				else
				{
					animationUrl = String(i.@src)
				}
			}
			
			if (previewUrl != "")
			{
				loadPreviewImage(previewUrl);
			}
			
		}
		
		protected function loadPreviewImage(url:String):void
		{						
			url = UriUtil.resolvePath(basePath, url);
			
			var request:URLRequest = new URLRequest(url);
			
			previewImage.contentLoaderInfo.addEventListener(Event.COMPLETE, onPreviewImageLoadComplete);
			
			try
			{
				previewImage.load(request);
			} 
			catch(error:Error) 
			{
				trace (error);
			}
			
			
		}
		
		protected function onPreviewImageLoadComplete (e:Event):void{
			previewImage.width = areaData.@width;
			previewImage.height = areaData.@height;
		}
		
		override public function open(evt:Event = null):void {
		
			if (!_isPlaying){
				
				var url:String = UriUtil.resolvePath(basePath, animationUrl);
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onContentLoaded);
				loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onContentLoadingError);

				loader.load(new URLRequest(url));
				previewImage.visible = false;
				loader.visible = true;
				_isPlaying = true;
			}
			else
			{
				stop();
			}
			
		}
		
		protected function stopMovieOnLastFrame(event:Event):void {
			var movie:MovieClip = event.target as MovieClip;			
			if (movie.currentFrame == movie.totalFrames) {				
				stop();		
			}
		}
		

		
		
		protected function onContentLoaded(event:Event):void {			
			
			event.target.removeEventListener(Event.COMPLETE, onContentLoaded);
			event.target.removeEventListener(IOErrorEvent.IO_ERROR, onContentLoadingError);							
			
			var inputBytes:ByteArray = loader.contentLoaderInfo.bytes;
			var hr:SWFReader = new SWFReader();			
			hr.parse(inputBytes);		
			var _contentIsSWF:Boolean = hr.parsed;
			
			if (_contentIsSWF && (hr.version < 9 || hr.asVersion < 3)) {				
				stop();
				trace ("Error: Content is AVM1!")
				return
			}
			
			//_swfSize = _size.clone();
			
			var movie:MovieClip = (loader.content as MovieClip);
			
			movie.width= areaData.@width;
			movie.height = areaData.@height;
			
			movie.addEventListener(Event.ENTER_FRAME, stopMovieOnLastFrame);
			movie.play();
		}
		
		protected function onContentLoadingError(event:Event):void {
			event.target.removeEventListener(Event.COMPLETE, onContentLoaded);
			event.target.removeEventListener(IOErrorEvent.IO_ERROR, onContentLoadingError);
			
			stop();
		}
	}
		
}