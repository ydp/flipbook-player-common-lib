﻿package com.ydp.pdf2flash.view.hotspots.embedded
{
	import com.ydp.pdf2flash.events.ActiveAreaEvent;
	import com.ydp.pdf2flash.hotspots.ActiveAreaType;
	import com.ydp.pdf2flash.hotspots.IEmbeddedActiveArea;
	import com.ydp.pdf2flash.view.hotspots.ActiveArea;
	import com.ydp.style.IStyleClient;
	import com.ydp.utils.UriUtil;
	import com.ydp.utils.YLoader;
	
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.NetStatusEvent;
	import flash.media.Sound;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.media.SoundChannel;
	
	public class AudioActiveArea extends ActiveArea implements IEmbeddedActiveArea
	{
		protected var _isPlaying:Boolean
		

		protected var previewImage:YLoader = new YLoader();
		protected var _basePath:String;
		//protected var skin:MovieClip;

		protected var audioUrl:String;
		protected var snd:Sound;
		protected var channel:SoundChannel = new SoundChannel();
		
		public function AudioActiveArea()
		{
			init()
			super();
		}
		
		public function get basePath():String
		{
			return _basePath;
		}

		public function set basePath(value:String):void
		{
			_basePath = value;
		}

		public function stop():void
		{
			if (_isPlaying)
			{
				channel.stop();
				//snd = null;
			}
			_isPlaying = false;
		}
		
		public function init():void
		{
			channel.addEventListener(Event.SOUND_COMPLETE, onSoudComplete)
			this.addChild(previewImage);
		}
		
		protected function onSoudComplete(event:Event):void
		{
			stop();
		}
		
		override public function set areaData(value:XML):void
		{
			super.areaData =value;
			

			var previewUrl:String;
			
			
			for each (var i:XML in areaData.assets.asset) 
			{
				if (i.@type == "preview")
				{
					previewUrl = String(i.@src)
				}
				else
				{
					audioUrl = String(i.@src)
				}
			}
			
			if (previewUrl != "")
			{
				loadPreviewImage(previewUrl);
			}
			
		}
		
		protected function loadPreviewImage(url:String):void
		{
	
						
			url = UriUtil.resolvePath(basePath, url);
			
			var request:URLRequest = new URLRequest(url);
			
			previewImage.contentLoaderInfo.addEventListener(Event.COMPLETE, onPreviewImageLoadComplete);
			
			try
			{
				previewImage.load(request);
			} 
			catch(error:Error) 
			{
				trace (error);
			}
			
			
		}
		
		protected function onPreviewImageLoadComplete (e:Event):void{
			previewImage.width = areaData.@width;
			previewImage.height = areaData.@height;
		}
		
		override public function open(evt:Event = null):void {
		
			if (!_isPlaying){
				
				var url:String = UriUtil.resolvePath(basePath, audioUrl);
				
				var request:URLRequest = new URLRequest(url);
				
				snd = new Sound();
				snd.load(request);
				snd.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
				_isPlaying = true;
				function onComplete(evt:Event):void {
					channel = snd.play();
				}
				
				
			}
			else
			{
				stop();
			}
			
			
		}
		
	
	}
		
}