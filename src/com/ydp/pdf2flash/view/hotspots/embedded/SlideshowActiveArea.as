﻿package com.ydp.pdf2flash.view.hotspots.embedded
{
	import com.ydp.pdf2flash.events.ActiveAreaEvent;
	import com.ydp.pdf2flash.hotspots.ActiveAreaType;
	import com.ydp.pdf2flash.hotspots.IEmbeddedActiveArea;
	import com.ydp.pdf2flash.view.hotspots.ActiveArea;
	import com.ydp.style.IStyleClient;
	import com.ydp.utils.UriUtil;
	import com.ydp.utils.YLoader;
	import com.ydp.view.ButtonDecorator;
	
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.geom.Rectangle;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.net.URLRequest;
	import flash.text.TextField;
	
	import pl.ydp.nexl.INexlExtensionDescription;
	import pl.ydp.nexl.Nexl;
	
	public class SlideshowActiveArea extends ActiveArea implements IEmbeddedActiveArea
	{
		
		protected var _basePath:String;
		protected var _inited:Boolean;
		
		protected var _nextArrow:ButtonDecorator;
		protected var _prevArrow:ButtonDecorator;
		protected var _indicator:MovieClip;
		
		protected var _currentSlide:int;
		protected var _numSlides:uint;
		//protected var skin:MovieClip;
		protected var image:YLoader = new YLoader();
		
		protected var slideShowWidth:Number = 0;
		protected var slideShowHeight:Number = 0;
		
		
		public function SlideshowActiveArea()
		{
			init()
			super();
		}
		
		public function get basePath():String
		{
			return _basePath;
		}
		
		public function set basePath(value:String):void
		{
			_basePath = value;
		}
		
		public function stop():void
		{
			
		}
		
		public function init():void
		{
			
			
			if (_inited)
			{
				return;
				
			}
			this.addChild(image);
			var exts:Array = Nexl.library.list( { baseClass:"flash.display.MovieClip", id:"media_player_skin" } );
			if (exts.length > 0) {				
				var ext:INexlExtensionDescription = exts[0] as INexlExtensionDescription;
				var skin:MovieClip = ext.create() as MovieClip;				
				_nextArrow = new ButtonDecorator(skin.arrowNext);
				_nextArrow.addEventListener(ButtonDecorator.CLICK, onNextPressed);
				_prevArrow = new ButtonDecorator(skin.arrowPrev);
				_prevArrow.addEventListener(ButtonDecorator.CLICK, onPrevPressed);
				_indicator = skin.embededSlideNumMc;
				addChild(_nextArrow.clip);
				addChild(_prevArrow.clip);
				addChild(_indicator);
			}
			
			this.addEventListener(MouseEvent.DOUBLE_CLICK, preventDubleclick)
			
			_inited = true;
			
		}
		
		protected function preventDubleclick(e:MouseEvent):void {
			e.preventDefault();
			e.stopImmediatePropagation();
		}
		
		
		override public function set areaData(value:XML):void
		{
			super.areaData =value;
					
			slideShowWidth = areaData.@width;
			slideShowHeight = areaData.@height;

			
			_nextArrow.clip.x = slideShowWidth-_nextArrow.clip.width;
			_prevArrow.clip.x = 0;
			_nextArrow.clip.y = _prevArrow.clip.y = slideShowHeight/2;
			_numSlides = assets.length;
			
			_indicator.x = 0;
			_indicator.y = slideShowHeight;
			_indicator.bg.width = slideShowWidth;
			
			useHandCursor = false;
			buttonMode = false;
			load();
			
		}
		
		override protected function setMouseCatcher():void {
			
		}
		
		protected function loadSlideNum(value:uint):void
		{
			var slide:XML = XML( areaData..asset[_currentSlide]);
			
			var url:String = UriUtil.resolvePath(basePath, slide.@src);
			
			var request:URLRequest = new URLRequest(url);
			
			image.contentLoaderInfo.addEventListener(Event.COMPLETE, onImageLoadComplete);
			
			updateIndicator();
			
			
			try
			{
				image.load(request);
			} 
			catch(error:Error) 
			{
				trace (error);
			}
			
			
		}
		
		protected function onImageLoadComplete (e:Event):void{
			image.contentLoaderInfo.removeEventListener(Event.COMPLETE, onImageLoadComplete);
			image.width = slideShowWidth;
			image.height = slideShowHeight;
		}
		
		public function updateIndicator():void {
			
			
			var txtField:TextField = _indicator.tfSlideNum;
			
			var txt:String = "("+String(_currentSlide+1)+"/"+String(_numSlides)+")";
			
			txtField.x = (_indicator.width - txtField.width)/2;
			
			txtField.text = txt;
			
			
		}
		
		override public function open(evt:Event = null):void {
			evt.stopImmediatePropagation();
			evt.preventDefault();
		}
		
		
		public function load():void {
			if (areaData) {
				_currentSlide = 0;
				loadSlideNum(_currentSlide);
			}
			
		}
		
		protected function onNextPressed(e:Event):void {
			_currentSlide++;
			if (_currentSlide>=_numSlides)
				_currentSlide=0;
			loadSlideNum(_currentSlide);
		}
		
		protected function onPrevPressed(e:Event):void {
			
			_currentSlide--;
			if (_currentSlide<0)
				_currentSlide=_numSlides-1;
			loadSlideNum(_currentSlide);
		}
		

	}
	
}