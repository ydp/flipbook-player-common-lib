package com.ydp.pdf2flash.view.hotspots 
{
	import com.ydp.pdf2flash.view.hotspots.ActiveArea;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.BlendMode;
	import flash.text.TextFormat;

	public class HideSpotActiveArea extends ActiveArea 
	{
		private var _hidespotVisibility:Boolean = true;
		
		override public function set areaData(value:XML):void 
		{
			super.areaData = value;
			this.addEventListener(MouseEvent.CLICK, onHidespotClick);
		}
		
		private function onHidespotClick(e:MouseEvent):void 
		{
			_hidespotVisibility = !_hidespotVisibility;
			updateDisplayList();
		}
		
		override public function updateDisplayList(evt:Event = null):void {
			var isMouseOver:Boolean = false;
			var format:TextFormat;
			if (evt && evt is MouseEvent && evt.type==MouseEvent.ROLL_OVER) {
				isMouseOver = true;
			}
			
			if (_conf && _conf is XML) {
				if (_mouseCatcher) {
					var gfx:Graphics = _mouseCatcher.graphics;
					gfx.clear();
					gfx.beginFill( 0x00ff00, 0 );
					drawShape(gfx, _shapeType, _shapePoints);
					gfx.endFill();
				}
				if (_iconMask) {					
					gfx = _iconMask.graphics;
					gfx.clear();
					gfx.beginFill( 0x00ff00, 1 );
					drawShape(gfx, _shapeType, _shapePoints);
					gfx.endFill();
				}
				
				if (_iconOverMask) {					
					gfx = _iconOverMask.graphics;
					gfx.clear();
					gfx.beginFill( 0x00ff00, 1 );
					drawShape(gfx, _shapeType, _shapePoints);
					gfx.endFill();
				}
				
				if (_iconDisabledMask) {					
					gfx = _iconDisabledMask.graphics;
					gfx.clear();
					gfx.beginFill( 0x00ff00, 1 );
					drawShape(gfx, _shapeType, _shapePoints);
					gfx.endFill();
				}
				
				
				if (_enabled) {
					var _fill:int = (isMouseOver)? shadowOverColor : shadowColor;
					var _alpha:Number = (isMouseOver)? shadowOverAlpha : (_areasVisible) ? shadowAlpha : 0;
					
					var fill:int = (isMouseOver)? backgroundOverColor : backgroundColor;
					var alpha:Number;
					if (_hidespotVisibility)
					{
						alpha = (isMouseOver)? backgroundOverAlpha : (_areasVisible) ? backgroundAlpha : 0;
					}
					else
					{
						alpha = 0;
					}
					var borderColor:int = (isMouseOver)? frameOverColor : frameColor;
					var borderWidth:Number = (isMouseOver)? frameOverWidth : frameWidth;
					var borderAlpha:Number = (isMouseOver)? frameOverAlpha : (_areasVisible) ? frameAlpha : 0;
										
					if (!_shadowSprite) {
						_shadowSprite = new Sprite();
						_shadowSprite.cacheAsBitmap = true;
						_shadowSprite.blendMode = BlendMode.LAYER;
						addChildAt(_shadowSprite, 0);
						_shadowSpriteMask = new Sprite();						
						_shadowSprite.addChild(_shadowSpriteMask);						
					}
					_shadowSprite.graphics.clear();
					_shadowSprite.graphics.beginFill(_fill, _alpha);
					drawShape(_shadowSprite.graphics, _shapeType, _shapePoints, 4, 4);						
					_shadowSprite.graphics.endFill();
					
					_shadowSpriteMask.cacheAsBitmap = true;
					_shadowSpriteMask.blendMode = BlendMode.ERASE;
					_shadowSpriteMask.graphics.clear();
					_shadowSpriteMask.graphics.beginFill(0x000000, 1);
					drawShape(_shadowSpriteMask.graphics, _shapeType, _shapePoints);						
					_shadowSpriteMask.graphics.endFill();
					
					
					graphics.clear();
					graphics.lineStyle( borderWidth, borderColor, borderAlpha );
					drawShape(graphics, _shapeType, _shapePoints);
					
					graphics.beginFill( fill, alpha );
					drawShape(graphics, _shapeType, _shapePoints);
					graphics.endFill();
					
					
					if (_iconDisabled) {
						_iconDisabled.visible = false;
					}
					var currentIcon:DisplayObject;
					var currentHAlign:String;
					if (_icon) {
						_icon.visible = !(isMouseOver);
						if(_icon.visible){
							currentIcon = _icon;
							currentHAlign = iconHalign;
						}
						positionIcon(_icon, iconValign, iconHalign);
						if (_icon is MovieClip && MovieClip(_icon).totalFrames > 0) {
							MovieClip(_icon).gotoAndPlay( 1 );
						}
					}
					if (_iconOver) {
						_iconOver.visible = (isMouseOver);
						if(_iconOver.visible){
							currentIcon = _iconOver;
							currentHAlign = iconOverHalign;
						}
						positionIcon(_iconOver, iconOverValign, iconOverHalign);
						if (_iconOver is MovieClip && MovieClip(_iconOver).totalFrames > 0) {
							MovieClip(_iconOver).gotoAndPlay( 1 );
						}
					}
					if (_descriptionTextField) {
						_descriptionTextField.visible = (isMouseOver) ? labelOverVisible: labelVisible;
						var size:int = (isMouseOver)? textOverSize : textSize;
						var color:int = (isMouseOver)? textOverColor : textColor;
																		
						format = new TextFormat("Arial", size, color);
						_descriptionTextField.setTextFormat( format, 0, _descriptionTextField.text.length );
						_descriptionTextField.width = currentIcon ? _conf.@width - currentIcon.width : _conf.@width;						
						positionText(currentIcon, currentHAlign);
					}
				} else {
					
					if (!_shadowSprite) {
						_shadowSprite = new Sprite();
						_shadowSprite.cacheAsBitmap = true;
						_shadowSprite.blendMode = BlendMode.LAYER;
						addChildAt(_shadowSprite, 0);
						_shadowSpriteMask = new Sprite();						
						_shadowSprite.addChild(_shadowSpriteMask);						
					}
					
					_shadowSprite.graphics.clear();
					_shadowSprite.graphics.beginFill(shadowDisabledColor, shadowDisabledAlpha);
					drawShape(_shadowSprite.graphics, _shapeType, _shapePoints, 4, 4);						
					_shadowSprite.graphics.endFill();
					
					_shadowSpriteMask.cacheAsBitmap = true;
					_shadowSpriteMask.blendMode = BlendMode.ERASE;
					_shadowSpriteMask.graphics.clear();
					_shadowSpriteMask.graphics.beginFill(0x000000, 1);
					drawShape(_shadowSpriteMask.graphics, _shapeType, _shapePoints);						
					_shadowSpriteMask.graphics.endFill();
														
					graphics.clear();
					graphics.beginFill( backgroundDisabledColor, backgroundDisabledAlpha );
					graphics.lineStyle( frameDisabledWidth, frameDisabledColor, frameDisabledAlpha );
					drawShape(graphics, _shapeType, _shapePoints);
					graphics.endFill();
					graphics.beginFill( backgroundDisabledColor, backgroundDisabledAlpha );
					drawShape(graphics, _shapeType, _shapePoints);
					graphics.endFill();
					
					if (_icon)
						_icon.visible = false;
					if (_iconOver)
						_iconOver.visible = false;
					if (_iconDisabled) {
						_iconDisabled.visible = true;
						positionIcon(_iconDisabled, iconDisabledValign, iconDisabledHalign);
					}
					if (_iconDisabled is MovieClip && MovieClip(_iconDisabled).totalFrames > 0) {
						MovieClip(_iconDisabled).gotoAndPlay( 1 );
					}
					if (_descriptionTextField) {
						_descriptionTextField.visible = labelDisabledVisible;
						format = new TextFormat("Arial", textDisabledSize, textDisabledColor);
						_descriptionTextField.setTextFormat( format, 0, _descriptionTextField.text.length );
					}
				}
			}
		}
	}
}