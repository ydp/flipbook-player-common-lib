﻿package com.ydp.pdf2flash.view.hotspots {
	
	import com.ydp.pdf2flash.events.ActiveAreaEvent;
	import com.ydp.pdf2flash.events.HotspotStateEvent;
	import com.ydp.pdf2flash.hotspots.Asset;
	import com.ydp.pdf2flash.hotspots.IActiveArea;
	import com.ydp.pdf2flash.hotspots.IEmbeddedActiveArea;
	import com.ydp.pdf2flash.hotspots.IStatefulHotspot;
	import com.ydp.pdf2flash.model.IGlossary;
	import com.ydp.pdf2flash.model.vo.HotspotStateVO;
	import com.ydp.pdf2flash.view.hotspots.ActiveArea;
	import com.ydp.pdf2flash.view.hotspots.HideSpotActiveArea;
	import com.ydp.pdf2flash.view.hotspots.embedded.AnimationActiveArea;
	import com.ydp.pdf2flash.view.hotspots.embedded.AudioActiveArea;
	import com.ydp.pdf2flash.view.hotspots.embedded.SlideshowActiveArea;
	import com.ydp.pdf2flash.view.hotspots.embedded.TextFieldActiveArea;
	import com.ydp.pdf2flash.view.hotspots.embedded.VideoActiveArea;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.ui.Mouse;
	
	/**
	 * Creates and displays ActiveAreas
	 * @author Michal Samujlo
	 */
	public class ActiveAreaContainer extends Sprite {
		private var _model:XML;
		
		private var _areas:Array;
		private var _hotspotStates:Array;
		public var xoffset:Number = 0;
		public var yoffset:Number = 0;
		private var _enabled:Boolean = true;
		private var _zoomEnabled = true;
		//
		public var pageData:XML;
		private var _glossaryAreas:Array;
		private var _areasVisible:Boolean = true;
		private var _basePath:String;
		private var _userProfileId:String;
		
		
		
		
		public function ActiveAreaContainer() {
			_userProfileId = userProfileId;
			_areas = new Array();
			_hotspotStates = new Array();
		}
		
		public function get glossaryAreas():Array
		{
			
			return _glossaryAreas;
		}
		
		public function set glossaryAreas(value:Array):void
		{
			trace ("DODODAJE MODEL GLOSSARY");
			_glossaryAreas = value;
			removeActiveAreas();
			createActiveAreas();
		}
		
		public function removeActiveAreas(clearModel:Boolean = false):void {
			stopAllHotspots();		
			
			if (clearModel){
				_model = null;
				_glossaryAreas = null;
				trace ("CZYSZCZE MODEL");
			}
			
			for each (var area:IActiveArea in _areas) {
				removeChild( DisplayObject(area) );
			}
			trace ("USUWAM HOTSPOTY");
			_areas = new Array();
		}
		
		
		private function createActiveAreas():void {
			trace ("RYSUJE HOTSPOT..." , Boolean(_model), (_glossaryAreas)? glossaryAreas.length:false);
			if (_model){
				var activeArea:IActiveArea 
				for each (var area:XML in _model..area) 
				{
					
					if(!isAssetAvailableForCurrentUserProfile(area) )
					{
						continue;
					}
					
					var areaType:String = area.@type;
					if (area.@embedded != "true")
					{
						if (areaType == "textfield")
						{
							activeArea = new TextFieldActiveArea();
						}
						else if (areaType == "hide_spot")
						{
							activeArea = new HideSpotActiveArea();
						}
						else
						{
							activeArea = new ActiveArea();
						}
					}
					else
					{
						switch(areaType)
						{
							case "video":
							{
								activeArea = new VideoActiveArea();
								break;
							}
							case "slideshow":
							{
								activeArea = new SlideshowActiveArea();
								break;
							}		
							case "audio":
							{
								activeArea = new AudioActiveArea();
								break;
							}
							case "animation":
							{
								activeArea = new AnimationActiveArea();
								break;
							}
							case "textfield":
							{
								activeArea = new TextFieldActiveArea();
								break;
							}
							default:
							{
								activeArea = new ActiveArea();
								break;
							}
						}
					}
					
					if (activeArea is IEmbeddedActiveArea)
					{
						(activeArea as IEmbeddedActiveArea).basePath = basePath;
					}
					
					if (activeArea is IStatefulHotspot)
					{
						(activeArea as DisplayObject).addEventListener(HotspotStateEvent.UPDATED, onHotspotStateUpdated);					
					}
					
					activeArea.enabled = enabled;
					activeArea.zoomEnabled = _zoomEnabled;
					activeArea.x = Number(area.@x) + xoffset;
					activeArea.y = Number(area.@y) + yoffset;
					activeArea.areaData = area;
					
					//added by ts
					activeArea.pageData = pageData;
					if (areaType != "textfield")
					{
						activeArea.addEventListener( ActiveAreaEvent.OPEN, onAreaOpened );
						activeArea.addEventListener( MouseEvent.CLICK, stopAllHotspots );
					}
					activeArea.addEventListener( MouseEvent.MOUSE_OVER, onAreaMouseOver );
					activeArea.addEventListener( MouseEvent.MOUSE_OUT, onAreaMouseOut );
					
					_areas.push( activeArea );
					addChild( DisplayObject(activeArea) );
				}
			}
			
			if (_glossaryAreas){
				for each (var glossaryArea:IActiveArea in glossaryAreas) 
				{
					glossaryArea.x = Number(glossaryArea.areaData.@x) + xoffset;
					glossaryArea.y = Number(glossaryArea.areaData.@y) + yoffset;
					glossaryArea.addEventListener( ActiveAreaEvent.OPEN, onAreaOpened );
					glossaryArea.addEventListener( MouseEvent.MOUSE_OVER, onAreaMouseOver );
					glossaryArea.addEventListener( MouseEvent.MOUSE_OUT, onAreaMouseOut );
					glossaryArea.addEventListener( MouseEvent.CLICK, stopAllHotspots );
					_areas.push( glossaryArea );
					addChild( DisplayObject(glossaryArea) );
				}
			}
			
			
			
			
			
			
			for each (var aa:IActiveArea in _areas) {
				aa.showHotSpotsAreas(_areasVisible);
			}
		}
		
		
		
		
		private function onAreaMouseOver(evt:MouseEvent):void {
			dispatchEvent( new ActiveAreaEvent( ActiveAreaEvent.MOUSE_OVER, evt.target as IActiveArea ) );
		}
		
		public function stopAllHotspots(evt:MouseEvent=null):void {
			
			var needStopAllPlayers:Boolean = false;
			var area:IEmbeddedActiveArea;
			for (var i:int = 0; i < _areas.length; i++) 
			{
				area = _areas[i] as IEmbeddedActiveArea;
				if (area && (!evt || area!=evt.currentTarget))
				{
					area.stop();
				}
				
				if (evt)
				{
					needStopAllPlayers = true;
				}
			}
			
			if (needStopAllPlayers)
			{
				dispatchEvent(new Event("startPlaying"));
			}
		}
		
		
		private function onAreaMouseOut(evt:MouseEvent):void {
			dispatchEvent( new ActiveAreaEvent( ActiveAreaEvent.MOUSE_OUT, evt.target as IActiveArea ) );
		}
		
		private function onAreaOpened(evt:ActiveAreaEvent):void {
			dispatchEvent( evt.clone() );
		}
		
		/**
		 * searches and opens area that has asset linked to assetSrc
		 * @return true if area is found
		 */
		public function openAsset( assetSrc:String ):Boolean {			
			for each (var aa:IActiveArea in _areas) {
				for each (var asset:Asset in aa.assets) {
					// remove basePath
					var ind:int = assetSrc.length - asset.src.length;
					var src:String = assetSrc.substring( ind );
					if (src.length>0 &&  src == asset.src ) {						
						aa.assetSrc = asset.src;
						aa.open();
						return true;
					}
				}
			}
			return false;
		}
		
		public function highlightAreas( b:Boolean ):void {
			for each (var aa:IActiveArea in _areas) {
				aa.highlightAreas(b);
			}
		}
		
		public function showHotSpotsAreas( b:Boolean ):void {
			_areasVisible = b;
			for each (var aa:IActiveArea in _areas) {
				aa.showHotSpotsAreas(b);
			}
		}
		
		protected function onHotspotStateUpdated(e:HotspotStateEvent):void
		{
			dispatchEvent(e.clone());
		}
		
		public function setHotspotsState(hotspotsState:Array):void
		{
			_hotspotStates = hotspotsState;
			for each (var singleHotspotState:HotspotStateVO in _hotspotStates)
			{
				var hotspotId:String = singleHotspotState.hotspotId;
				for each (var obj:Object in _areas) 
				{
					var area:IStatefulHotspot = obj as IStatefulHotspot;
					if (area != null && area.hotspotId == hotspotId)
					{
						area.stateData = singleHotspotState.data;
						break;
					}
				}
			}
		}
		
		public function getHotspotsState():Array
		{
			for each (var obj:Object in _areas) 
			{
				var area:IStatefulHotspot = obj as IStatefulHotspot;
				if (area != null)
				{
					for each (var singleHotspotState:HotspotStateVO in _hotspotStates)
					{
						if (area.hotspotId == singleHotspotState.hotspotId)
						{
							singleHotspotState.data = area.stateData;
							break;
						}
					}
				}
			}
			return _hotspotStates;		
		}
		
		// getters/setters
		public function get areas():Array {
			return _areas;
		}
		
		public function get model():XML {
			return _model;
		}
		
		public function set model(value:XML):void {
			trace ("DODAJE MODEL HOTSPOTOW");
			_model = value;
			removeActiveAreas();
			createActiveAreas();
		}
		
		protected function isAssetAvailableForCurrentUserProfile(asset:XML):Boolean
		{
			var profileList:XMLList = asset.userProfiles.profile;
			
			if (profileList.length() == 0)
			{
				return true;
			}
			
			for each (var profile:XML in profileList) {
				if (profile.toString() == _userProfileId)
				{
					return true;
				}
			}
			return false;
		}
		
		public function set enabled(b:Boolean):void {
			_enabled = b;
			for each (var area:IActiveArea in _areas) {
				area.enabled = b;
			}
		}
		
		public function get enabled():Boolean {
			return _enabled;
		}
		
		public function set zoomEnabled(b:Boolean) {
			_zoomEnabled = b;
		}
		
		public function get basePath():String
		{
			return _basePath;
		}
		
		public function set basePath(value:String):void
		{
			_basePath = value;
		}
		
		public function get userProfileId():String
		{
			return _userProfileId;
		}
		
		public function set userProfileId(value:String):void
		{
			_userProfileId = value;
		}
		
	}
	
}

