﻿package com.ydp.pdf2flash.view 
{
	import com.ydp.core.IDisplayObject;
	import com.ydp.pdf2flash.model.IBook;
	import com.ydp.pdf2flash.model.IConfig;
	import com.ydp.pdf2flash.model.IGlossary;
	import com.ydp.pdf2flash.model.IMarker;
	import com.ydp.pdf2flash.model.Note;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author Michal Samujlo
	 */
	public interface IDisplayArea extends IDisplayObject
	{
		function zoom( sc:Number ):void;
		function zoomToPoint(mousePoint:Point, centerPoint:Point, sc:Number):void;
		function fitToWidth():void;
		function showDrawingArea():void;
		function hideDrawingArea():void;
		
		function selectNote(note:Note):void;
		function selectMarker(note:IMarker):void;
		function showIconNote(note:Note):void;
		function removeIconNote(note:Note):void;
		function getNoteData():Note;
		function getMarkerData():IMarker;
		
		function setBorderStyle( width:Number, color:Number, alpha:Number, blur:Number):void;
		function setActualSize(w:int, h:int):void;
		function setPagePosition(x:int, y:int):void;
		function getPagePosition():Point;
		function getActualPagesBounds(target:DisplayObject = null):Rectangle;
		function getPagesBounds():Rectangle;
		function get scale():Number;
		function set model(model:IBook):void;
		function get drawingArea():Sprite;
		function get toolsDrawingArea():Sprite;
		function get galleryArea():Sprite;
		function set isBorderVisible(v:Boolean):void;
		function get isBorderVisible():Boolean;
		function set currentPage(nr:int):void;
		function get currentPage():int;
		function set maskEnabled(st:Boolean):void;
		function get zoomValue():Number;
		function get defaultZoom():Number;
		function showWatermark(s:String):void;

		// allow interaction with hotspots
		function set enabled(b:Boolean):void;
		
		// enable drag scrolling
		function set dragMode(b:Boolean):void;
		
		// how many pages are displayed at once
		function get pageCount():int;
		
		// flexbook specific
		function set useBitmap(v:Boolean):void;
		function set hoverEnabled(st:Boolean):void;
		function get hoverEnabled():Boolean;
		function get flexbookPath():String;
		function set flexbookPath(value:String):void;
		
		function getPageScale():Point;
		
		/**
		 * searches for and opens area that has asset linked to assetSrc
		 */
		function openAsset(assetSrc:String):Boolean;
		
		/**
		 * @return array of numbers of visible pages
		 */
		function getVisiblePages():Array;
		
		// hotSpot zoom enabled
		function set hotSpotZoomEnabled(b:Boolean):void;
		
		//added by CAOgroup
		function set usePagesBlockade(value:Boolean) : void	
		function setBlockadeWindowDefinition( window:Object ) : void;
		function notifyIdentificationError() : void;
		
		function set allowToShowMap(value:Boolean):void;		
		
		function set configViewer(cfg:IConfig):void;
		
		function highlightHotSpotsAreas(b:Boolean):void;
		function showHotSpotsAreas(b:Boolean):void;
		
		function get glossary():IGlossary;
		function set glossary(value:IGlossary):void;
		
		function stopAllHotspots():void;
		
	}
	
}