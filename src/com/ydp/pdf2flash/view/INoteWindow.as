package com.ydp.pdf2flash.view {
	import com.ydp.pdf2flash.model.Note;

	public interface INoteWindow extends IResourcesWindow{
		function getSelectNote():Note;
	}
}