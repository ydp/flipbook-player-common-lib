﻿package  com.ydp.pdf2flash.view.bookmarks
{
	import flash.display.MovieClip;
	import flash.events.IEventDispatcher;
	
	/**
	 * ...
	 * @author 
	 */
	public interface IMainBookmarksContent extends IEventDispatcher{
		function init():void;
		function invalidateSize():void ;
		function setSize(w:Number, h:Number):void;
		function set dataProvider(value:Array):void;
		function get dataProvider():Array;
		function addItem(value:Object):void;
		function removeItem():void;
	}
	
}