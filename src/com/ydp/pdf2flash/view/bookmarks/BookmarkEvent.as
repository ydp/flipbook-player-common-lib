﻿package  com.ydp.pdf2flash.view.bookmarks
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author loleszczak
	 */
	public class BookmarkEvent extends Event{
		
		public static const CLOSE_MAIN_BOOKMARK_PANEL:String = "closeMainBookmark";
		public static const CLOSE_SHORT_BOOKMARK_PANEL:String = "closeShortBookmark";
		public static const ADD_PAGES:String = "addBookmark";
		public static const SWITCH_TO_SHORT_PANEL:String = "switchToShortPanel";
		public static const GO_TO_ITEM:String = "goToItem";
		public static const DATA_PROVIDER_CHANGED:String = "dataProviderChanged";
		public static const REMOVE_ITEM:String = "removeItem";
		
		public var parameters:Object;
		public function BookmarkEvent(type:String, parameters:Object=null, bubbles:Boolean = false, cancelable:Boolean = false){
			super(type, bubbles, cancelable);
			this.parameters = parameters;
		}
		
	}
	
}