﻿package com.ydp.pdf2flash.view 
{
	import com.ydp.core.IDisplayObject;
	
	/**
	 * ...
	 * @author Tomasz Szarzyński
	 */
	public interface IPageBlockadeWindow extends IDisplayObject
	{
		function notifyError() : void
	}
	
}