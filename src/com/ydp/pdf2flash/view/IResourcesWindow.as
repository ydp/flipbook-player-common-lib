﻿package com.ydp.pdf2flash.view 
{
	import com.ydp.core.IDisplayObject;
	import com.ydp.pdf2flash.model.IBook;
	
	/**
	 * @author Michał Samujło, Michał Achtel
	 */
	public interface IResourcesWindow extends IDisplayObject
	{
		/**
		 * set currenPage
		 * @param nr currentPage number
		 */
		function setSelectedPage(nr:int,cp:int):void; 

		/**
		 * @return name of selected page
		 */
		function get selectedPage():String;
		
		/**
		 * Show resources for given pages
		 * @param pages array of page numbers 
		 */
		function set selectedPages(pages:Array):void
		
		/**
		 * Returns data for last selected item
		 */
		function get selectedItem():String;
		
		function setActualSize(w : Number, h: Number):void; 
			
		function setActualPosition(x : Number, y: Number) : void;
			
		function set onScreen(bool:Boolean):void; 
			
		function get onScreen(): Boolean;
		
		function set model(model:Object):void;
	}
	
}