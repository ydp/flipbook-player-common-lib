﻿package com.ydp.pdf2flash.view.drawingTools {

	import com.ydp.utils.AssetFactory;
	import com.ydp.view.ButtonDecorator;
	import com.ydp.view.TooltipDecorator;
	import flash.display.*;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Rectangle;
	import flash.ui.*;
	import flash.geom.Point;
	import com.ydp.pdf2flash.events.ViewerEvent;
	
	public class CacheOvalTool extends DrawingTools implements IDrawingTool {
		
		public static const CACHE_DRAWING_STATE_CHANGED : String = "cacheDrawingStateChanged";
		protected var btnCloseDecorator:ButtonDecorator;
		
		override public function init(drawingArea:Sprite, pedzelArea:Sprite, closeArea:Sprite, bounds:Rectangle, skinBounds:Array):void {
			super.init(drawingArea, pedzelArea, closeArea, bounds, skinBounds);
			_name = "cacheOval";
			_col = 0x000000;
			_thick = 1;
			_pedzel = AssetFactory.getAsset("pen4") as MovieClip;
			_pedzel.mouseEnabled = false;
			_pedzel.visible = false;
			pedzelArea.addChild(_pedzel);
			btnCloseDecorator = new ButtonDecorator(_btnClose);
			new TooltipDecorator(_btnClose, resources.getProperty("close_cache"));
			
		}
		
		public function get type():String {
			return _name;
		}
		
		public function startDrawing():void {
			_drawingMode = true;
			eventArea.visible = true;
			pedzelArea.visible = true;
			eventArea.addEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
			eventArea.addEventListener( MouseEvent.MOUSE_OUT, onMouseOut );
			
			eventArea.addEventListener( MouseEvent.MOUSE_MOVE, onMouseMove );
			eventArea.stage.addEventListener( MouseEvent.MOUSE_MOVE, onMouseStageMove );
			eventArea.addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
			eventArea.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			_listenersAdded = true;
			dispatchEvent(new Event(Event.CHANGE));
		}
		
		public function stopDrawing():void {
			_drawingMode = false;
			eventArea.visible = false;
			pedzelArea.visible = false;
			_pedzel.visible = false;
			Mouse.show();
			if (_listenersAdded) {
				eventArea.removeEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
				eventArea.removeEventListener( MouseEvent.MOUSE_OUT, onMouseOut );
				
				eventArea.removeEventListener( MouseEvent.MOUSE_MOVE, onMouseMove );
				eventArea.stage.removeEventListener( MouseEvent.MOUSE_MOVE, onMouseStageMove );
				eventArea.removeEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
				eventArea.removeEventListener( MouseEvent.MOUSE_UP, onMouseUp );
				_listenersAdded = false;
			}
			dispatchEvent(new Event(Event.CHANGE));
		}
		
		override public function updateAreas(bounds:Rectangle, skinBounds:Array):void {
			super.updateAreas(bounds, skinBounds);
			setCloseButtonPosition();
		}
		
		protected function mousePedzelPos():Array {
			var point:Point = _drawingArea.parent.localToGlobal( new Point(_drawingArea.mouseX, _drawingArea.mouseY) );
			return [point.x,point.y];
		}

		private function onMouseOver(evt:MouseEvent):void {
			Mouse.hide();
			_pedzel.visible = true;
		}
		
		private function onMouseOut(evt:MouseEvent):void {
			Mouse.show();
			_pedzel.visible = false;
		}
		
		private function onMouseStageMove(evt:MouseEvent):void {
			_pedzel.x = mousePedzelPos()[0];
			_pedzel.y = mousePedzelPos()[1];			
		}
		
		private function onMouseMove(evt:MouseEvent):void {
			if (isEmpty() || !_drawing) 
				return;			
			_currSprite.graphics.clear();
			_currSprite.graphics.beginFill(_col,0);
			_currSprite.graphics.lineStyle(_thick,_col,1);
			_currSprite.graphics.drawEllipse(_moveXML.@x0, _moveXML.@y0, mouseDrawPos()[0] - _moveXML.@x0, mouseDrawPos()[1] - _moveXML.@y0);
			_currSprite.graphics.endFill();			
		}
		
		private function onMouseDown(evt:MouseEvent):void {
			dispatchEvent(new ViewerEvent(ViewerEvent.START_DRAWING));
			if (_btnClose != null) 
				_btnClose.visible = false;			
			createMove();
			addMove();
			_currSprite.stage.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			_drawing = true;
		}

		private function onMouseUp(evt:MouseEvent):void {
			if (!_drawing) return;
			_drawing = false;
			
			_currSprite.stage.removeEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			
			var x1:Number = mouseDrawPos()[0];
			var y1:Number = mouseDrawPos()[1];
			
			if (Math.abs(x1 - _startX) < 2 && Math.abs(y1 - _startY) < 2) {
				this.clear();
				_node = stateNode.copy();
				return;
			}
			
			if (x1 < drawArea.x) x1 = drawArea.x;
			if (x1 > drawArea.x + drawArea.width) x1 = drawArea.x + drawArea.width;
			if (y1 < drawArea.y) y1 = drawArea.y;
			if (y1 > drawArea.y + drawArea.height) y1 = drawArea.y + drawArea.height;
			
			_moveXML.@x1 = x1;
			_moveXML.@y1 = y1;
			_node.appendChild(_moveXML);

			draw(_moveXML.@x0, _moveXML.@y0, x1, y1);
			stopDrawing();
			dispatchEvent(new ViewerEvent(ViewerEvent.STOP_DRAWING));
			if (isEmpty()) 
				return;
		}
		
		private function draw(x0:Number, y0:Number, x1: Number, y1:Number):void {
			_currSprite.graphics.clear();
			_currSprite.graphics.lineStyle(_thick, _col, 0);
			
			_currSprite.graphics.beginFill(_col, 1);
			_currSprite.graphics.drawEllipse(x0, y0, x1 - x0, y1 - y0); 
			_currSprite.graphics.endFill();			
			setCloseButtonPosition();
		}
		
		private function setCloseButtonPosition():void {
			if (_btnClose) {
				_btnClose.visible = !isEmpty();
				var p:Point = new Point( Math.max(_node.move.@x1, _node.move.@x0), Math.min(_node.move.@y0, _node.move.@y1) );
				var point:Point = _drawingArea.parent.localToGlobal( p )
				_btnClose.x = point.x - _btnClose.width;
				_btnClose.y = point.y;
			}
		}
		
		override public function saveState():XML {
			var state:XML = super.saveState();
			return state;
		}
		
		override public function loadState(xmlNode:XML):void {
			if (!_currSprite) {
				createMove();
			}
			_currSprite.graphics.clear();
			_col = xmlNode.@col;
			_thick = xmlNode.@thick;
			_node = xmlNode;
			draw(xmlNode.move.@x0, xmlNode.move.@y0, xmlNode.move.@x1, xmlNode.move.@y1);
		}
		
		public function zoomChanged(value:Number):void{
			setCloseButtonPosition();
		}
	}
}