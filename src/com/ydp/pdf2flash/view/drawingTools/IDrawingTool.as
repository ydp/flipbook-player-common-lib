﻿package com.ydp.pdf2flash.view.drawingTools {
	
	import flash.display.Sprite;
	import flash.events.IEventDispatcher;
	import flash.geom.Rectangle;
	
	public interface IDrawingTool extends IEventDispatcher {
		
		/**
		 * @param	drawingArea Sprite used to draw on
		 * @param	pedzelArea Sprite to attach cursor
		 * @param	closeArea Sprite to attach close button
		 * @param	bounds drawing bounds
		 * @param 	skinBounds top bar and navigation bar bounds
		 */
		function init(drawingArea:Sprite, pedzelArea:Sprite, closeArea:Sprite, bounds:Rectangle, skinBounds:Array):void;

		/**
		 * switched to drawing mode
		 */
		function startDrawing():void;
		
		/**
		 * stops drawing mode
		 */
		function stopDrawing():void;

		/**
		 * loads state
		 */
		function loadState(xmlNode:XML):void

		/**
		 * saves state
		 */
		function saveState():XML
		
		/**
		 * unique tool id
		 */
		function get type():String;

		/**
		 * clears the drawing and savestate
		 */
		function clear():void		
		
		function updateAreas(bounds:Rectangle, skinBounds:Array):void;
		
		function zoomChanged(value:Number):void;
	}
	
}
