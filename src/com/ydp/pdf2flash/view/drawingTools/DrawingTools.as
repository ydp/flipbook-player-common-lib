﻿package com.ydp.pdf2flash.view.drawingTools {
	
	import com.ydp.pdf2flash.events.ViewerEvent;
	import com.ydp.resources.IResourceManager;
	import flash.display.*;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.geom.Rectangle;
	import flash.display.Sprite;
	import com.ydp.utils.YURLLoader;
	import flash.net.URLRequest;	
	import flash.ui.*;
	import com.ydp.utils.*;
	import pl.ydp.nexl.Nexl;
	import pl.ydp.nexl.INexlExtensionDescription;
	
	public class DrawingTools extends EventDispatcher {
		
		public static var DRAWING_TOOLS_STATE_LOADED:Boolean = false;
		
		protected var _drawingArea:Sprite;
		protected var _drawArea:Sprite;
		protected var _eventArea:Sprite;
		protected var _pedzelArea:Sprite;	
		protected var _node:XML;
		protected var _moveXML:XML;
		protected var _allNodes:XML;
		protected var _currSprite:Sprite;
		protected var _drawing:Boolean;
		protected var _col:Number; 
		protected var _thick:Number;
		protected var _name:String;
		protected var _listenersAdded:Boolean;
		protected var _btnClose:MovieClip;
		protected var _startX:Number;
		protected var _startY:Number;
		protected var _drawingMode:Boolean;
		protected var _closeArea:Sprite;
		protected var _skinBounds:Array;
		
		protected var _pedzel:MovieClip;
		protected var resources:IResourceManager;
		
		public function DrawingTools() {
			// empty state
			_node = stateNode.copy();
		}
		
		// TODO przypilnowac, zeby drugie wolanie init tez dzialalo
		public function init(drawingArea:Sprite, pedzelArea:Sprite, closeArea:Sprite, bounds:Rectangle, skinBounds:Array):void {
			_drawingArea = drawingArea;
			_pedzelArea = pedzelArea;
			_pedzelArea.mouseEnabled = false;
			_closeArea = closeArea;
			_skinBounds = skinBounds;
			
			var exts:Array = Nexl.library.list( { type: "com.ydp.resources.IResourceManager" } );
			var ext:INexlExtensionDescription = exts[0];
			resources = ext.create() as IResourceManager;
			
			_btnClose = AssetFactory.getAsset("closeSpotCache") as MovieClip;
			_btnClose.visible = false;

			var glowFilter:GlowFilter = new GlowFilter(0, 0.5, 3, 3, 10, BitmapFilterQuality.HIGH );
			_btnClose.filters = [ glowFilter ];
			
			_closeArea.addChild(_btnClose);
			
			_btnClose.addEventListener( MouseEvent.MOUSE_DOWN, onCloseDown );
			
			if (_drawingArea.numChildren != 0) 
				return;
				
			//DRAW AREA
			_drawArea = new Sprite();
			_drawArea.name = "drawArea";
			createRectangle(_drawArea, bounds, false, 0);
			_drawingArea.addChild(_drawArea);
			//_drawArea.visible = false;
			_drawArea.mouseEnabled = false;
			_drawArea.mouseChildren = false;
			
			//EVENT AREA
			_eventArea = new Sprite();
			_eventArea.name = "eventArea";
			createRectangle(_eventArea, bounds, true, 0);
			_drawingArea.addChild(_eventArea);
			_eventArea.visible = false;

		}
		
		private function btnCloseLoaded(evt:Event):void {
		}
		
		private function onCloseDown(evt:MouseEvent):void {
			clear();
			_node = stateNode.copy();
			Mouse.show();
		}
		
		protected function mouseDrawPos():Array {
			return [drawArea.mouseX,drawArea.mouseY];
		}			
		
		protected function createMove():void {
			_node = stateNode.copy();
			_node.@type = _name;
			_node.@col = _col;
			_node.@thick = _thick;
			_currSprite = new Sprite();
			for (var i:int = 0; i < drawArea.numChildren; i++) {
				var child : * = drawArea.getChildAt(i);
				var nam : String = child.name;
				if (nam != null) {
					if (nam == _name) {
						_currSprite = Sprite(child)	
					}	
				}
			}
			
			if (_currSprite.name == _name) {
				_currSprite.graphics.clear();
			} else {
				_currSprite.name = _name;
				drawArea.addChildAt(_currSprite, 0);
			}
			_node.@sprite = _currSprite.name;
		}		
		
		protected function addMove():XML {
			var moveNode:XML = new XML("<move/>");
			_startX = mouseDrawPos()[0];
			_startY = mouseDrawPos()[1];
			
			moveNode.@x0 = _startX;
			moveNode.@y0 = _startY;
			
			_currSprite.graphics.lineTo( mouseDrawPos()[0], mouseDrawPos()[1]);
			_moveXML = moveNode;
			return moveNode;
		}
		
		
		public function updateAreas(bounds:Rectangle, skinBounds:Array):void {
			_skinBounds = skinBounds;
			if (_drawingArea.numChildren == 0) return;
				createRectangle(drawArea, bounds, false, 0);
				createRectangle(eventArea, bounds, true, 0);
		}
		
		private function createRectangle(sp:Sprite, bounds:Rectangle, fillMode:Boolean, alpha:Number):void {
			sp.graphics.clear();
			if (fillMode)
				sp.graphics.beginFill(0xFFFF00, alpha);
			sp.graphics.lineStyle(0,0xFF0000, 0);
			sp.graphics.drawRect(0, 0, bounds.width, bounds.height);
			if (fillMode)
				sp.graphics.endFill();			
		}		
		
		public function loadState(xmlNode:XML):void {
		}
		
		public function saveState():XML {
			return _node;
		}
		
		public function clear():void {
			if (_currSprite != null) {
				_currSprite.graphics.clear();
			}
			if (_btnClose.visible == true) {
				dispatchEvent(new ViewerEvent(ViewerEvent.HIDE_CACHE_TOOL));
				dispatchEvent(new ViewerEvent(ViewerEvent.HIDE_SPOT_TOOL));
			}
			if (_btnClose != null) 
				_btnClose.visible = false;
				
			_node = stateNode.copy();
		}
		
		public function isEmpty() {
			return _node.attribute("type").length()==0;
		}
		
		protected function get stateNode():XML {
			return <drawingTool />;
		}
		
		// Getters / Setters
		
		public function get drawArea():Sprite {
			return Sprite(_drawingArea.getChildByName("drawArea"));
		}
		
		public function get eventArea():Sprite {
			return Sprite(_drawingArea.getChildByName("eventArea"));
		}
		
		public function get pedzelArea():Sprite {
			//return Sprite(_drawingArea.getChildByName("pedzelArea"));
			return _pedzelArea;
			
		}	
		
		public function get drawingState():Boolean {
			return _drawingMode;
		}
		
		protected function onLoadError(evt:IOErrorEvent):void {
			trace("IOError:", evt.text );
		}	

	}
	
}