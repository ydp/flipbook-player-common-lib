﻿package com.ydp.pdf2flash.view.blockade 
{
	import com.ydp.pdf2flash.view.IPageBlockadeWindow;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.utils.getDefinitionByName;
	import com.ydp.pdf2flash.events.BlockadeWindowEvent;
	
	import pl.ydp.nexl.Nexl;
	import pl.ydp.nexl.INexlExtensionDescription;
	/**
	 * ...
	 * @author Tomasz Szarzyński
	 */
	public class BlockadeContainer extends Sprite 
	{
		public var xoffset:Number = 0;
		public var yoffset:Number = 0;
		private var _enabled:Boolean = true;
		private var _zoomEnabled = true;
		//
		private var _blockade:Sprite;
		private var _window:IPageBlockadeWindow;
		
		
		public function BlockadeContainer() 
		{
			
		}
		
		public function setActualSize(w:Number, h:Number)
		{			
			if (_window != null)
			{
				_window.x = int((w / 2) - (_window.width / 2));
				_window.y = int((h / 2) - (_window.height / 2));
			}
		}
		
		public function setWindow(window:Object) : void
		{
			var ClassReference:Class = getDefinitionByName(window as String) as Class;
            var instance:Object = new ClassReference();
			
			/*var exts:Array;
		
			
			exts = Nexl.library.list( { type: "com.ydp.pdf2flash.view.IPageBlockadeWindow" } );	
			if (exts.length>0) {
				var obj:Object = INexlExtensionDescription(exts[0]).create();
				//sendToFriend = obj as DisplayObject;	
				_window = addChild(obj as DisplayObject);
			}*/
			_window = addChild(instance as DisplayObject) as IPageBlockadeWindow;
			_window.addEventListener(BlockadeWindowEvent.IDENTIFY, onEvent, false, 0, true);
			_window.addEventListener(BlockadeWindowEvent.REGISTER, onEvent, false, 0, true);
		}	
		
		public function notifyError() : void
		{
			_window.notifyError();
		}
		
		private function onEvent(event:BlockadeWindowEvent) : void
		{
			dispatchEvent( event.clone() );
		}
	}	
}