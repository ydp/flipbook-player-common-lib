package com.ydp.pdf2flash.hotspots
{
	import flash.display.Graphics;
	
	/**
	 * Definition of active area shape.
	 * @see IActiveArea 
	 * @author MSamujlo
	 */
	public interface IActiveAreaShape
	{
		function get type():String;
		
		/**
		 * add shape definition to node
		 */
		function toXML():XML;
		
		/**
		 * draw shape
		 */
		function draw( g:Graphics ):void;
		
		function copy():IActiveAreaShape;
	}
}