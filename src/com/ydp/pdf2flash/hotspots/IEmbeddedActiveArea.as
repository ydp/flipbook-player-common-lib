package com.ydp.pdf2flash.hotspots
{
	public interface IEmbeddedActiveArea extends IActiveArea
	{
		function stop():void
		function set basePath(value:String):void;
		function get basePath():String;
	}
}