﻿package com.ydp.pdf2flash.hotspots {
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import com.ydp.style.IStyleClient;
	
	/**
	* ActiveArea displays assets listed in areaData xml e.g.:
	*	<area x="62" y="259" width="264" height="312" 
	*		type="video" activation="button" 
	*		isPopup="true" hasButton="true" 
	*		isPlayerUIVisible="true" link="" areaTooltip="" button="" rolloverIcon="">
	*		<assets>
	*			<asset src="assets/dummy.flv"/>
	*		</assets>
	*		<style>
	*		<background color="#ff0000" overColor="#3300ff" alpha="0.2"/>
	*			<frame color="#ffffff" overColor="#ffffff" alpha="0.2"/>
	*			<popup color="#ffffff" overColor="#ffffff" alpha="0.2"/>
	*			<popupFrame color="#ffffff" overColor="#ffffff" alpha="0.2"/>
	*		</style>
	*	</area>
	* or:
	*	<area x="22" y="37" width="136" height="252" 
	*		type="link" activation="none" 
	*		isPopup="false" hasButton="false" 
	* 		isPlayerUIVisible="true" link="http://www.google.com" areaTooltip="" button="" rolloverIcon="">
	*		<style>
	*			<background color="#ffffff" overColor="#ffffff" alpha="0.2"/>
	*			<frame color="#ffffff" overColor="#ffffff" alpha="0.2"/>
	*			<popup color="#ffffff" overColor="#ffffff" alpha="0.2"/>
	*			<popupFrame color="#ffffff" overColor="#ffffff" alpha="0.2"/>
	*		</style>
	*	</area>
	* 
	* @author Michal Samujlo
	*/
	public interface IActiveArea extends IEventDispatcher, IStyleClient {
		
		// redraw area
		function updateDisplayList(evt:Event=null):void;
		
		// called when area is activated e.g. by click
		function open(evt:Event = null):void;
		
		function highlightAreas(b:Boolean):void;
		function showHotSpotsAreas(b:Boolean):void;
		
		function set enabled(b:Boolean):void;
		
		function get x():Number;
		function set x(dx:Number):void;
		
		function get y():Number;
		function set y(dy:Number):void;
		
		function get areaData():XML;
		function set areaData(data:XML):void;
		
		function get type():ActiveAreaType;
		function set type(t:ActiveAreaType):void;
		
		function get assetSrc():String;
		function set assetSrc(s:String):void;
		
		// Array<com.ydp.pdf2flash.hotspots.Asset>
		function get assets():Array;
		
		function isPopup():Boolean;
		function hasButton():Boolean;
		function isPlayerUIVisible():Boolean;
		function areaTooltip():String;
		
		//zoomowanie wlaczone/wylaczone na hotspot
		function set zoomEnabled(b:Boolean):void;
		
		//added by ts
		function set pageData(value:XML) : void;
		function get pageData() : XML;

	}
	
}