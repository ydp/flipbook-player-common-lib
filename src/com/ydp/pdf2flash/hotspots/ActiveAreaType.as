﻿package com.ydp.pdf2flash.hotspots {
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * @author Michal Samujlo
	 */
	public class ActiveAreaType {
		
		private static var _types:Dictionary = new Dictionary(true);
		
		// active area nie wykonujące żadnej akcji
		public static const DUMMY:ActiveAreaType = new ActiveAreaType("dummy");
		
		// active area otwiera przez flashex
		public static const FILE:ActiveAreaType = new ActiveAreaType("file");
		
		// link do strony wewnątrz publikacji
		public static const INTERNAL_LINK:ActiveAreaType = new ActiveAreaType("internal_link");
		
		//link zewnętrzny otwierany przez przeglądarkę n.p.: http://www.wp.pl albo /assets/test.pdf
		public static const LINK:ActiveAreaType = new ActiveAreaType("link");
		
		//strona lub test utopii
		public static const CONTENT_LINK:ActiveAreaType = new ActiveAreaType("content_link");
		
		//watermark zasłaniający cześć publikacji
		public static const WATERMARK:ActiveAreaType = new ActiveAreaType("watermark");
		
		public static const LEO_CONTENT_LINK:ActiveAreaType   = new ActiveAreaType("leo_content_link");

		public static const EMPIRIA:ActiveAreaType = new ActiveAreaType("empiria");
		
		// odtwarzanie mediów
		public static const AUDIO:ActiveAreaType = new ActiveAreaType("audio");
		public static const VIDEO:ActiveAreaType = new ActiveAreaType("video");
		public static const ANIMATION:ActiveAreaType = new ActiveAreaType("animation");
		public static const IMAGE:ActiveAreaType = new ActiveAreaType("image");
		public static const YOUTUBE:ActiveAreaType = new ActiveAreaType("youtube");
		public static const GLOSSARY:ActiveAreaType = new ActiveAreaType("glossary");
		public static const HTML5:ActiveAreaType = new ActiveAreaType("html5");
		
		// deprecated
		public static const SIMULATION:ActiveAreaType = new ActiveAreaType("simulation");
		public static const SLIDESHOW:ActiveAreaType = new ActiveAreaType("slideshow");
		
		//added by ts
		public static const ZOOM:ActiveAreaType = new ActiveAreaType("zoom");
		public static const MULTIFILE:ActiveAreaType = new ActiveAreaType("multifile");
		public static const EMPTY:ActiveAreaType = new ActiveAreaType("empty");
		public static const TOOLTIP:ActiveAreaType = new ActiveAreaType("tooltip");
		public static const HIDE_SPOT:ActiveAreaType = new ActiveAreaType("hide_spot");
		
		private var _name:String;
		
		public function ActiveAreaType(name:String) {
			_name = name;
			_types[name] = this;
		}
		
		public static function getByName(name:String):ActiveAreaType {
			return _types[name];
		}
		
		public static function getFileExtensionsForType(name:String):String {
			var ext:String;
			switch (name) {
				case ActiveAreaType.AUDIO.name:
					ext = "*.mp3";
					break;
				case ActiveAreaType.VIDEO.name:
					ext = "*.flv";
					break;
				case ActiveAreaType.ANIMATION.name:
					ext = "*.swf";
					break;
				case ActiveAreaType.IMAGE.name:
					ext = "*.swf;*.jpg;*.png;*.gif;";
					break;
				default:
					ext = "";
					break;
			}
			return ext;
		}
		
		public function get name():String {
			return _name;
		}
		
		public function toString():String {
			return _name;
		}
	}

}