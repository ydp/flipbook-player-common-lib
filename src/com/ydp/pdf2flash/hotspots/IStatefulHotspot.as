package com.ydp.pdf2flash.hotspots 
{
	public interface IStatefulHotspot 
	{
		function get hotspotId():String;
		function set hotspotId(value:String):void;
		function set stateData(value:String):void;
		function get stateData():String;
	}
}