﻿package com.ydp.pdf2flash.hotspots 
{
	
	/**
	* ...
	* @author Michał Samujło
	*/
	public class Asset 
	{
		public var src:String;
		private var _version:String;
		private var _title:String;
		//added by ts
		public var type : ActiveAreaType;
		
		public function get title():String { return _title; }
		
		public function set title(value:String):void 
		{
			_title = (value!=null)? value : "";
		}
		
		public function get version():String { return _version; }
		
		public function set version(value:String):void 
		{
			_version = (value=="avm1")? value : null;
		}
		
	}
	
}