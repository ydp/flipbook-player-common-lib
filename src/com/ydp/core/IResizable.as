﻿package com.ydp.core 
{
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author Michal Samujlo
	 */
	public interface IResizable 
	{
		function getActualSize():Rectangle;
		function setActualSize(w:Number, h:Number):void;
	}
	
}