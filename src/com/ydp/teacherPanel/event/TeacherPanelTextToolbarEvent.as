package com.ydp.teacherPanel.event
{
	import flash.events.Event;

	public class TeacherPanelTextToolbarEvent extends Event{
		//params -TextFormat
		public static const TEXT_FORMAT_CHANGED:String = "textFormatChanged";
		//params -Rectange - new size
		public static const SIZE_CHANGED:String = "sizeChanged";
		
		//params - char
		public static const INSERT_CHAR:String = "insertChar";
		
		public var params:Object;
		public function TeacherPanelTextToolbarEvent(type:String, params:Object=null, bubbles:Boolean=false, cancelable:Boolean=false){
			super(type, bubbles, cancelable);
			this.params = params;
		}
		
	}
}
