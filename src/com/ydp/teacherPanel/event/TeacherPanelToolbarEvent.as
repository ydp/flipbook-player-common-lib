package com.ydp.teacherPanel.event
{
	import flash.events.Event;

	public class TeacherPanelToolbarEvent extends Event{
		public static const ACTIVATE_TOOL:String = "activateTool";
		public static const DEACTIVATE_TOOL:String = "deactivateTool";		
		public static const TOOL_USED:String = "toolUsed";	
		
		public var params:Object;
		public function TeacherPanelToolbarEvent(type:String, params:Object=null, bubbles:Boolean=false, cancelable:Boolean=false){
			super(type, bubbles, cancelable);
			this.params = params;
		}
		
	}
}