package com.ydp.teacherPanel
{
	import com.ydp.teacherPanel.IFormatableTextContainer;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.geom.Rectangle;
	import flash.text.TextFormat;
	
	public interface ITeacherPanelTextEditorToolbar extends IEventDispatcher{		
		function getTextFormat():TextFormat;
		function setTextBounds(value:Rectangle):void;
		function setTextField(value:IFormatableTextContainer):void;
		function getTextField():IFormatableTextContainer;
		function selectionChangedHandler(event:Event):void;
		function get panelBounds():Rectangle;
	}
}