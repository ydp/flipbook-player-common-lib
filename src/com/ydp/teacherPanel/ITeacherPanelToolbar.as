package com.ydp.teacherPanel
{
	import flash.events.IEventDispatcher;
	
	public interface ITeacherPanelToolbar extends IEventDispatcher{
		function toolStateChanged(toolName:String, params:Object):void;		
	}
}