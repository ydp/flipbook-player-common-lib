package com.ydp.teacherPanel
{
	public class ToolTypes
	{
		public static const PEN:String = "Pen";
		public static const MARKER:String = "Marker";
		public static const LINE:String = "Line";
		public static const ARROW_LINE:String = "ArrowLine";
		public static const TRIANGLE:String = "Triangle";
		public static const FILLED_TRIANGLE:String = "FilledTriangle";
		public static const ARROW:String = "Arrow";
		public static const UNDO:String = "Undo";
		public static const REDO:String = "Redo";		
		public static const FILLED_SQUARE:String = "FilledSquare";
		public static const SQUARE:String = "Square";
		public static const FILLED_ELLIPSE:String = "FilledEllipse";
		public static const ELLIPSE:String = "Ellipse";
		
		public static const POLYGON:String = "Polygon";
		
		public static const CACHE:String = "Cache";
		public static const CACHE_OVAL:String = "CacheOval";
		public static const SPOT:String = "Spot";
		
		public static const MARK:String = "Mark";
		public static const SHADE:String = "Shade";
		
		public static const TEXT:String = "Text";
		
		public static const CALCULATOR:String = "Calculator";
		
		public static const ERASER:String = "Eraser";
		public static const CLEAR_DOUBLE:String = "ClearDouble";
		public static const CLEAR_ALL:String = "ClearAll";
		
		public function ToolTypes()
		{
		}

	}
}