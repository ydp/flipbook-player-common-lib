package com.ydp.teacherPanel
{
	import flash.events.IEventDispatcher;
	import flash.geom.Rectangle;
	import flash.text.TextFormat;
	
	public interface IFormatableTextContainer extends IEventDispatcher{
		function getTextFormat():TextFormat;
		function setTextFormat(value:TextFormat):void;		
		function getTextBounds():Rectangle;
		function setTextBounds(value:Rectangle):void;
		function getLocalTextBounds():Rectangle;
		function getLimitBounds():Rectangle;
		function insertText(value:String):void;
		function get allowResize():Boolean;
		function get allowMove():Boolean;
		function get backgroundColor():uint;
		function set backgroundColor(value:uint):void;
		function get backgroundAlpha():Number;
		function set backgroundAlpha(value:Number):void;			
		function setTextFieldFocus():void;
		
	}
}