package pl.ydp.p2.flash
{
	import pl.ydp.p2.flash.base.IUtopiaAppBase;
	
	/**
	 * Utopia media player Wrapper
	 * Provides access to multimedia opened in Utopia player
	 */
	[Event(name="interfaceLoaded", type="mx.events.Event")]
	public interface IUtopiaMediaPlayer extends IUtopiaAppBase
	{
		/**
		 * Loads Utopia interface
		 * @param interfaceUri path to *.utt interface file
		 */
		function loadInterface( interfaceUri:String ):void;
		
		/**
		 * Loads media
		 * @param uri path base for absolute path resolving
		 * @param utp xml document to load
		 */
		function setPage( uri:String, utp:XML):void;
		
		/**
		 * Interface loaded flag
		 * @return interface loaded flag
		 */
		function get interfaceLoaded():Boolean;
		
		/**
		 * Provides utopia containter minimum width
		 * @return minimum width
		 */
		function get utopiaMinWidth():Number;
		
		/**
		 * Provides utopia containter minimum height
		 * @return minimum height
		 */
		function get utopiaMinHeight():Number;
	}
}