package pl.ydp.p2.flash.base
{
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	/**
	 * Base loader for Utopia Applications: FlUtopiaPlayer i FlUtopiaMediaPlayer
	 */
	public interface IUtopiaAppBase
	{
		/**
		 * Provides Utopia size
		 * @return utopia size
		 */
		function getUtopiaSize():Rectangle;
		
		/**
		 * Searches for Utopia module by its name/id
		 * @param name module name/id
		 * @return module
		 */
		function findModuleByName(name:String):DisplayObject;
	}
}