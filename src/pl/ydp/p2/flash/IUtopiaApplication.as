package pl.ydp.p2.flash
{
	import pl.ydp.p2.flash.base.IUtopiaAppBase;
	
	/**
	 * Utopia test player Wrapper
	 * Provides access to Utopia application within flipbook
	 */
	public interface IUtopiaApplication extends IUtopiaAppBase
	{
		/**
		 * Loads test file
		 * @param uri path to *.utt file
		 */
		function loadFile( uri:String ):void;
		
		/**
		 * Provides test starting page
		 * @return start page No.
		 */
		function get startPage():int;
		
		/**
		 * Sets page No. which Utopia player starts from
		 * @param page page No.
		 */
		function set startPage(page:int):void;
	}
}